const
  bcrypt = require('bcryptjs');

const
  CONSTANTS = require('./ew-constants'),
  {FUNCTION, OBJECT, STRING} = CONSTANTS.TYPES;

const
  IS_UNDEFINED = (val) => val === undefined,
  
  IS_NOT_UNDEFINED = (val) => !IS_UNDEFINED(val),
  
  IS_NULL = (val) => val === null,

  IS_NOT_NULL = (val) => !IS_NULL(val),

  IS_FUNCTION = (val) => typeof val === FUNCTION,

  IS_STRING = (val) => typeof val === STRING && !!val.length,

  IS_ARRAY = (val) => Array.isArray(val),

  IS_OBJECT = (val) => typeof(val) === OBJECT && IS_NOT_NULL(val) && !IS_ARRAY(val),

  IS_PROMISE = (val) => val && IS_FUNCTION(val['then']),

  AS_PROMISE = (val) => IS_PROMISE(val) ? val : new Promise(resolve => resolve(val)),
  
  AS_ARRAY = (val) => IS_ARRAY(val) ? val : [val],

  DO_NOTHING = () => undefined,
  
  CHECK_AND_CALL = (...args) => {
    let
      _args = Array.from(args),
      _fn = _args.splice(0, 1)[0];
    return IS_FUNCTION(_fn) && _fn.apply(null, _args);
  },

  DIG_OUT = (..._args) => {
    let
      args = Array.from(_args),
      base = args.splice(0, 1)[0],
      i = 0;

    try {
      let
        val = (base && base[args[i]]) || undefined,
        len = args.length;

      if (val) while ((len - 1) > i++) {
        let _val = val[args[i]];
        if (IS_NOT_UNDEFINED(_val)) {
          val = _val;
        } else {
          val = undefined;
          i = len;
        }
      }
      return val;
    } catch (err) {
      return undefined;
    }
  },

  PIPE = async (...args) => new Promise((resolve, reject) => {
    const
      _args = Array.from(args);

    const
      gotError = (val) => IS_OBJECT(val) && val['error'],

      doStep = (val, i = 0) => {
        try {
          if (gotError(val)) {
            _args.length = 0;
            reject(val);
          } else {
            let step = _args[i];
            IS_UNDEFINED(step) ?
              resolve(val) :
              (IS_PROMISE(step) ? step : AS_PROMISE(IS_FUNCTION(step) ? CHECK_AND_CALL(step, val) : step))
                .then(_val => doStep(_val, ++i))
                .catch(err => reject(err));
          }
        } catch (err) {
          reject(err);
        }
      };

    doStep();
  }),

  CREATE_HASH = (str, salt = 10) => bcrypt.hashSync(str, salt),

  COMPARE_HASH = (str, hash) => bcrypt.compareSync(str, hash);

module.exports = {
  IS_UNDEFINED,
  IS_NOT_UNDEFINED,
  IS_NULL,
  IS_NOT_NULL,
  IS_FUNCTION,
  IS_STRING,
  IS_ARRAY,
  IS_OBJECT,
  IS_PROMISE,
  CREATE_HASH,
  COMPARE_HASH,
  AS_ARRAY,
  AS_PROMISE,
  DO_NOTHING,
  CHECK_AND_CALL,
  DIG_OUT,
  PIPE
};

