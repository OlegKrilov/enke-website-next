const
  multer = require('multer'),
  upload = multer();

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  {PIPE} = HELPERS,
  {DB, APP, SMTP} = CONSTANTS.MODULES,
  {GET_MESSAGES, CREATE_MESSAGE, UPDATE_MESSAGE, CREATE_GENERAL_MESSAGE, CREATE_JOB_SEEKING_MESSAGE, CREATE_CASE_STUDY_MESSAGE} = CONSTANTS.ROUTES,
  {MESSAGE_MODEL} = CONSTANTS.DB_MODELS;

module.exports = async (routerCtrl) => {
  const
    app = global[APP],
    dbCtrl = global[DB],
    smtpCtrl = global[SMTP];
  
  const
    // buildEmailHeader = (data) => `${data['firstName'] || ''} ${data['lastName'] || ''} <${data['email'] || ''}>`,
    buildEmailHeader = (data) => 'Info@enkesystems.com',
    buildEmailBody = (text, data) => `
      <div>
        <h3>Hello, ENKE!</h3>
        <p style="margin-bottom: 100px;">${text}</p>
        <p>Regards, ${data['firstName'] || ''} ${data['lastName'] || ''}</p>
        <a>${data['email']}</a>
      </div>
    `;

  const
    {BAD_REQUEST, sendError, checkAuthorization} = routerCtrl;

  app.get(`/${GET_MESSAGES}`, checkAuthorization, (req, res) =>
    PIPE(
      () => dbCtrl.read(MESSAGE_MODEL, Object.assign({},
        req.query['isRead'] === 'true' ? {isRead: true} : {},
        req.query['isRead'] === 'false' ? {isRead: false} : {}
      ), true)
    )
      .then(messages => res.send(messages))
      .catch(err => sendError(err, res))
  );

  app.post(`/${CREATE_MESSAGE}`, (req, res) =>
    PIPE(
      () => ({
        email: req.body['email'],
        text: req.body['text'],
        phone: req.body['phone'],
        theme: req.body['theme'],
        created: new Date().getTime()
      }),
      data => dbCtrl.create(MESSAGE_MODEL, data)
    )
      .then(() => res.send({error: false, statusText: 'Thank you for your appeal!'}))
      .catch(err => sendError(err, res))
  );

  app.post(`/${UPDATE_MESSAGE}`, checkAuthorization, (req, res) => {
    const
      searchQuery = {_id: req.body['_id']},
      updateQuery = req.body['data'];

    PIPE(
      dbCtrl.read(MESSAGE_MODEL, searchQuery),
      message => !message ? BAD_REQUEST : dbCtrl.update(MESSAGE_MODEL, searchQuery, updateQuery),
      wasUpdated => !wasUpdated ? BAD_REQUEST : wasUpdated
    )
      .then(() => res.send({error: false, statusText: `Message ${req.body['id']} was successfully updated.`}))
      .catch(err => sendError(err, res));
  });
  
  app.post(`/${CREATE_GENERAL_MESSAGE}`, upload.none(), (req, res) => {
    const
      body = req.body;
    
    smtpCtrl.sendMessage({
      from: buildEmailHeader(body),
      subject: `General`,
      html: buildEmailBody('Could you please contact me?', body)
    })
      .then(_res => res.send({statusText: 'Thanks! We shall contact you within next 48 hours.'}))
      .catch(err => sendError(err, res));
  });
  
  app.post(`/${CREATE_JOB_SEEKING_MESSAGE}`, upload.single('file'), (req, res) => {
    const
      body = req.body;
    
    smtpCtrl.sendMessage({
      from: buildEmailHeader(body),
      subject: `Want to be a partner`,
      html: buildEmailBody('Please check my CV.', body),
      attachments: [{
        filename: req.file.originalname,
        content: req.file.buffer
      }]
    })
      .then(_res => res.send({statusText: 'Thank you! We shall check your profile and contact you.'}))
      .catch(err => sendError(err, res));
  });
  
  app.post(`/${CREATE_CASE_STUDY_MESSAGE}`, upload.none(), (req, res) => {
    const
      body = req.body;
    
    smtpCtrl.sendMessage({
      from: buildEmailHeader(body),
      subject: `Request Case Study`,
      html: buildEmailBody('Send me this information.', body)
    })
      .then(_res => res.send({
        statusText: 'Your request for the case study has been submitted successfully. We shall get back to you in the next 48 hours.'
      }))
      .catch(err => sendError(err, res));
  });
};

