const
	CONSTANTS = require('../ew-constants'),
	HELPERS = require('../ew-helpers');

const
	{DB, APP} = CONSTANTS.MODULES,
	{BLOG_ITEMS, CREATE_POST, DELETE_POST, READ_POST, COMMENT_POST} = CONSTANTS.ROUTES,
	{POST_MODEL} = CONSTANTS.DB_MODELS,
	{POSTS} = CONSTANTS.DB_COLLECTIONS;

const
	{PIPE, DIG_OUT} = HELPERS;

module.exports = async (routerCtrl) => {
	const
		app = global[APP],
		dbCtrl = global[DB];
	
	const
		{sendError, checkAuthorization, NOT_FOUND, BAD_REQUEST} = routerCtrl;
	
	app.get(`/${BLOG_ITEMS}`, (req, res) => PIPE(
		() => DIG_OUT(req, 'query') || {},
		params => PIPE(
			() => dbCtrl.getModel(POST_MODEL),
			model => model
				.find({})
				.sort({date: DIG_OUT(params, 'sortDesc') ? 1 : -1})
				.limit(Math.ceil(DIG_OUT(params, 'limit') || 8))
				.skip(Math.ceil(DIG_OUT(params, 'skip') || 0))
		)
		)
			.then(data => res.send(data))
			.catch(err => sendError(err, res))
	);
	
	app.get(`/${READ_POST}`, (req, res) => dbCtrl.read(POST_MODEL, {_id: req.query.id})
		.then(data => res.send(data))
		.catch(err => sendError(err, res))
	);
	
	app.post(`/${CREATE_POST}`, checkAuthorization, (req, res) => dbCtrl.create(POST_MODEL, req.body)
		.then(() => res.send({message: 'Post created'}))
		.catch(err => sendError(err, res))
	);
	
	app.post(`/${DELETE_POST}`, checkAuthorization, (req, res) => dbCtrl.delete(POST_MODEL, {
			_id: req.body.id
		})
			.then(() => res.send({message: `Post ${req.body.id} deleted`}))
			.catch(err => sendError(err, res))
	);
	
	app.post(`/${COMMENT_POST}`, (req, res) => PIPE(
		() => dbCtrl.read(POST_MODEL, {_id: req.body.id}),
		post => !post ? BAD_REQUEST : PIPE(
			() => post.comments.push({
				text: req.body.text,
				date: new Date().getTime()
			}),
			() => post.save()
		)
			.then(() => res.send(post.comments))
			.catch(err => sendError(err, res))
	));
	
};
