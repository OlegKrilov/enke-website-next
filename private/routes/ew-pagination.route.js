const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  PAGES_DATA = {
    HOME: require("../seed/home-page.data.json"),
    TECHNOLOGY: require("../seed/technology-page.data.json"),
    PEOPLE: require("../seed/people-page.data.json"),
    ABOUT: require("../seed/about-page.data.json")
  };

const
  {IS_STRING, IS_NULL, IS_OBJECT, PIPE, COMPARE_HASH, CREATE_HASH, DIG_OUT} = HELPERS,
  {DB, APP} = CONSTANTS.MODULES,
  {NAME, PAGE, CONTENT, TYPE, LABEL, FOOTER, HEADER, TIMESTAMP} = CONSTANTS.COMMON,
  {GET_PAGES, GET_PAGE, GET_SECTION, GET_SECTIONS, UPDATE_PAGE, RESET_PAGE, RESET_SECTION} = CONSTANTS.ROUTES,
  {PAGE_MODEL, SECTION_MODEL, USER_MODEL, TOKEN_MODEL} = CONSTANTS.DB_MODELS,
  {USER, TOKEN, LOGIN, PASSWORD, ROLE} = CONSTANTS.AUTH;

module.exports = async (routerCtrl) => {
  const
    app = global[APP],
    dbCtrl = global[DB];

  const
    {BAD_REQUEST, sendError, checkAuthorization} = routerCtrl;

  app.get(`/${GET_PAGE}`, (req, res) => {
    const
      {query} = req;

    let
      pageName = (query.page || '').toUpperCase();

    !pageName ?
      res.status().send({statusText: 'Unknown API'}) :

    PIPE(
      dbCtrl.read(PAGE_MODEL, {[NAME]: pageName}),
      page => !page ? BAD_REQUEST : dbCtrl.read(SECTION_MODEL, {[PAGE]: page[NAME]}, true),
      sections => !sections ? BAD_REQUEST : sections.reduce((agg, d) => Object.assign(agg, {[d[NAME]]: d[CONTENT]}), {})
    )
      .then(pageData => res.send(pageData))
      .catch(err => sendError(err, res));
  });

  app.get(`/${GET_PAGES}`, (req, res) => PIPE(
    dbCtrl.read(SECTION_MODEL, {}, true),

    sections => sections.reduce((agg, d) => {
      agg[d[PAGE]] = agg[d[PAGE]] || {};
      agg[d[PAGE]][CONTENT] = agg[d[PAGE]][CONTENT] || {};
      agg[d[PAGE]][CONTENT][d[NAME]] = d[CONTENT];

      return agg;
    }, {}),

    (pagesData) => PIPE(
      dbCtrl.read(PAGE_MODEL, {}, true),
      pages => pages.reduce((agg, d) => {
        Object.assign(pagesData[d[NAME]], {
          [NAME]: d[NAME],
          [LABEL]: d[LABEL],
          [TYPE]: d[TYPE],
          [HEADER]: d[HEADER],
          [FOOTER]: d[FOOTER]
        });

        return agg;
      }, pagesData)
    ))
      .then(data => res.send(data))
      .catch(err => sendError(err, res))
  );

  app.post(`/${UPDATE_PAGE}`, checkAuthorization, (req, res) => {
    const
      pageQuery = {[NAME]: req.body['page']},
      sectionQuery = {[NAME]: req.body['section'], [PAGE]: req.body['page']},
      updateQuery = req.body['data'];

    PIPE(
      () => dbCtrl.read(PAGE_MODEL, pageQuery),
      page => !page ? BAD_REQUEST : dbCtrl.read(SECTION_MODEL, sectionQuery),
      section => !section ? BAD_REQUEST : Object.assign(section.content, updateQuery),
      query => !query ? BAD_REQUEST : dbCtrl.update(SECTION_MODEL, sectionQuery, {content: query})
    )
      .then(data => res.send(data))
      .catch(err => sendError(err, res))
  });

  app.post(`/${RESET_SECTION}`, checkAuthorization, (req, res) => {
    const
      filterQuery = {[PAGE]: req.body['page'].toUpperCase(), [NAME]: req.body['section'].toUpperCase()},
      updateQuery = DIG_OUT(PAGES_DATA, filterQuery[PAGE], filterQuery[NAME]);

    PIPE(
      () => dbCtrl.read(SECTION_MODEL, filterQuery),
      section => !section ? BAD_REQUEST : PIPE(
        () => section[CONTENT] = updateQuery,
        () => section.save()
      )
    )
      .then(() => res.send({'success': true}))
      .catch(err => sendError(err, res));
  });

};
