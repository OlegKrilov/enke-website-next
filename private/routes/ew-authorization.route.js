const
  jwt = require('jsonwebtoken');

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  {IS_STRING, IS_NULL, IS_OBJECT, PIPE, COMPARE_HASH, CREATE_HASH, DIG_OUT} = HELPERS,
  {DB, APP} = CONSTANTS.MODULES,
  {NAME, PAGE, CONTENT, TYPE, LABEL, FOOTER, HEADER, TIMESTAMP} = CONSTANTS.COMMON,
  {SIGN_IN, SIGN_OUT, CHECK_TOKEN, CLEAR_TOKEN} = CONSTANTS.ROUTES,
  {PAGE_MODEL, SECTION_MODEL, USER_MODEL, TOKEN_MODEL} = CONSTANTS.DB_MODELS,
  {USER, TOKEN, LOGIN, PASSWORD, ROLE} = CONSTANTS.AUTH;

module.exports = async (routerCtrl) => {
  const
    app = global[APP],
    dbCtrl = global[DB];

  const
    {
      USER_KEY, TOKEN_SECRET, NOT_AUTHORIZED, BAD_CREDENTIALS,
      sendError, getAuthHeader, getHeaderProperty, getAuthCredentials, getTimestamp, createToken, checkAuthorization, checkCredentials
    } = routerCtrl;

  const
    getUserData = user => ({
      [LOGIN]: user[LOGIN],
      [ROLE]: user[ROLE]
    });

  app.post(`/${SIGN_IN}`, checkCredentials,
    (req, res) => {
      const cashedValues = {};
      PIPE(
        dbCtrl.read(USER_MODEL, {[LOGIN]: req.body[LOGIN]}),
        user => !user ? BAD_CREDENTIALS : Object.assign(cashedValues, {
          [LOGIN]: user[LOGIN],
          [TOKEN]:  createToken(user[LOGIN])
        }),
        () => dbCtrl.delete(TOKEN_MODEL, {[LOGIN]: cashedValues[LOGIN]}),
        () => dbCtrl.create(TOKEN_MODEL, cashedValues[TOKEN]),
        () => dbCtrl.update(USER_MODEL, {[LOGIN]: cashedValues[LOGIN]}, {[TOKEN]: cashedValues[TOKEN][TOKEN]}),
        user => !user ? BAD_CREDENTIALS : getUserData(user)
      )
        .then(user => res.set({[USER_KEY]: cashedValues[TOKEN][TOKEN]}).send(user))
        .catch(err => sendError(err));
  });

  app.post(`/${SIGN_OUT}`, checkAuthorization,
    (req, res) => PIPE(
      () => getAuthHeader(req),
      tokenQuery => IS_NULL(tokenQuery) ? NOT_AUTHORIZED : dbCtrl.delete(TOKEN_MODEL, tokenQuery)
    )
      .then(() => res.send({message: 'Signed out'}))
      .catch(err => sendError(err, res))
  );

  app.post(`/${CHECK_TOKEN}`, checkAuthorization,
    (req, res) => PIPE(
      getAuthHeader(req),
      token => dbCtrl.read(TOKEN_MODEL, token),
      tokenEntity => !tokenEntity ? NOT_AUTHORIZED : dbCtrl.read(USER_MODEL, {[LOGIN]: tokenEntity[LOGIN]}),
      user => !user ? NOT_AUTHORIZED : getUserData(user)
    )
      .then(user => res.send(user))
      .catch(err => sendError(err, res))
  );
};

