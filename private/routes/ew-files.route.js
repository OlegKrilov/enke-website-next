const
  bodyParser = require('body-parser'),
  multer = require('multer'),
  appRoot = require('app-root-path'),
  fs = require('fs');

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  DESTINATION_FOLDER = 'uploaded',
  {IS_STRING, IS_NULL, IS_OBJECT, PIPE, COMPARE_HASH, CREATE_HASH, DIG_OUT} = HELPERS,
  {DB, APP} = CONSTANTS.MODULES,
  {NAME, PAGE, CONTENT, TYPE, LABEL, FOOTER, HEADER, TIMESTAMP} = CONSTANTS.COMMON,
  {LOAD_FILE, SAVE_FILE, SAVE_FILES} = CONSTANTS.ROUTES,
  {PAGE_MODEL, SECTION_MODEL, USER_MODEL, TOKEN_MODEL} = CONSTANTS.DB_MODELS,
  {USER, TOKEN, LOGIN, PASSWORD, ROLE} = CONSTANTS.AUTH;

  // multipleStorage = multer.diskStorage({
  //   destination: (var upload = multer();)
  //
  // });

module.exports = async (routerCtrl) => {
  const
    app = global[APP],
    dbCtrl = global[DB];

  const
    {sendError, checkAuthorization, getFormFields, NOT_FOUND} = routerCtrl;

  app.post(`/${SAVE_FILE}`, checkAuthorization, (req, res) => {
    const
      storage = multer.diskStorage({
        destination: (req, file, cb) => cb(null, DESTINATION_FOLDER),
        filename: (req, file, cb) => cb(null, file.originalname)
      }),
      upload = multer({ storage: storage }).single('file');
    
    upload(req, res, (err) => {
      err ?
        sendError(err, res) :
        res.send({'ok': 200});
    });
  });

  app.get(`/${LOAD_FILE}`, (req, res) => {
    let
      fileName = req.query.file,
      fullPath = `${appRoot}/${DESTINATION_FOLDER}/${fileName}`;

    fs.existsSync(fullPath) ?
      res.download(fullPath, fileName) :
      sendError(NOT_FOUND, res);
  });
  
  app.post(`/${SAVE_FILES}/:bucketName`, checkAuthorization, (req, res) => {
    const
      bucketName = req.params['bucketName'] || 'test-test',
      fullPath = `${DESTINATION_FOLDER}/${bucketName}`;
  
    !fs.existsSync(DESTINATION_FOLDER) && fs.mkdirSync(DESTINATION_FOLDER);
    !fs.existsSync(fullPath) && fs.mkdirSync(fullPath);

    const
      bucketStorage = multer.diskStorage({
        destination: (req, file, cb) => cb(null, fullPath),
        filename: (req, file, cb) => cb(null, file.originalname)
      }),
      bucketUpload = multer({storage: bucketStorage}).array('images', 100);

    bucketUpload(req, res, (err) => err ? sendError(err, res) : res.send({'ok': 200}));
  });

};
