const
  jwt = require('jsonwebtoken'),
  path = require('path');

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers'),
  AUTH_ROUTE = require('../routes/ew-authorization.route'),
  PAGINATION_ROUTE = require('../routes/ew-pagination.route'),
  MESSAGES_ROUTE = require('../routes/ew-messages.route'),
  FILES_ROUTE = require('../routes/ew-files.route'),
  BLOG_ROUTE = require('../routes/ew-blog.route');

const
  {AS_ARRAY, IS_NULL, COMPARE_HASH, IS_OBJECT, PIPE, DIG_OUT} = HELPERS,
  {DB, APP} = CONSTANTS.MODULES,
  {TIMESTAMP} = CONSTANTS.COMMON,
  {USER_MODEL, TOKEN_MODEL} = CONSTANTS.DB_MODELS,
  {TOKEN, LOGIN, PASSWORD} = CONSTANTS.AUTH,
  {ANY_ROUTE, RESET_DB} = CONSTANTS.ROUTES;

const
  USER_KEY = '_usr_',
  TOKEN_SECRET = '_enke4ever_';

const
  BAD_REQUEST = {
    error: true,
    status: 400,
    statusText: 'Bad request'
  },
  NOT_AUTHORIZED = {
    error: true,
    status: 401,
    statusText: 'Not authorized'
  },
  BAD_CREDENTIALS = {
    error: true,
    status: 403,
    statusText: 'Login and/or password incorrect'
  },
  NOT_FOUND = {
    error: true,
    status: 404,
    statusText: 'File not found'
  },
  SERVER_ERROR = {
    error: true,
    status: 500,
    statusText: 'Something went wrong'
  };

module.exports = async () => {
  let
    app = global[APP],
    dbCtrl = global[DB],
    ctrl = new EW_ROUTER_CONTROLLER();

  await AUTH_ROUTE(ctrl);
  await PAGINATION_ROUTE(ctrl);
  await MESSAGES_ROUTE(ctrl);
  await FILES_ROUTE(ctrl);
  await BLOG_ROUTE(ctrl);
  
  app.get('**', (req, res) => res.sendFile(path.resolve(__dirname, '../../build/index.html')));

  app.post(`/${RESET_DB}`, ctrl.checkAuthorization,
    (req, res) => dbCtrl.resetCollections()
      .then(() => res.send({
        error: false,
        statusText: 'DB Cleared'
      }))
      .catch(err => ctrl.sendError(err, res))
  );

  return ctrl;
};

function EW_ROUTER_CONTROLLER () {
  const
    ctrl = this;

  const
    app = global[APP],
    dbCtrl = global[DB];

  const
    sendError = (err, res) => {
      console.log(err);
      res.status(err.status || 500).send({statusText: IS_OBJECT(err) ? err.statusText || err.text: err});
    },

    getHeaderProperty = (req, prop) => DIG_OUT(req, 'headers', prop),

    getAuthHeader = (req) => {
      const key = getHeaderProperty(req, USER_KEY);
      return key ? {[TOKEN]: key} : null;
    },

    getAuthCredentials = (req) => ({
      [LOGIN]: req.body[LOGIN],
      [PASSWORD]: req.body[PASSWORD]
    }),

    getTimestamp = () => new Date().getTime(),

    getFormFields = (fields = null) => (req, res, next) => {
      let
        bodyStr = '';
      
      req
        .on('data', chunk => {
          bodyStr += chunk;
        })
        .on('end', () => {
          console.log(bodyStr);
          
          const
            body = JSON.parse(bodyStr);
          
          req.body = req.body || {};
          
          Object.keys(body)
            .filter(key => IS_NULL(fields) ? true : !!AS_ARRAY(fields).includes(key))
            .forEach(key => req.body[key] = body[key]);
          
          
          next();
        });
    },
    
    createToken = (login) => ({
      [LOGIN]: login,
      [TOKEN]: jwt.sign({[LOGIN]: login}, TOKEN_SECRET),
      [TIMESTAMP]: getTimestamp()
    }),

    checkAuthorization = (req, res, next) => PIPE(
      getAuthHeader(req),
      tokenQuery => IS_NULL(tokenQuery) ? NOT_AUTHORIZED : dbCtrl.read(TOKEN_MODEL, tokenQuery),
      tokenEntity => !tokenEntity ? NOT_AUTHORIZED : dbCtrl.read(USER_MODEL, {[LOGIN]: tokenEntity[LOGIN]}),
      user => !user ? NOT_AUTHORIZED : dbCtrl.update(TOKEN_MODEL, getAuthHeader(req), {[TIMESTAMP]: new Date().getTime()})
    )
      .then(() => next())
      .catch(err => sendError(err, res)),

    checkCredentials = (req, res, next) => PIPE(
      () => dbCtrl.read(USER_MODEL, {[LOGIN]: req.body[LOGIN]}),
      user => {
        console.log(user);
        return user;
      },

      user => !user ? BAD_CREDENTIALS : COMPARE_HASH(req.body[PASSWORD], user[PASSWORD]),
      isValid => !isValid ? BAD_CREDENTIALS : true
    )
      .then(() => next())
      .catch(err => sendError(err, res));

  Object.defineProperties(ctrl, {
    USER_KEY: {
      get: () => USER_KEY
    },
    TOKEN_SECRET: {
      get: () => TOKEN_SECRET
    },
    BAD_REQUEST: {
      get: () => BAD_REQUEST
    },
    NOT_AUTHORIZED: {
      get: () => NOT_AUTHORIZED
    },
    BAD_CREDENTIALS: {
      get: () => BAD_CREDENTIALS
    },
    NOT_FOUND: {
      get: () => NOT_FOUND
    },
    SERVER_ERROR: {
      get: () => SERVER_ERROR
    },
    sendError: {
      value: sendError
    },
    getHeaderProperty: {
      value: getHeaderProperty
    },
    getAuthHeader: {
      value: getAuthHeader
    },
    getAuthCredentials: {
      value: getAuthCredentials
    },
    getTimestamp: {
      value: getTimestamp
    },
    getFormFields: {
      value: getFormFields
    },
    createToken: {
      value: createToken
    },
    checkAuthorization: {
      value: checkAuthorization,
    },
    checkCredentials: {
      value: checkCredentials
    }
  });

  /** Test Routes */
  app.get('/test', (req, res) => {
    console.log('TEST-GET');
    res.send({ok: 200});
  });

  app.post('/test', (req, res) => {
    PIPE(
      getAuthHeader(req),
      authHeader => console.log(authHeader),
      () => getHeaderProperty(req, 'test'),
      testHeader => console.log(testHeader),
      () => getHeaderProperty(req, 'test2')
    )
      .then(_res => res.send(_res))
      .catch(err => sendError(err, res));
  });

  app.get('/error', (req, res) => {
    console.log('ERROR-GET');
    sendError(SERVER_ERROR, res);
  });

  app.post('/error', (req, res) => {
    console.log('ERROR-POST');
    sendError(SERVER_ERROR, res);
  });

}

