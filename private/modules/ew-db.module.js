const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const
  refreshPageModel = require('../models/ew-page.model'),
  refreshUserModel = require('../models/ew-user.model'),
  refreshSectionModel = require('../models/ew-section.model'),
  refreshTokenModel = require('../models/ew-token.model'),
  refreshMessageModel = require('../models/ew-message.model'),
  refreshPostModel = require('../models/ew-post.model');

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  {DB_NAME, DB_HOST, DB_PORT} = CONSTANTS.CONFIG,
  {PAGE_MODEL, MESSAGE_MODEL, SECTION_MODEL, USER_MODEL, TOKEN_MODEL, POST_MODEL} = CONSTANTS.DB_MODELS;

const
  {PIPE, IS_NULL, IS_NOT_NULL, IS_OBJECT, CREATE_HASH, CHECK_AND_CALL} = HELPERS;

module.exports = () => new EW_DB_Controller();

function EW_DB_Controller () {

  const
    ctrl = this,
    models = {},
    modelResetPipes = {};
  
  const
    getDefaultSettings = () => ({
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true,
      useUnifiedTopology: true
    });

  Object.defineProperties(ctrl, {

    connect: {
      value: (
          connectionPath = `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`,
          connectionSettings = {}
        ) => new Promise((resolve, reject) => {
          ctrl.isConnected ?
            resolve(ctrl) :
            mongoose.connect(connectionPath, Object.assign(getDefaultSettings(), connectionSettings))
              .then(async () => {
                modelResetPipes[PAGE_MODEL] = await refreshPageModel(ctrl);
                modelResetPipes[USER_MODEL] = await refreshUserModel(ctrl);
                modelResetPipes[SECTION_MODEL] = await refreshSectionModel(ctrl);
                modelResetPipes[TOKEN_MODEL] = await refreshTokenModel(ctrl);
                modelResetPipes[MESSAGE_MODEL] = await refreshMessageModel(ctrl);
                modelResetPipes[POST_MODEL] = await refreshPostModel(ctrl);
                resolve(ctrl);
              })
              .catch(err => reject(err))
        })
    },

    disconnect: {
      value: () => mongoose.disconnect()
    },

    isConnected: {
      get: () => mongoose.connection.readyState === 1
    },

    create: {
      value: (modelName, datum) => new Promise((resolve, reject) => ctrl.getModel(modelName)
        .then(model => new model(datum)
          .save()
          .then(res => resolve(res))
          .catch(err => reject(err))
        )
        .catch(err => reject(err))
      )
    },

    read: {
      value: (modelName, filter, getMany = false) => new Promise((resolve, reject) => ctrl.getModel(modelName)
        .then(model => model[getMany ? 'find' : 'findOne'](filter)
          .then(res => resolve(res))
          .catch(err => reject(err)))
        .catch(err => reject(err))
      )
    },

    update: {
      value: (modelName, filter, update) => new Promise((resolve, reject) => ctrl.read(modelName, filter)
        .then(datum => {
          IS_OBJECT(datum) && Object.keys(update).forEach(key => datum[key] = update[key]);
          datum.save()
            .then(res => resolve(res))
            .catch(err => reject(err));
        })
        .catch(err => reject(err))
      )
    },

    delete: {
      value: (modelName, filter) => PIPE(
        () => ctrl.getModel(modelName),
        model => model.findOneAndDelete(filter)
        // new Promise((resolve, reject) => ctrl.getModel(modelName)
        // .then(model => model.findOneAndDelete(filter)
        //   .then(res => resolve(res))
        //   .catch(err => reject(err))
        // ).catch(err => reject(err))
      )
    },

    count: {
      value: modelName => new Promise((resolve, reject) => ctrl.getModel(modelName)
        .then(model => model.countDocuments({}, (err, num) => err ? reject(err) : resolve(num)))
        .catch(err => reject(err))
      )
    },

    getModel: {
      value: modelName => new Promise((resolve, reject) => {
        let model = models[modelName] || null;
        IS_NOT_NULL(model) ? resolve(model) : reject({
          statusCode: 500,
          statusText: `Model ${modelName} is not defined`
        });
      })
    },

    getCollection: {
      value: collectionName => new Promise((resolve, reject) => ctrl.getModel(collectionName.replace(/s$/))
        .then(model => resolve(model.collection))
        .catch(err => reject(err))
      )
    },
    
    addModel: {
      value: (modelName, modelData) => new Promise((resolve, reject) => {
        models[modelName] = modelData;
        resolve(ctrl);
      })
    },

    resetCollections: {
      value: () => new Promise((resolve, reject) => {
        const
          keys = Object.keys(modelResetPipes).filter(key => key !== TOKEN_MODEL),
          resetCollection = (i = 0) => {
            let modelResetPipe = modelResetPipes[keys[i]];
            modelResetPipe ?
              CHECK_AND_CALL(modelResetPipe)
                .then(() => resetCollection(i + 1))
                .catch(err => reject(err)) :
              resolve({
                error: false,
                statusText: `All collections were reset to default.`
              });
          };
        resetCollection();
      })
    }

  });
}

