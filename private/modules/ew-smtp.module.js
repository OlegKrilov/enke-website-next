const
	CONSTANTS = require('../ew-constants'),
	HELPERS = require('../ew-helpers');
	
const
	SMTP_CONFIG = require('../../smtp.config.json');

const
	sendMail = require('sendmail')(),
	nodemailer = require('nodemailer');

module.exports = () => new EW_SMTP_Controller();

function EW_SMTP_Controller () {
	
	const
		ctrl = this;
	
	let
		_isConnected = false;
	
	Object.defineProperties(ctrl, {
		core: {
			value: null,
			writable: true
		},
		
		connect: {
			value: () => HELPERS.PIPE(
				() => ctrl.core = nodemailer.createTransport({
					host: SMTP_CONFIG.HOST,
					port: SMTP_CONFIG.PORT,
					secure: true,
					auth: {
						user: SMTP_CONFIG.USER,
						pass: SMTP_CONFIG.PSWD
					},
					tls: {
						rejectUnauthorized: false
					}
				}),
				() => _isConnected = true,
				() => ctrl
			)
		},
		disconnect: {
			value: () => HELPERS.PIPE(
				() => _isConnected = false,
				() => ctrl
			)
		},
		isConnected: {
			get: () => _isConnected
		},
		sendMessage: {
			value: data => new Promise((resolve, reject) => {
				ctrl.core ?
					ctrl.core && ctrl.core.sendMail(Object.assign({to: SMTP_CONFIG.TARGET}, data),
					(err, info) => err ? reject(err) : resolve(info)) :
					reject({errorStatus: 500, error: 'SMTP server is not running'}
				);
			})
		}
	});
}

