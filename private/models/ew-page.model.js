const
  mongoose = require('mongoose'),
  model = mongoose.model,
  Schema = mongoose.Schema;

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers'),
  PAGES_DATA = {
    HOME: require("../seed/home-page.data.json"),
    TECHNOLOGY: require("../seed/technology-page.data.json"),
    PEOPLE: require("../seed/people-page.data.json"),
    ABOUT: require("../seed/about-page.data.json")
  };

const
  {PIPE} = HELPERS,
  {PAGE_MODEL} = CONSTANTS.DB_MODELS,
  {PAGES, USERS} = CONSTANTS.DB_COLLECTIONS,
  {NAME, TYPE, LABEL, DATA, CONTENT, HEADER, FOOTER} = CONSTANTS.COMMON;

const
  createDefaultPages = (ctrl) => PIPE(
    () => console.log('Creating default pages...'),
    () => ctrl.getModel(PAGE_MODEL),
    model => model.insertMany(Object.keys(PAGES_DATA).map(pageKey => ({
      [NAME]: pageKey,
      [LABEL]: pageKey,
      [TYPE]: 'Public Page',
      [CONTENT]: Object.keys(PAGES_DATA[pageKey]).map(sectionKey => sectionKey),
      [FOOTER]: true,
      [HEADER]: ['HOME', 'TECHNOLOGY', 'PEOPLE', 'ABOUT'].some(n => n === pageKey)
    })))
  ),
  getModelResetPipe = ctrl => () => PIPE(
    () => ctrl.getModel(PAGE_MODEL),
    model => model.deleteMany({}),
    () => createDefaultPages(ctrl)
  );

module.exports = (ctrl) => PIPE(

  () => console.log('Refreshing Page Model...'),

  () => new model(PAGE_MODEL, new Schema({
      [NAME]: {
        type: String,
        unique: true,
        required: true
      },
      [LABEL]: String,
      [TYPE]: {
        type: String,
        default: 'CUSTOM_PAGE'
      },
      [CONTENT]: [String],
      [HEADER]: {
        type: Boolean,
        default: false,
      },
      [FOOTER]: {
        type: Boolean,
        default: false,
      }
    }, {collection: PAGES})),

  pageModel => ctrl.addModel(PAGE_MODEL, pageModel),

  () => ctrl.count(PAGE_MODEL),

  num => !num && createDefaultPages(ctrl),

  () => console.log('Done.'),

  () => getModelResetPipe(ctrl)

);

