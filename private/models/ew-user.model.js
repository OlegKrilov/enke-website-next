const
  mongoose = require('mongoose'),
  model = mongoose.model,
  Schema = mongoose.Schema;

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  {CREATE_HASH, PIPE} = HELPERS,
  {USER_MODEL} = CONSTANTS.DB_MODELS,
  {USERS} = CONSTANTS.DB_COLLECTIONS,
  {LOGIN, PASSWORD, ROLE, TOKEN} = CONSTANTS.AUTH;

const
  createDefaultUser = (ctrl) => PIPE(
    () => console.log('Creating default user...'),
    () => ctrl.create(USER_MODEL, {
      [LOGIN]: 'admin',
      [PASSWORD]: CREATE_HASH('enke4ever'),
      [ROLE]: 'Temporary User'
    }),
    () => ctrl.create(USER_MODEL, {
      [LOGIN]: 'olegkrilov',
      [PASSWORD]: CREATE_HASH('enke_dev'),
      [ROLE]: 'Developer'
    })
  ),
  getModelResetPipe = ctrl => () => PIPE(
    () => ctrl.getModel(USER_MODEL),
    model => model.deleteMany({}),
    () => createDefaultUser(ctrl)
  );

module.exports = (ctrl) => PIPE(

  () => console.log('Refreshing User Model...'),

  () => new model(USER_MODEL, new Schema({
    [LOGIN]: {
      type: String,
      required: true,
      unique: true
    },
    [PASSWORD]: {
      type: String,
      required: true,
      unique: true
    },
    [ROLE]: {
      type: String,
      required: true
    },
    [TOKEN]: String
  }, {collection: USERS})),

  userModel => ctrl.addModel(USER_MODEL, userModel),

  () => ctrl.count(USER_MODEL),

  num => !num && createDefaultUser(ctrl),

  () => console.log('Done'),

  () => getModelResetPipe(ctrl)

);

