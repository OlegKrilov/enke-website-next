const
  mongoose = require('mongoose'),
  model = mongoose.model,
  Schema = mongoose.Schema;

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers'),
  PAGES_DATA = {
    HOME: require("../seed/home-page.data.json"),
    TECHNOLOGY: require("../seed/technology-page.data.json"),
    PEOPLE: require("../seed/people-page.data.json"),
    ABOUT: require("../seed/about-page.data.json")
  };

const
  {PIPE} = HELPERS,
  {SECTION_MODEL} = CONSTANTS.DB_MODELS,
  {SECTIONS} = CONSTANTS.DB_COLLECTIONS,
  {NAME, PAGE, CONTENT} = CONSTANTS.COMMON;

const
  createDefaultSections = (ctrl) => PIPE(
    () => console.log('Creating default sections...'),
    () => ctrl.getModel(SECTION_MODEL),
    model => model.insertMany(Object.keys(PAGES_DATA).reduce(
      (sections, pageKey) => sections.concat(Object.keys(PAGES_DATA[pageKey]).map(
        (sectionKey) => ({
          [NAME]: sectionKey,
          [PAGE]: pageKey,
          [CONTENT]: PAGES_DATA[pageKey][sectionKey]
        })
      )), [])
    )
  ),
  getModelResetPipe = ctrl => () => PIPE(
    () => ctrl.getModel(SECTION_MODEL),
    model => model.deleteMany({}),
    () => createDefaultSections(ctrl)
  );

module.exports = (ctrl) => PIPE(

  () => console.log('Refreshing Section Model...'),

  () => new model(SECTION_MODEL, new Schema({
      [NAME]: {
        type: String,
        required: true,
        index: true
      },
      [PAGE]: {
        type: String,
        required: true,
        index: true
      },
      [CONTENT]: {
        type: Schema.Types.Mixed,
        required: true
      }
    }, {collection: SECTIONS})
  ),

  sectionModel => ctrl.addModel(SECTION_MODEL, sectionModel),

  () => ctrl.count(SECTION_MODEL),

  num => !num && createDefaultSections(ctrl),

  () => console.log('Done.'),

  () => getModelResetPipe(ctrl)

);
