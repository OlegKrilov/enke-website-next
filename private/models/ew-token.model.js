const
  mongoose = require('mongoose'),
  model = mongoose.model,
  Schema = mongoose.Schema;

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  {PIPE} = HELPERS,
  {TOKEN_MODEL} = CONSTANTS.DB_MODELS,
  {TOKENS} = CONSTANTS.DB_COLLECTIONS,
  {TIMESTAMP} = CONSTANTS.COMMON,
  {LOGIN, TOKEN} = CONSTANTS.AUTH;

const
  getModelResetPipe = ctrl => () => PIPE(
    () => ctrl.model(TOKEN_MODEL),
    model => model.deleteMany({})
  );

module.exports = ctrl => PIPE(

  () => console.log('Refreshing Token Model...'),

  () => new model(TOKEN_MODEL, new Schema({
    [LOGIN]: {
      type: String,
      required: true,
      unique: true
    },
    [TOKEN]: {
      type: String,
      required: true,
      unique: true
    },
    [TIMESTAMP]: {
      type: Number,
      required: true,
    }
  }, {collection: TOKENS})),

  tokenModel => ctrl.addModel(TOKEN_MODEL, tokenModel),

  () => console.log('Done.'),

  () => getModelResetPipe(ctrl)

);

