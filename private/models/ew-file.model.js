const
  mongoose = require('mongoose'),
  model = mongoose.model,
  Schema = mongoose.Schema;

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  {PIPE} = HELPERS,
  {FILE_MODEL} = CONSTANTS.DB_MODELS,
  {FILES} = CONSTANTS.DB_COLLECTIONS,
  {NAME, TYPE, LABEL, DATA, CONTENT, HEADER, FOOTER, DATE} = CONSTANTS.COMMON;

module.exports = PIPE(

  () => console.log('Refreshing uploaded model...'),

  () => new model(FILE_MODEL, new Schema({
    [NAME]: {
      type: String,
      required: true,
      unique: true
    },
    [DATA]: {


    }


  }, {collection: FILES}))

);
