const
	mongoose = require('mongoose'),
	model = mongoose.model,
	Schema = mongoose.Schema;

const
	CONSTANTS = require('../ew-constants'),
	HELPERS = require('../ew-helpers');

const
	{PIPE} = HELPERS,
	{POST_MODEL} = CONSTANTS.DB_MODELS,
	{POSTS} = CONSTANTS.DB_COLLECTIONS,
	{NAME, TYPE, LABEL, DATA, CONTENT, HEADER, FOOTER, DATE} = CONSTANTS.COMMON;

const
	getModelResetPipe = ctrl => () => PIPE(
		() => ctrl.getModel(POST_MODEL),
		model => model.deleteMany({})
	);

module.exports = (ctrl) => PIPE(
	
	() => console.log('Refreshing BlogPost Model...'),
	
	() => new model(POST_MODEL, new Schema({
		id: {
			type: String,
			required: true,
			unique: true
		},
		author: String,
		header: {
			type: String,
			required: true,
			unique: true
		},
		content: {
			type: [],
			required: true
		},
		date: {
			type: Number,
			required: true
		},
		likes: {
			type: Number,
			default: 0
		},
		comments: {
			type: Array,
			default: []
		},
		tags: {
			type: Array,
			default: []
		}
	}, {collection: POSTS})),
	
	postModel => ctrl.addModel(POST_MODEL, postModel),

	() => console.log('Done.'),
	
	() => getModelResetPipe(ctrl)

);

