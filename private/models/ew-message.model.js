const
  mongoose = require('mongoose'),
  model = mongoose.model,
  Schema = mongoose.Schema;

const
  CONSTANTS = require('../ew-constants'),
  HELPERS = require('../ew-helpers');

const
  {PIPE} = HELPERS,
  {MESSAGE_MODEL} = CONSTANTS.DB_MODELS,
  {MESSAGES} = CONSTANTS.DB_COLLECTIONS;

const
  createDefaultMessage = ctrl => PIPE(
    () => console.log('Creating default message...'),
    () => ctrl.create(MESSAGE_MODEL, {
      email: 'general@enkesystems.com',
      text: 'THIS IS THE TEST MESSAGE',
      created: new Date().getTime()
    })
  ),

  getModelResetPipe = ctrl => () => PIPE(
    () => ctrl.getModel(MESSAGE_MODEL),
    model => model.deleteMany({}),
    () => createDefaultMessage(ctrl)
  );


module.exports = ctrl => PIPE(

  () => console.log('Refreshing Message Model...'),

  () => new model(MESSAGE_MODEL, new Schema({
      email: {
        type: String,
        required: true
      },
      phone: {
        type: String,
        default: 'None'
      },
      text: {
        type: String,
        required: true
      },
      theme: {
        type: String,
        default: 'General'
      },
      created: {
        type: Number,
        required: true
      },
      isRead: {
        type: Boolean,
        default: false
      },
      opened: {
        type: Schema.Types.Mixed,
        default: null
      },
    }, {collection: MESSAGES})),

  messageModel => ctrl.addModel(MESSAGE_MODEL, messageModel),

  () => ctrl.count(MESSAGE_MODEL),

  num => !num && createDefaultMessage(ctrl),

  () => console.log('Done.'),

  () => getModelResetPipe(ctrl)

);


