const
  envVariables = require('dotenv').config().parsed;

module.exports = {

  COMMON: {
    NAME: 'name',
    LABEL: 'label',
    TYPE: 'type',
    DATA: 'data',
    PAGE: 'page',
    CONTENT: 'content',
    HEADER: 'header',
    FOOTER: 'footer',
    DATE: 'date',
    TIMESTAMP: 'timestamp'
  },

  CONFIG: ['NODE_ENV', 'PORT', 'DB_NAME', 'DB_HOST', 'DB_PORT', 'SMTP_HOST', 'SMTP_PORT'].reduce(
    (config, prop) => Object.assign(config, {[prop]: (envVariables && envVariables[prop]) || (process.env && process.env[prop])}), {}),

  MODULES: {
    APP: 'app',
    DB: 'db',
    API: 'api',
    SERVER: 'server',
    ROUTER: 'router',
    SMTP: 'smtp'
  },

  DB_MODELS: {
    USER_MODEL: 'EW_User',
    PAGE_MODEL: 'EW_Page',
    SECTION_MODEL: 'EW_Section',
    TOKEN_MODEL: 'EW_Token',
    MESSAGE_MODEL: 'EW_Message',
    FILE_MODEL: 'EW_File',
    POST_MODEL: 'EW_Post'
  },

  DB_COLLECTIONS: {
    USERS: 'EW_Users',
    PAGES: 'EW_Pages',
    SECTIONS: 'EW_Sections',
    TOKENS: 'EW_Tokens',
    MESSAGES: 'EW_Messages',
    FILES: 'EW_Files',
    POSTS: 'EW_Posts'
  },

  TYPES: {
    FUNCTION: 'function',
    STRING: 'string',
    OBJECT: 'object'
  },

  AUTH: {
    USER: 'user',
    LOGIN: 'login',
    PASSWORD: 'password',
    TOKEN: 'token',
    ROLE: 'role'
  },

  ROUTES: {

    /** Auth */
    SIGN_IN: 'sign-in',
    SIGN_OUT: 'sign-out',
    CHECK_TOKEN: 'check-token',
    CLEAR_TOKEN: 'clear-token',

    /** Pagination */
    GET_PAGE: 'get-page',
    GET_PAGES: 'get-pages',
    GET_SECTION: 'get-section',
    GET_SECTIONS: 'get-sections',
    UPDATE_PAGE: 'update-page',
    RESET_PAGE: 'reset-page',
    RESET_SECTION: 'reset-section',

    /** Messages */
    GET_MESSAGES: 'get-messages',
    CREATE_MESSAGE: 'create-message',
    UPDATE_MESSAGE: 'update-message',
    CREATE_GENERAL_MESSAGE: 'create-general-message',
    CREATE_JOB_SEEKING_MESSAGE: 'create-job-seeking-message',
    CREATE_CASE_STUDY_MESSAGE: 'create-case-study-message',
    
    /** Blog */
    BLOG_ITEMS: 'blog-items',
    READ_POST: 'read-post',
    CREATE_POST: 'create-post',
    DELETE_POST: 'delete-post',
    EDIT_POST: 'edit-post',
    COMMENT_POST: 'comment-post',
    
    /** Global */
    ANY_ROUTE: '*',
    RESET_DB: 'reset-db',
    LOAD_FILE: 'load-file',
    SAVE_FILE: 'save-file',
    SAVE_FILES: 'save-files'
  }
};
