const
  path = require('path'),
  express = require('express'),
  bodyParser = require('body-parser'),
  cookieParser = require('cookie-parser'),
  cors = require('cors');

const
  CONSTANTS = require('./private/ew-constants'),
  HELPERS = require('./private/ew-helpers'),
  {PORT, NODE_ENV} = CONSTANTS.CONFIG,
  {APP, DB, SERVER, ROUTER, SMTP} = CONSTANTS.MODULES,
  STATIC_SOURCE = path.join(__dirname, '/build');

const
  dbModule = require('./private/modules/ew-db.module'),
  smtpModule = require('./private/modules/ew-smtp.module'),
  configRoutes = require('./private/modules/ew-router.module');

(async function ENKE_SYSTEMS_WEBSITE () {
  global[APP] = await createApp();
  global[DB] = await connectToDatabase();
  global[SMTP] = await connectToSMTPServer();
  global[SERVER] = await runServer();
})();

async function createApp () {
  const app = express();

  console.log('Configuring app...');

  app.use(cors({
    'allowedHeaders': '*',
    'exposedHeaders': '*',
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
  }));

  app.use(bodyParser.json({
    parameterLimit: 500000,
    limit: '50mb',
    extended: true
  }));

  app.use(bodyParser.urlencoded({
    parameterLimit: 1000000,
    limit: '100mb',
    extended: false
  }));
  
  app.use(express.static(__dirname));
  app.use(express.static(STATIC_SOURCE));
  
  return app;
}

async function connectToDatabase () {
  const dbCtrl = dbModule();

  console.log('Now connecting to database...');

  return new Promise(async (resolve, reject) => dbCtrl.connect()
    .then(() => resolve(dbCtrl))
    .catch(err => reject(err))
  );
}

async function connectToSMTPServer () {
  const
    smtpCtrl = smtpModule();
  
  console.log('Now connecting to SMTP...');
  
  return new Promise(async (resolve, reject) => smtpCtrl.connect()
    .then(() => resolve(smtpCtrl))
    .catch(err => reject(err))
  );
}

async function runServer () {
  console.log('Now running server...');

  const
    server = global[APP].listen(PORT, () => configRoutes()
      .then(() => console.log(`ENKE-Systems (new) te salutant on PORT:${PORT}`))
      .catch(err => console.log(err))
    );

  const
    onTerminate = () => {
      console.log('Stopping services...');
      HELPERS.PIPE(
        () => global[DB].disconnect(),
        () => global[SMTP].disconnect(),
        () => console.log('Good bye!'),
        () => server.close()
      );
    };

  ['SIGINT', 'SIGTERM'].forEach(e => process.on(e, () => onTerminate()));

  return server;
}

