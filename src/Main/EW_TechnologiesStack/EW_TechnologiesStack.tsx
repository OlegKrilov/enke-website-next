import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {observer} from "mobx-react";

const
  ROOT = `ew-technologies-stack`,
  TECH_CATEGORY = `${ROOT}-category`,
  TECH_CATEGORY_ITEM = `${TECH_CATEGORY}-item`,
  TECH_CATEGORY_LABEL = `${TECH_CATEGORY}-label`,
  TECH_CATEGORY_ICON = `${TECH_CATEGORY}-icon`;

@observer
export class EW_TechnologiesStack extends AbstractComponent {

  private items: any[] = [
    {
      LABEL: 'Frontend Technologies',
      ITEMS: {
        'Javascript': 'javascript',
        'React': 'react',
        'Angular': 'angular',
        'React Native': 'react_native',
        'D3': 'd3'
      }
    },
    {
      LABEL: 'Backend Technologies',
      ITEMS: {
        'Java': 'java',
        'Python': 'python',
        '.NET': 'ms_dot_net',
        'Objective C': 'objective_c',
        'Node JS': 'node_js'
      }
    },
    {
      LABEL: 'Databases',
      ITEMS: {
        'Redshift': 'amazon_redshift',
        'Postgre SQL': 'postgre_sql',
        'SQL Server': 'sql_server',
        'Cassandra': 'cassandra',
        'Dynamo DB': 'dynamo_db',
        'Mongo DB': 'mongo'
      }
    },
    {
      LABEL: 'Infrastructures',
      ITEMS: {
        'Hadoop': 'hadoop',
        'Spark': 'apache_spark',
        'Kafka': 'kafka',
        'Tensor Flow': 'tensorflow',
        'Office 365': 'office_365',
        'SharePoint': 'sharepoint',
        'Power Apps': 'power_apps',
        'SailPoint': 'sail_point',
        'ServiceNow': 'service_now',
        'SalesForce': 'sales_force',
        'Bizagi': 'bizagi',
        'AWS': 'aws',
        'Azure': 'azure',
        'Google Cloud': 'google_cloud'
      }
    }
  ];

  render () {
    const
      {props, items} = this,
      {className, children, isActive} = props;
    
    return <div className={`${ROOT} ${className || EMPTY_STRING}`}>{items.map((d, i) =>
      <div className={`${TECH_CATEGORY} padding-v-40 margin-v-40`} key={d.LABEL}>{
        <div className={`row`}>
          <div className={`col-12 col-lg-3`}>
            <b className={`font-size-22 display-flex ew-block-height`}>{d.LABEL}</b>
          </div>
          <div className={`col-12 col-lg-9`}>{Object.keys(d.ITEMS).map(dd =>
            <div className={`${TECH_CATEGORY_ITEM} display-inline-flex flex-direction-column`} key={dd}>
              <div className={`${TECH_CATEGORY_ICON}`}>
                <EW_BackgroundImage source={`TechIcons/${d.ITEMS[dd]}.jpg`} />
              </div>
              <b className={`${TECH_CATEGORY_LABEL} font-size-12`}>{dd}</b>
            </div>
          )}</div>
        </div>
      }</div>
    )}</div>;
  }
}

