import * as d3 from 'd3';
import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {
  DISPLAY_IN_FOOTER,
  IAppRoute,
  IS_MENU_ITEM,
  IS_PRIVATE_PAGE,
  ROUTES,
  RoutingService
} from "../../Services/Routing.service";
import {SharedService} from "../../Services/Shared.service";
import {LABEL, NAME} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EMPTY_STRING, IS_OPENED, IS_SELECTED} from "../../Core/Constants/ViewClasses.cnst";
import {AS_FUNCTION} from "../../Core/Helpers/Helpers.misc";
import {_IS_SELECTED_} from "../../Core/Abstract/AbstractCollection";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {
  EW_PRIMARY_BANNER,
  EW_PRIMARY_TEXT, EW_SECONDARY_BANNER, EW_SECONDARY_TEXT,
  EW_SECTION_HEADER
} from "../../Common/Constants/EW_ViewClasses.cnst";

const
  ROOT = `ew-app-footer`,
  NAV = `${ROOT}-nav`,
  NAV_LINK = `${NAV}-link`,
  NAV_LINK_LABEL = `${NAV_LINK}-label`;


const
  LOGO_DARK = '/logo-dark.png',
  LOGO_LIGHT = '/logo-light.png';


@inject('routingService', 'sharedService')
@observer
export default class EW_AppFooter extends AbstractComponent {

  private routingService: RoutingService;

  private sharedService: SharedService;

  private getMenuItems = (customClass: string = EMPTY_STRING) => this.routingService.routesCollection.items
    .filter((d: IAppRoute) => !d[IS_PRIVATE_PAGE] && d[DISPLAY_IN_FOOTER])
    .map((d: IAppRoute) =>
      <div className={`${NAV_LINK} ${customClass} padding-v-20 ew-clickable
      ${AS_FUNCTION(d[_IS_SELECTED_]) ? IS_SELECTED : EMPTY_STRING}`}
           key={d[NAME]}
           onClick={() => !AS_FUNCTION(d[_IS_SELECTED_]) && this.routingService.goTo(d[NAME])}>
        <span className={`${NAV_LINK_LABEL} pos-rel`}>{d[LABEL]}</span>
      </div>
    );

  private getCopyrightYear = (): string => `${new Date().getFullYear()}`;

  render() {
    return (
      <footer className={`ew-bg-footer `}>
        <div className={`container`}>
          <div className={`row`}>
            <div className={`col-12 col-lg-4 col-xl-4 `}>
              <div className={`display-flex  align-left  ew-block-height-2`}>
                <div>
                  <div className={` ew-block-height ew-block-width`}>
                    <EW_BackgroundImage source={LOGO_LIGHT} children={
                      <div className={'size-cover pos-rel'}>
                        <div className={`full-width text-center ew-color-footer pos-abs bottom-0  font-size-10`}>
                          <span>Ⓒ {this.getCopyrightYear()} Enke System</span>
                        </div>
                      </div>
                    }/>

                  </div>
                </div>
              </div>
            </div>
            <div className={`hidden-md-down col-lg-8 col-xl-8 ew-color-footer  `}>
              <div
                className={` ${EW_SECONDARY_TEXT} display-flex align-right ew-block-height-2 font-size-12 letter-spacing-96`}>{
                this.getMenuItems('margin-left-30')
              }</div>
            </div>
            {/*<div className="TestButton">*/}
            {/*  <EW_Button onClick={(e) => this.sharedService.isLoading.toggleValue()} children={*/}
            {/*    EW_ButtonModel.buildRoundButton("Test")*/}
            {/*  }/>*/}
            {/*</div>*/}
          </div>
        </div>
      </footer>
    );
  }

  constructor(props) {
    super(props);
    this.sharedService = props.sharedService;
    this.routingService = props.routingService;
  }
}
