import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {ICON, LABEL, NAME, VALUE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {FaFacebook, FaLinkedin, FaTwitter} from "react-icons/all";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";

const
	ROOT = `ew-social-links`,
	ITEM = `${ROOT}-item`;

export interface I_EW_SocialLink {
	[NAME]: string,
	[LABEL]: string,
	[ICON]: any,
	[VALUE]: string
}

export default class EW_SocialLinks extends AbstractComponent {
	
	private readonly socialLinks: AbstractCollection = new AbstractCollection(NAME).addItems([
		{
			[NAME]: 'facebook',
			[LABEL]: 'Facebook',
			[ICON]: <FaFacebook />,
			[VALUE]: 'https://ru-ru.facebook.com/enkesystems/'
		},
		{
			[NAME]: 'linkedIn',
			[LABEL]: 'linkedIn',
			[ICON]: <FaLinkedin />,
			[VALUE]: 'https://www.linkedin.com/company/enkesystems/'
		},
		{
			[NAME]: 'twitter',
			[LABEL]: 'twitter',
			[ICON]: <FaTwitter />,
			[VALUE]: 'https://twitter.com/enkesystems'
		}
		
	] as I_EW_SocialLink[]);
	
	render () {
		const
			{socialLinks, props} = this,
			{className} = props,
			links = socialLinks.items;
		
		return <div className={`${ROOT} ${className || EMPTY_STRING}
		    font-size-30 padding-v-10 display-flex align-left`}>{links.map((d: I_EW_SocialLink, i: number) =>
			<a className={`${ITEM} display-inline-flex margin-right-20`}
			   href={d[VALUE]} target={'_blank'} key={i}>{d[ICON]}
			</a>
		)}</div>;
	}
	
}
