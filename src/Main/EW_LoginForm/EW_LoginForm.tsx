import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {UserService} from "../../Services/User.service";
import {SharedService} from "../../Services/Shared.service";
import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {I_EW_FormField} from "../../Common/Interfaces/FormField.interface";
import {CLASSNAME, MODEL, NAME} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EW_InputModel} from "../../Components/EW_Input/EW_Input.model";
import {IS_REQUIRED} from "../../Common/Helpers/EW_Validators";
import {PASSWORD_INPUT, STRING_INPUT} from "../../Common/Constants/EW_InputTypes.cnst";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {EW_Input} from "../../Components/EW_Input/EW_Input";
import {computed} from "mobx";
import {RoutingService} from "../../Services/Routing.service";
import {EMPTY_STRING, IS_OPENED} from "../../Core/Constants/ViewClasses.cnst";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {LOGIN, PASSWORD} from "../../Common/Constants/EW_User.cnst";
import {DIG_OUT, PIPE} from "../../Core/Helpers/Helpers.misc";
import {StructureObservable} from "../../Core/Observables/StructureObservable";
import {SwitchObservable} from "../../Core/Observables/SwitchObservable";
import EW_Modal from "../../Components/EW_Modal/EW_Modal";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";
import {IoMdHand} from "react-icons/all";

const
  ROOT = `ew-login-form`,
  FORM_CONTENT = `${ROOT}-content`;

@inject('userService', 'sharedService', 'routingService')
@observer
export class EW_LoginForm extends AbstractComponent{

  private readonly userService: UserService;

  private readonly sharedService: SharedService;

  private readonly routingService: RoutingService;

  private readonly error: StructureObservable = new StructureObservable();

  private readonly formFields: AbstractCollection = new AbstractCollection(NAME).addItems([
    {
      [NAME]: LOGIN,
      [MODEL]: new EW_InputModel({
        validators: IS_REQUIRED(),
        type: STRING_INPUT,
        placeholder: 'Login'
      }),
      [CLASSNAME]: 'col-12'
    },
    {
      [NAME]: PASSWORD,
      [MODEL]: new EW_InputModel({
        validators: IS_REQUIRED(),
        type: PASSWORD_INPUT,
        placeholder: 'Password'
      }),
      [CLASSNAME]: 'col-12'
    }
  ] as I_EW_FormField[]);

  private readonly submitButton: EW_ButtonModel = new EW_ButtonModel();

  private readonly isOpened: SwitchObservable = new SwitchObservable();

  @computed
  private get hasError (): boolean {
    return !this.error.isEmpty;
  }

  componentDidMount(): void {
    const
      {userService, sharedService, routingService, submitButton, formFields, error, isOpened} = this,
      {isLoading, growl} = sharedService;

    const
      formStateObserver = new AbstractObserver(() => formFields.items.reduce(
        (isValid: boolean, d: I_EW_FormField) => isValid ?  d[MODEL].isValid : isValid, true)
      ),

      isOpenedStateObserver = new AbstractObserver(
        () => !userService.isAuthorized && routingService.privatePage
      );

    this.registerSubscriptions(

      formStateObserver.getSubscription(isValid => submitButton.toggleDisabled(!isValid)),

      isOpenedStateObserver.getSubscription(
        state => isOpened.setValue(state)
      ),

      submitButton.onClick.getSubscription(
        e => PIPE(
          error.resetValue(),
          () => isLoading.setValue(true),
          () => userService.login({
            [LOGIN]: DIG_OUT(formFields.getItem(LOGIN), MODEL, 'currentValue'),
            [PASSWORD]: DIG_OUT(formFields.getItem(PASSWORD), MODEL, 'currentValue')
          }),
        )
          .then((res: any) => growl
            .addMessage(EW_GrowlModel.buildInfoMessage(`Welcome, ${res.login}!`, <IoMdHand />)).show()
          )
          .catch(err => {
            growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show();
            error.setValue(err);
          })
          .finally(() => sharedService.isLoading.setValue(false))
      ),

      isOpened.getSubscription(
        () => PIPE(
          formFields.items.forEach((d: I_EW_FormField) => d[MODEL].clearValue()),
          () => error.resetValue()
        )
      )
    );
  }

  render () {
    const
      {formFields, submitButton, isOpened, hasError, error} = this;

    return <EW_Modal className={`${ROOT}`} isOpened={isOpened} children={
      <div className={`size-cover display-flex align-center`}>
        <div className={`${FORM_CONTENT} ew-bg-white padding-50`}>
          {formFields.items.map((d: I_EW_FormField, i) =>
            <div className={`row`} key={i}>
              <div className={`${d[CLASSNAME]} padding-bottom-30`}>
                <EW_Input className={'full-width'} model={d[MODEL]}/>
              </div>
            </div>
          )}
          <div className={`text-right`}>
            <EW_Button className={`display-block full-width`} model={submitButton} children=
              {EW_ButtonModel.buildSquareButton('Submit')}/>
          </div>
          {hasError && <div className={'text margin-v-20'}>
            <span>{DIG_OUT(error.value, 'statusText')}</span>
          </div>}
        </div>
      </div>
    } />;
  }

  constructor (props) {
    super(props);
    this.sharedService = props.sharedService;
    this.userService = props.userService;
    this.routingService = props.routingService;
  }
}

