import React, {Suspense, lazy} from "react";
import {AbstractComponent} from "../../../Core/Abstract/AbstractComponent";
import {EMPTY_STRING} from "../../../Core/Constants/ViewClasses.cnst";
import {inject, observer} from "mobx-react";
import {UserService} from "../../../Services/User.service";
import {SharedService} from "../../../Services/Shared.service";
import {EW_FormModel, LOGIN_FORM} from "../../../Components/EW_Form/EW_Form.model";
import {DIG_OUT} from "../../../Core/Helpers/Helpers.misc";
import {DATA} from "../../../Core/Constants/PropertiesAndAttributes.cnst";
import {AbstractPipe} from "../../../Core/Abstract/AbstractPipe";
import {EW_GrowlModel} from "../../../Components/EW_Growl/EW_Growl.model";

const
	EW_Modal = lazy(() => import('../../../Components/EW_Modal/EW_Modal')),
	EW_Form = lazy(() => import('../../../Components/EW_Form/EW_Form'));

const
	ROOT = `ew-admin-login-form`;

@inject('sharedService', 'userService')
@observer
export default class EW_AdminLoginForm extends AbstractComponent {
	
	private readonly userService: UserService;
	
	private readonly sharedService: SharedService;
	
	private readonly loginForm: EW_FormModel = new EW_FormModel(LOGIN_FORM);
	
	componentDidMount() {
		const
			{sharedService, loginForm, userService} = this;
		
		const
			authorizationRequestPipe = new AbstractPipe([
				() => loginForm.isSubmitted.setValue(true),
				() => userService.login(loginForm.value)
			], false);
			
		
		this.registerSubscriptions(
			sharedService.showLoginForm.getSubscription(
				state => state && loginForm.resetForm()
			),
			
			loginForm.onSubmit.getSubscription(
				e => authorizationRequestPipe.run()
			),
			
			authorizationRequestPipe.catch(
				err => sharedService.growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show()
			),
			
			authorizationRequestPipe.finally(
				() => sharedService.showLoginForm.setValue(false)
			)
		);
		
		this.registerPipes(authorizationRequestPipe);
	}
	
	render () {
		const
			{sharedService, loginForm} = this,
			{showLoginForm} = sharedService;
		
		return <Suspense fallback={EMPTY_STRING}>
			<EW_Modal className={`${ROOT} display-flex align-center ew-bg-white padding-50 margin-top-50`}
					isOpened={showLoginForm}
          showCloseButton={true}>
				<Suspense fallback={EMPTY_STRING}><EW_Form model={loginForm} /></Suspense>
			</EW_Modal>
		</Suspense>;
	}
	
	constructor(props) {
		super(props);
		this.sharedService = props.sharedService;
		this.userService = props.userService;
	}
}

