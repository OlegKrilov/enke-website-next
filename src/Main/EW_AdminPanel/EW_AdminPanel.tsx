import React, {Suspense, lazy} from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {IoIosArrowForward, IoMdAdd, IoMdLock, IoMdUnlock} from "react-icons/all";
import {UserService} from "../../Services/User.service";
import {SharedService} from "../../Services/Shared.service";
import {EMPTY_STRING, IS_OPENED} from "../../Core/Constants/ViewClasses.cnst";
import {RoutingService} from "../../Services/Routing.service";
import {_TOGGLE_, AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {ACTION, ACTIONS, COMPONENT, LABEL, NAME, TRIGGER} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {AS_ARRAY, AS_FUNCTION, AWAIT, DIG_OUT, PIPE} from "../../Core/Helpers/Helpers.misc";

const
	EW_AdminLoginForm = lazy(() => import('./EW_AdminLoginForm/EW_AdminLoginForm'));

const
	ROOT = 'ew-admin-panel',
	ACTION_BUTTONS = `${ROOT}-action-buttons`,
	ACTION_BUTTON = `${ROOT}-action-button`,
	ACTION_BUTTON_LABEL = `${ACTION_BUTTON}-label`,
	ACTION_BUTTON_ICON = `${ACTION_BUTTON}-icon`;

@inject('userService', 'sharedService', 'routingService')
@observer
export default class EW_AdminPanel extends AbstractComponent {
	
	private readonly userService: UserService;
	
	private readonly sharedService: SharedService;
	
	private readonly routingService: RoutingService;
	
	private readonly isOpened: BooleanObservable = new BooleanObservable();
	
	private readonly actions: AbstractCollection = new AbstractCollection(NAME).addItems([
		{
			[NAME]: 'signIn',
			[LABEL]: () => this.userService.isAuthorized ? 'Sign out' : 'Sign in',
			[TRIGGER]: () => this.userService.isAuthorized ? <IoMdUnlock /> : <IoMdLock />,
			[COMPONENT]: () => <Suspense fallback={EMPTY_STRING}><EW_AdminLoginForm /></Suspense>,
			[ACTION]: () => this.userService.isAuthorized ?
				this.userService.logout() :
				this.sharedService.showLoginForm.setValue(true)
		}
	]);
	
	private onTriggerClick = (d) => PIPE(
		() => AS_FUNCTION(d[_TOGGLE_], true),
		() => AWAIT(300),
		() => AS_FUNCTION(d[ACTION])
	);
	
	componentDidMount() {
		const
			{routingService, userService} = this;
	}
	
	render () {
		const
			{isOpened, sharedService, userService, routingService, actions} = this,
			{isAuthorized} = userService;
		
		const
			allowedActions = (isAuthorized ? AS_ARRAY(DIG_OUT(routingService.currentRoute, ACTIONS)) : [])
				.concat(['signIn'])
				.filter(key => actions.hasItem(key))
				.map(key => actions.getItem(key));
		
		return <div className={`${ROOT}`}>
			<div className={`${ACTION_BUTTONS} pos-fix right-0`}>{allowedActions.map((d, i) =>
				<div className={`${ACTION_BUTTON} pos-rel`} key={i} onClick={() => this.onTriggerClick(d)}>
					<div className={`${ACTION_BUTTON_LABEL} pos-abs display-flex align-center`}>
						<span>{AS_FUNCTION(d[LABEL])}</span>
					</div>
					<div className={`${ACTION_BUTTON_ICON} pos-rel display-flex align-center font-size-20`}>
						{AS_FUNCTION(d[TRIGGER])}
					</div>
				</div>
			)}</div>
			{actions.selection && AS_FUNCTION(DIG_OUT(actions.selection, COMPONENT))}
		</div>;
	}
	
	constructor(props) {
		super(props);
		this.sharedService = props.sharedService;
		this.userService = props.userService;
		this.routingService = props.routingService;
	}
}

