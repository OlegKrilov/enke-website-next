import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {
  DISPLAY_IN_HEADER,
  IAppRoute,
  IS_MENU_ITEM,
  IS_PRIVATE_PAGE,
  ROUTES,
  RoutingService
} from "../../Services/Routing.service";
import {SharedService} from "../../Services/Shared.service";
import {LABEL, NAME} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EMPTY_STRING, IS_OPENED, IS_SELECTED} from "../../Core/Constants/ViewClasses.cnst";
import {AS_FUNCTION, DIG_OUT, PIPE} from "../../Core/Helpers/Helpers.misc";
import {_IS_SELECTED_} from "../../Core/Abstract/AbstractCollection";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {IoMdClose, IoMdLogIn, IoMdLogOut, IoMdMenu} from "react-icons/all";
import {UserService} from "../../Services/User.service";
import {computed} from "mobx";

const
  ROOT = `ew-app-header`,
  LOGO = `${ROOT}-logo`,
  NAV = `${ROOT}-nav`, 
  NAV_LINK = `${NAV}-link`,
  NAV_LINK_LABEL = `${NAV_LINK}-label`,
  MOBILE_NAV = `${ROOT}-mobile-nav`,
  MOBILE_NAV_BTN = `${MOBILE_NAV}-btn`,
  MOBILE_NAV_DROPDOWN = `${MOBILE_NAV}-dropdown`,
  MOBILE_NAV_CONTENT = `${MOBILE_NAV}-content`;

const
  LOGO_DARK = '/logo-dark.png',
  LOGO_LIGHT = '/logo-light.png';

@inject('routingService', 'sharedService', 'userService')
@observer
export default class EW_AppHeader extends AbstractComponent {

  private readonly routingService: RoutingService;

  private readonly sharedService: SharedService;

  private readonly userService: UserService;

  @computed
  private get showPrivateLinks () {
    const
      {routingService, userService} = this;
    return routingService.privatePage && userService.isAuthorized;
  }

  private getMenuItems = (customClass: string = EMPTY_STRING) => {
    const
      {routingService, showPrivateLinks} = this;

    const
      getMenuItem = (d: IAppRoute) =>
        <div className={`${NAV_LINK} 
            ${customClass} padding-v-20
            ${AS_FUNCTION(d[_IS_SELECTED_]) ? IS_SELECTED : EMPTY_STRING}`
            } key={d[NAME]} onClick={() => !AS_FUNCTION(d[_IS_SELECTED_]) && routingService.goTo(d[NAME])}>
          <span className={`${NAV_LINK_LABEL} pos-rel`}>{d[LABEL]}</span>
        </div>;

    return routingService.routesCollection.items
      .filter((d: IAppRoute) => d[DISPLAY_IN_HEADER])
      .filter((d: IAppRoute) => showPrivateLinks ? d[IS_PRIVATE_PAGE] : !d[IS_PRIVATE_PAGE])
      .map((d: IAppRoute) => getMenuItem(d));
  };

  private onLogInClick = () => this.routingService.goTo(ROUTES.HOME);
  
  componentDidMount(): void {
    const
      {routingService, sharedService} = this,
      {menuState} = sharedService;

    this.registerSubscriptions(

      routingService.onRouteChange(
        () => menuState.toggleValue(false)
      ),

      menuState.getSubscription(
        state => state && window.scroll(0,0)
      )
    );
  }

  render () {
    const
      {routingService, sharedService, showPrivateLinks, userService} = this,
      {menuState} = sharedService,
      menuIsOpened = menuState.value,
      desktopMenu = this.getMenuItems('margin-left-30'),
      mobileMenu = this.getMenuItems();

    return <div className={`${ROOT} pos-rel ew-bg-white`}>

      <div className={`hidden-md-down pos-abs top-0 left-0 full-height half-width ew-bg-white`} />
      <div className={`hidden-md-down pos-abs top-0 right-0 full-height half-width ew-bg-blue`} />

      <div className={`container`}>
        <div className={`row`}>
          <div className={`col-12 col-lg-6 col-xl-6`}>
            <div className={`${LOGO} ew-block-height ew-block-width`}>
              <EW_BackgroundImage source={LOGO_DARK} children={
                <div className={'size-cover ew-clickable'} onClick={() => this.onLogInClick()} />
              } />
            </div>
          </div>

          <div className={`hidden-lg-up pos-rel ${MOBILE_NAV} ${menuIsOpened ? IS_OPENED : EMPTY_STRING}`}>
            <div className={`${MOBILE_NAV_BTN}
                  pos-abs top-0 right-0 display-flex align-center ew-block-width ew-block-height ew-clickable font-size-40`}
                 onClick={() => menuState.toggleValue()}>
              {menuIsOpened ? <IoMdClose/> : <IoMdMenu/>}
            </div>
          </div>

          <div className={`hidden-md-down col-lg-6 col-xl-6 ew-color-white`}>
            <div className={`${NAV} display-flex align-right ew-block-height font-size-12 letter-spacing-96`}>
              {desktopMenu}
              {/*<div className={'margin-left-30 font-size-18'}>{showPrivateLinks ?*/}
              {/*  <IoMdLogOut className={`${NAV_LINK} ew-clickable`} onClick={() => this.onLogOutClick()}/> :*/}
              {/*  <IoMdLogIn className={`${NAV_LINK} ew-clickable`} onClick={() => this.onLogInClick()}/>*/}
              {/*}</div>*/}
            </div>
          </div>

        </div>
      </div>

      <div className={`${MOBILE_NAV_DROPDOWN} ${menuIsOpened ? IS_OPENED : EMPTY_STRING} hidden-lg-up pos-fix top-0 left-0 size-cover`}>
        <div className={`${MOBILE_NAV_CONTENT} ew-bg-blue ew-color-white`}>
          <div className={`container`}>
            <div className={`row`}>
              <div className={`col-12`}>
                <div className={`${LOGO} ew-block-height ew-block-width`}>
                  <EW_BackgroundImage source={LOGO_LIGHT} />
                </div>
              </div>
              <div className={`col-12 ew-color-white`}>
                <div className={`${NAV} font-size-12 letter-spacing-96 padding-bottom-30`}>{mobileMenu}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.sharedService = props.sharedService;
    this.routingService = props.routingService;
    this.userService = props.userService;
  }

}

