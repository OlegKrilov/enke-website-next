import React, {Suspense, lazy} from "react";
import {AbstractComponent} from "../../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {EW_InputModel} from "../../../Components/EW_Input/EW_Input.model";
import {EW_Input} from "../../../Components/EW_Input/EW_Input";
import {AbstractCollection} from "../../../Core/Abstract/AbstractCollection";
import {NAME} from "../../../Core/Constants/PropertiesAndAttributes.cnst";
import {PIPE} from "../../../Core/Helpers/Helpers.misc";
import {IoMdClose} from "react-icons/all";
import {SharedService} from "../../../Services/Shared.service";

const
	ROOT = `ew-admin-blog-constructor-tags`,
	CORE = `${ROOT}-core`,
	TAG_ITEM = `${ROOT}-item`,
	TAG_ITEM_LABEL = `${TAG_ITEM}-label`,
	TAG_ITEM_ICON = `${TAG_ITEM}-icon`;

@inject('sharedService')
@observer
export default class EW_AdminBlogConstructorTags extends AbstractComponent {
	
	private readonly sharedService: SharedService;
	
	private readonly tagInput: EW_InputModel = new EW_InputModel({
		placeholder: 'Add tag'
	});
	
	private readonly tags: AbstractCollection;
	
	private addTag = (tag: string) => this.tags.addItems({[NAME]: tag});
	
	componentDidMount() {
		const
			{tagInput, sharedService} = this;
		
		this.registerSubscriptions(
			sharedService.showBlogConstructor.getSubscription(
				state => {
					if (state) {
						tagInput.clearValue();
						this.tags.clearItems();
					}
				}
			),
			
			tagInput.onSubmit.getSubscription(
				e => PIPE(
					this.addTag(e.data),
					() => tagInput.clearValue(),
					() => tagInput.toggleFocused(true)
				)
			)
		);
	}
	
	render () {
		const
			{tagInput, tags} = this;
		
		return <div className={`${ROOT}`}>
			{tags.items.map((d, i) =>
				<div className={`${TAG_ITEM} display-inline-flex align-left`} key={i}>
					<span className={`${TAG_ITEM_LABEL} margin-right-10 font-size-18`}>{d[NAME]}</span>
					<IoMdClose className={TAG_ITEM_ICON} onClick={e => tags.removeItem(d[NAME])}/>
				</div>
			)}
			<EW_Input className={`${CORE}`} model={tagInput}/>
		</div>;
	}
	
	constructor(props) {
		super(props);
		this.sharedService = props.sharedService;
		this.tags = props.tags;
	}
	
}

