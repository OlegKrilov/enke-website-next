import React from "react";
import {AbstractComponent} from "../../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {
	COMPONENT,
	ID,
	IMAGE,
	LABEL,
	NAME,
	TEXT,
	TYPE,
	VALUE
} from "../../../Core/Constants/PropertiesAndAttributes.cnst";
import {_IS_SELECTED_, _REMOVE_, _TOGGLE_, AbstractCollection} from "../../../Core/Abstract/AbstractCollection";
import {EW_InputModel} from "../../../Components/EW_Input/EW_Input.model";
import {EW_ImageInputModel} from "../../../Components/EW_ImageInput/EW_ImageInput.model";
import {AS_FUNCTION, DIG_OUT, GET_RANDOM_ID} from "../../../Core/Helpers/Helpers.misc";
import {
	BlockSizeEnum,
	BlockTypeEnum,
	ImageDecorationEnum,
	ImageFormatEnum,
	TextDecorationEnum,
	TextFormatEnum
} from "../../../Common/Enums/BlockViewClasses.enum";
import {
	MdAdd,
	MdChat,
	MdClose,
	MdCrop,
	MdCropFree,
	MdFormatBold,
	MdFormatItalic,
	MdFormatQuote,
	MdFormatUnderlined,
	MdImage,
	MdLooks3,
	MdLooksOne,
	MdLooksTwo,
	MdPhotoSizeSelectLarge,
	MdTextFields,
	MdLightbulbOutline
} from "react-icons/all";
import {EMPTY_STRING, IS_SELECTED} from "../../../Core/Constants/ViewClasses.cnst";
import {TEXT_INPUT} from "../../../Common/Constants/EW_InputTypes.cnst";
import {EW_FileInputModel} from "../../../Components/EW_FileInput/EW_FileInput.model";
import {EW_Input} from "../../../Components/EW_Input/EW_Input";
import {EW_ImageInput} from "../../../Components/EW_ImageInput/EW_ImageInput";
import {EW_Button} from "../../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../../Components/EW_Button/EW_Button.model";
import {EW_BackgroundImage} from "../../../Components/EW_BackgroundImage/EW_BackgroundImage";

const
	ROOT = `ew-blog-constructor-content`,
	BLOCK = `${ROOT}-block`,
	BLOCK_SETTINGS = `${BLOCK}-settings`,
	CONTROL_PANEL = `${ROOT}-control-panel`,
	CONTROL_PANEL_BUTTON = `${CONTROL_PANEL}-button`,
	BLOCK_CONTENT = `${BLOCK}-content`,
	BLOCK_CONTENT_ITEM = `${BLOCK_CONTENT}-item`,
	BLOCK_CONTENT_ITEM_DECORATION = `${BLOCK_CONTENT_ITEM}-decoration`,
	BLOCK_CONTENT_ITEM_INPUT = `${BLOCK_CONTENT_ITEM}-input`,
	BLOCK_CONTENT_ITEM_SETTINGS = `${BLOCK_CONTENT_ITEM}-settings`,
	BLOCK_CONTENT_ITEM_DECORATION_LOGO = `${BLOCK_CONTENT_ITEM_DECORATION}-logo`,
	BLOCK_CONTENT_ITEM_DECORATION_LOGO_INNER = `${BLOCK_CONTENT_ITEM_DECORATION_LOGO}-inner`,
	BLOCK_CONTENT_ITEM_DECORATION_BANNER = `${BLOCK_CONTENT_ITEM_DECORATION}-banner`;

const
	LOGO_SOURCE = 'logo-light.png';
	
export const
	BLOCK_SIZE = 'blockSize',
	
	TEXT_BLOCK = 'textBlock',
	TEXT_FORMAT = 'textFormat',
	TEXT_DECORATION = 'textDecoration',
	
	IMAGE_BLOCK = 'imageBlock',
	IMAGE_FORMAT = 'imageFormat',
	IMAGE_DECORATION = 'imageDecoration',
	IMAGE_BANNER = 'imageBanner',
	IMAGE_DECORATION_COLOR = 'imageDecorationColor',

	CONTENT_ITEMS = 'contentItems',
	
	HAS_LOGO = 'hasLogo',
	HAS_BANNER = 'hasBanner',
	INVERTED_COLOR = 'invertedColor';

export interface I_BlogConstructorBlock {
	[ID]: string,
	[BLOCK_SIZE]: AbstractCollection,
	[CONTENT_ITEMS]: AbstractCollection
}

export interface I_BlogConstructorBlockContent {
	[ID]: string,
	[TYPE]: AbstractCollection,
	
	[TEXT]: EW_InputModel,
	[TEXT_FORMAT]: AbstractCollection,
	[TEXT_DECORATION]: AbstractCollection,
	
	[IMAGE]: EW_ImageInputModel,
	[IMAGE_FORMAT]: AbstractCollection,
	[IMAGE_DECORATION]: AbstractCollection,
	[IMAGE_BANNER]: EW_InputModel
}

@inject('sharedService')
@observer
export default class EW_AdminBlogConstructorContent extends AbstractComponent {

	private readonly blocks: AbstractCollection;
	
	private getSizesCollection = () => ([
		{
			[NAME]: 'wide',
			[VALUE]: BlockSizeEnum.wide,
			[COMPONENT]: <MdLooksOne/>
		},
		{
			[NAME]: 'medium',
			[VALUE]: BlockSizeEnum.medium,
			[COMPONENT]: <MdLooksTwo/>
		},
		{
			[NAME]: 'small',
			[VALUE]: BlockSizeEnum.small,
			[COMPONENT]: <MdLooks3/>
		}
	]);
	
	private getTypesCollection = () => ([
		{
			[NAME]: TEXT_BLOCK,
			[VALUE]: BlockTypeEnum.textBlock,
			[LABEL]: 'Text',
			[COMPONENT]: <MdTextFields/>
		},
		{
			[NAME]: IMAGE_BLOCK,
			[VALUE]: BlockTypeEnum.imageBlock,
			[LABEL]: 'Image',
			[COMPONENT]: <MdImage/>
		}
	]);
	
	private getTextDecorationsCollection = () => ([
		{
			[NAME]: 'bold',
			[VALUE]: TextDecorationEnum.bold,
			[COMPONENT]: <MdFormatBold/>
		},
		{
			[NAME]: 'italic',
			[VALUE]: TextDecorationEnum.italic,
			[COMPONENT]: <MdFormatItalic/>
		},
		{
			[NAME]: 'underline',
			[VALUE]: TextDecorationEnum.underline,
			[COMPONENT]: <MdFormatUnderlined/>
		},
		{
			[NAME]: 'blockquote',
			[VALUE]: TextDecorationEnum.blockquote,
			[COMPONENT]: <MdFormatQuote/>
		}
	]);
	
	private getTextFormatsCollection = () => ([
		{
			[NAME]: 'header1',
			[VALUE]: TextFormatEnum.header1,
			[COMPONENT]: <div style={{
				fontSize: '30px',
				fontWeight: 'bold'
			}}>&lt;h1 /&gt;</div>
		},
		{
			[NAME]: 'header2',
			[VALUE]: TextFormatEnum.header2,
			[COMPONENT]: <div style={{
				fontSize: '22px',
				fontWeight: 'bold'
			}}>&lt;h2 /&gt;</div>
		},
		{
			[NAME]: 'header3',
			[VALUE]: TextFormatEnum.header3,
			[COMPONENT]: <div style={{
				fontSize: '18px',
				fontWeight: 'bold'
			}}>&lt;h3 /&gt;</div>
		},
		{
			[NAME]: 'paragraph',
			[VALUE]: TextFormatEnum.paragraph,
			[COMPONENT]: <div style={{fontSize: '14px'}}>&lt;p /&gt;</div>
		}
	]);
	
	private getImageDecorationsCollection = () => ([
		{
			[NAME]: HAS_LOGO,
			[VALUE]: ImageDecorationEnum.hasLogo,
			[COMPONENT]: <MdPhotoSizeSelectLarge />
		},{
			[NAME]: HAS_BANNER,
			[VALUE]: ImageDecorationEnum.hasBanner,
			[COMPONENT]: <MdChat />
		},
		{
			[NAME]: INVERTED_COLOR,
			[VALUE]: ImageDecorationEnum.invertedColor,
			[COMPONENT]: <MdLightbulbOutline />
		}
	]);
	
	private getImageFormatsCollection = () => ([
		{
			[NAME]: 'cover',
			[VALUE]: ImageFormatEnum.cover,
			[COMPONENT]: <MdCrop />
		},{
			[NAME]: 'contain',
			[VALUE]: ImageFormatEnum.contain,
			[COMPONENT]: <MdCropFree />
		}
	]);
	
	private getBlockSizeClass = (d: I_BlogConstructorBlock | null = null) => DIG_OUT(
		(d || this.blocks.items[this.blocks.size - 1] || null), BLOCK_SIZE, 'selection', VALUE
	) || BlockSizeEnum.wide;
	
	private getButtonClass = (d) =>
		`${CONTROL_PANEL_BUTTON} ${AS_FUNCTION(d[_IS_SELECTED_]) ? IS_SELECTED : EMPTY_STRING}`;
	
	private renderBlock = (d: I_BlogConstructorBlock) =>
		<div className={`${BLOCK} ${this.getBlockSizeClass(d)} margin-v-10`} key={d[ID]}>
			{this.renderBlockContent(d)}
			{this.renderBlockSettingsPanel(d)}
		</div>;
	
	private renderBlockSettingsPanel = (d: I_BlogConstructorBlock) =>
		<div className={`${BLOCK_SETTINGS} margin-bottom-10`}>
			<div className={CONTROL_PANEL}>
				<div className={CONTROL_PANEL_BUTTON} onClick={() => AS_FUNCTION(d[_REMOVE_])}>
					<MdClose />
				</div>
				<div className={CONTROL_PANEL_BUTTON} onClick={() => this.addContent(d)}>
					<MdAdd />
				</div>
			</div>
			<div className={CONTROL_PANEL}>{d[BLOCK_SIZE].items.map(dd =>
				<div className={this.getButtonClass(dd)}
				     onClick={() => AS_FUNCTION(dd[_TOGGLE_], true)}
				     key={dd[NAME]}>{dd[COMPONENT]}</div>
			)}</div>
		</div>;
		
	private renderBlockContent = (d: I_BlogConstructorBlock) =>
		<div className={`${BLOCK_CONTENT}`}>
			{!d[CONTENT_ITEMS].size && <h3 className={`col-12 opacity-3 padding-v-10`}>No Content</h3>}
			{d[CONTENT_ITEMS].items.map((dd: I_BlogConstructorBlockContent) => {
				const
					selectedTemplate = DIG_OUT(dd[TYPE].selection, NAME);
				
				return <div className={`${BLOCK_CONTENT_ITEM} margin-top-20`} key={dd[ID]}>
					{this.renderContentSettingsPanel(dd)}
					{selectedTemplate === IMAGE_BLOCK && this.renderImageContentConstructor(dd)}
					{selectedTemplate === TEXT_BLOCK && this.renderTextContentConstructor(dd)}
				</div>;
			})}
		</div>;
	
	private renderContentSettingsPanel = (d: I_BlogConstructorBlockContent) =>
		<div className={`${BLOCK_CONTENT_ITEM_SETTINGS}`}>
			<div className={CONTROL_PANEL}>
				<div className={CONTROL_PANEL_BUTTON} onClick={() => AS_FUNCTION(d[_REMOVE_])}>
					<MdClose />
				</div>
			</div>
			{[TYPE].concat(DIG_OUT(d[TYPE], 'selection', NAME) === TEXT_BLOCK ?
				[TEXT_FORMAT, TEXT_DECORATION] :
				[IMAGE_FORMAT, IMAGE_DECORATION]
			).map((collectionName: string) =>
				<div className={CONTROL_PANEL} key={collectionName}>{
					(d[collectionName] as AbstractCollection).items.map(dd =>
						<div className={this.getButtonClass(dd)}
						     key={dd[NAME]}
						     onClick={() => this.onOptionSelect(d, dd, collectionName)}>{dd[COMPONENT]}</div>
					)
				}</div>
			)}
			{
				DIG_OUT(d[TYPE], 'selection', NAME) === IMAGE_BLOCK &&
				d[IMAGE_DECORATION].isSelected(d[IMAGE_DECORATION].getItem(HAS_BANNER)) &&
				<div className={CONTROL_PANEL}>
					<EW_Input model={d[IMAGE_BANNER]} />
				</div>
			}
		</div>;
	
	private renderTextContentConstructor = (d: I_BlogConstructorBlockContent) =>
		<div className={`pos-rel ${BLOCK_CONTENT_ITEM_INPUT} text-input
				${DIG_OUT(d[TEXT_FORMAT].selection, VALUE)}
				${(d[TEXT_DECORATION].selection || []).reduce(
					(className, dd) => `${className} ${DIG_OUT(dd, VALUE)}`, EMPTY_STRING
				)}`}>
			<div className={`${BLOCK_CONTENT_ITEM_DECORATION} full-height pos-abs top-0 font-size-50 opacity-5`}>
				<MdFormatQuote/>
			</div>
			<div className={`ew-block-height-3`}>
				<EW_Input className={`display-block size-cover`} model={d[TEXT]}/>
			</div>
		</div>;
	
	private renderImageContentConstructor = (d: I_BlogConstructorBlockContent) =>
		<div className={`${BLOCK_CONTENT_ITEM_INPUT} ew-block-height-3 pos-rel`}>
			<EW_ImageInput className={`display-block full-width full-height pos-rel`} model={d[IMAGE]}
			               useSizeContain={DIG_OUT(d[IMAGE_FORMAT], 'selection', VALUE) === ImageFormatEnum.contain}>
				{
					!!d[IMAGE].currentValue &&
					d[IMAGE_DECORATION].isSelected(d[IMAGE_DECORATION].getItem(HAS_LOGO)) &&
					<div className={`${BLOCK_CONTENT_ITEM_DECORATION_LOGO} pos-abs bottom-0 left-0 ew-block-width ew-block-height`}>
						<EW_BackgroundImage className={BLOCK_CONTENT_ITEM_DECORATION_LOGO_INNER} source={LOGO_SOURCE} />
					</div>
				}
				{
					!!d[IMAGE].currentValue &&
					d[IMAGE_DECORATION].isSelected(d[IMAGE_DECORATION].getItem(HAS_BANNER)) &&
					<div className={`${BLOCK_CONTENT_ITEM_DECORATION_BANNER} pos-abs top-0 right-0`}>
						<h3>{d[IMAGE_BANNER].currentValue}</h3>
					</div>
				}
				{!d[IMAGE].currentValue && <div className={`size-cover display-flex flex-direction-column align-center opacity-5`}>
					<h4>Add image</h4>
					<MdImage className={`font-size-50`}/>
				</div>}
			</EW_ImageInput>
		</div>;
	
	private addBlock = () => {
		const
			{blocks} = this,
			newBlock: I_BlogConstructorBlock = {
				[ID]: GET_RANDOM_ID(),
				[BLOCK_SIZE]: new AbstractCollection(NAME).addItems(this.getSizesCollection()),
				[CONTENT_ITEMS]: new AbstractCollection(ID)
			},
			previousBlockSize = DIG_OUT(blocks.items[blocks.size - 1], BLOCK_SIZE, 'selection', NAME) || null;
		
		previousBlockSize ?
			newBlock[BLOCK_SIZE].select(previousBlockSize) :
			newBlock[BLOCK_SIZE].selectFirst();
		
		blocks.addItems(newBlock);
	};
	
	private addContent = (block: I_BlogConstructorBlock) => block[CONTENT_ITEMS].addItems({
		[ID]: GET_RANDOM_ID(),
		[TYPE]: new AbstractCollection(NAME).addItems(this.getTypesCollection()).selectFirst(),
		[TEXT_FORMAT]: new AbstractCollection(NAME).addItems(this.getTextFormatsCollection()).selectLast(),
		[TEXT_DECORATION]: new AbstractCollection(NAME, true).addItems(this.getTextDecorationsCollection()),
		[IMAGE_FORMAT]: new AbstractCollection(NAME).addItems(this.getImageFormatsCollection()).selectFirst(),
		[IMAGE_DECORATION]: new AbstractCollection(NAME, true).addItems(this.getImageDecorationsCollection()),
		[TEXT]: new EW_InputModel({
			type: TEXT_INPUT,
			placeholder: 'Text content'
		}),
		[IMAGE]: new EW_FileInputModel(),
		[IMAGE_BANNER]: new EW_InputModel({
			placeholder: 'Image banner'
		})
	} as I_BlogConstructorBlockContent);
	
	private onOptionSelect = (d: I_BlogConstructorBlockContent, dd: any, collectionName: string) =>
		AS_FUNCTION(dd[_TOGGLE_], [TYPE, IMAGE_FORMAT, TEXT_FORMAT].some(n => n === collectionName) || null);
	
	componentDidMount() {
		// const
		// 	sharedService: SharedService = this.getService('sharedService');
		
		
	}
	
	render() {
		const
			{blocks} = this;
		
		return <div className={ROOT}>
			<div className={`row`}>
				{blocks.items.map((d: I_BlogConstructorBlock) => this.renderBlock(d))}
				{!blocks.size && <h3 className={`col-12 opacity-3 padding-v-10`}>No Content</h3>}
				<div className={`col-12 text-right padding-v-20`}>
					<EW_Button onClick={() => this.addBlock()}>{
						EW_ButtonModel.buildSquareButton(`Add Block`, `white-button`, <MdAdd />)
					}</EW_Button>
				</div>
			</div>
		</div>;
	}
	
	constructor (props) {
		super(props, true);
		this.blocks = props.blocks;
	}

}
