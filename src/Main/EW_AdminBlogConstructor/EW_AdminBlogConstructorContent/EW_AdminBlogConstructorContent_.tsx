import React from "react";
import {AbstractComponent} from "../../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {COMPONENT, ID, IMAGE, NAME, TEXT, TYPE, VALUE} from "../../../Core/Constants/PropertiesAndAttributes.cnst";
import {_IS_SELECTED_, _TOGGLE_, AbstractCollection} from "../../../Core/Abstract/AbstractCollection";
import {
	IoMdAdd,
	IoMdClose,
	MdFormatBold,
	MdFormatItalic,
	MdFormatQuote,
	MdFormatUnderlined,
	MdImage,
	MdLooks3,
	MdLooksOne,
	MdLooksTwo,
	MdTextFields
} from "react-icons/all";
import {EMPTY_STRING, IS_SELECTED} from "../../../Core/Constants/ViewClasses.cnst";
import {AS_FUNCTION, DIG_OUT, GET_RANDOM_ID} from "../../../Core/Helpers/Helpers.misc";
import {EW_InputModel} from "../../../Components/EW_Input/EW_Input.model";
import {EW_Input} from "../../../Components/EW_Input/EW_Input";
import {TEXT_INPUT} from "../../../Common/Constants/EW_InputTypes.cnst";
import {EW_Button} from "../../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../../Components/EW_Button/EW_Button.model";
import {EW_FileInputModel} from "../../../Components/EW_FileInput/EW_FileInput.model";
import {EW_ImageInputModel} from "../../../Components/EW_ImageInput/EW_ImageInput.model";
import {EW_ImageInput} from "../../../Components/EW_ImageInput/EW_ImageInput";
import {SharedService} from "../../../Services/Shared.service";
import {BlockFormatEnum, BlockSizeEnum, BlockTypeEnum} from "../../../Common/Enums/BlockViewClasses.enum";

const
	ROOT = `ew-admin-blog-constructor-content`,
	CONSTRUCTOR_BLOCKS = `${ROOT}-blocks`,
	CONSTRUCTOR_BLOCK = `${ROOT}-block`,
	CONSTRUCTOR_BLOCK_CONTENT = `${ROOT}-block-content`,
	CONSTRUCTOR_BLOCK_CONTENT_SETTINGS = `${CONSTRUCTOR_BLOCK_CONTENT}-settings`,
	CONSTRUCTOR_BLOCK_CONTENT_MAIN = `${CONSTRUCTOR_BLOCK_CONTENT}-main`,
	CONSTRUCTOR_BLOCK_CONTENT_MAIN_ICON = `${CONSTRUCTOR_BLOCK_CONTENT_MAIN}-icon`,
	CONSTRUCTOR_BLOCK_CONTENT_MAIN_CORE = `${CONSTRUCTOR_BLOCK_CONTENT_MAIN}-core`,
	CONSTRUCTOR_BLOCK_CONTENT_PANEL = `${CONSTRUCTOR_BLOCK_CONTENT}-panel`,
	CONSTRUCTOR_BLOCK_CONTENT_PANEL_ITEM = `${CONSTRUCTOR_BLOCK_CONTENT_PANEL}-item`,
	CONSTRUCTOR_BLOCK_CONTENT_CONTROLS = `${CONSTRUCTOR_BLOCK_CONTENT}-controls`,
	ADD_BLOCK_BUTTON = `${ROOT}-add-block`;

export const
	BLOCK_SIZE = 'blockSize',
	BLOCK_FORMAT = 'blockFormat',
	TEXT_BLOCK = 'textBlock',
	IMAGE_BLOCK = 'imageBlock';

export interface IEW_AdminBlogConstructorContentBlock {
	[ID]: string,
	[TYPE]: AbstractCollection,
	[BLOCK_SIZE]: AbstractCollection,
	[BLOCK_FORMAT]: AbstractCollection,
	[TEXT]: EW_InputModel,
	[IMAGE]: EW_ImageInputModel
}

@inject('sharedService')
@observer
export default class EW_AdminBlogConstructorContent extends AbstractComponent {
	
	private readonly sharedService: SharedService;
	
	private readonly blocks: AbstractCollection;
	
	private getTypesCollection = () => ([
		{
			[NAME]: TEXT_BLOCK,
			[VALUE]: BlockTypeEnum.textBlock,
			[COMPONENT]: <MdTextFields/>
		},
		{
			[NAME]: IMAGE_BLOCK,
			[VALUE]: BlockTypeEnum.imageBlock,
			[COMPONENT]: <MdImage/>
		}
	]);
	
	private getSizesCollection = () => ([
		{
			[NAME]: 'wide',
			[VALUE]: BlockSizeEnum.wide,
			[COMPONENT]: <MdLooksOne/>
		},
		{
			[NAME]: 'medium',
			[VALUE]: BlockSizeEnum.medium,
			[COMPONENT]: <MdLooksTwo/>
		},
		{
			[NAME]: 'small',
			[VALUE]: BlockSizeEnum.small,
			[COMPONENT]: <MdLooks3/>
		}
	]);
	
	private getFormatsCollection = () => ([
		{
			[NAME]: 'bold',
			[VALUE]: BlockFormatEnum.bold,
			[COMPONENT]: <MdFormatBold/>
		},
		{
			[NAME]: 'italic',
			[VALUE]: BlockFormatEnum.italic,
			[COMPONENT]: <MdFormatItalic/>
		},
		{
			[NAME]: 'underline',
			[VALUE]: BlockFormatEnum.underline,
			[COMPONENT]: <MdFormatUnderlined/>
		},
		{
			[NAME]: 'blockquote',
			[VALUE]: BlockFormatEnum.blockquote,
			[COMPONENT]: <MdFormatQuote/>
		}
	]);
	
	private getButtonClassname = () => DIG_OUT(
		this.blocks.items[this.blocks.size - 1], 0,
		BLOCK_SIZE, 'selection', VALUE
	) || BlockSizeEnum.wide;
	
	private getBlockSettingsPanel = (id) => {
		const
			block = this.blocks.getItem(id);
		
		const
			toggleItem = (collectionName, item) => {
				if (collectionName === BLOCK_FORMAT) AS_FUNCTION(item[_TOGGLE_]);
				else !AS_FUNCTION(item[_IS_SELECTED_]) && AS_FUNCTION(item[_TOGGLE_]);
			};
		
		return <div className={CONSTRUCTOR_BLOCK_CONTENT_SETTINGS}>{(block ? [TYPE, BLOCK_SIZE].concat(
			DIG_OUT(block, TYPE, 'selection', NAME) === TEXT_BLOCK ? [BLOCK_FORMAT] : []
		) : []).map((collectionName) => {
			const
				collection = DIG_OUT(block, collectionName) || {},
				items = DIG_OUT(collection, 'items') || [];
			
			return <div className={`
						${CONSTRUCTOR_BLOCK_CONTENT_PANEL}
						display-inline-flex align-left`}
			            key={`${id}_${collectionName}`}>{items.map(d =>
				<div className={`
							${CONSTRUCTOR_BLOCK_CONTENT_PANEL_ITEM}
							${AS_FUNCTION(d[_IS_SELECTED_]) ? IS_SELECTED : EMPTY_STRING}
							display-inline-flex align-center
						`} key={`${id}_${collectionName}_${d[NAME]}`}
				     onClick={() => toggleItem(collectionName, d)}>{d[COMPONENT]}</div>
			)}</div>
		})
		}</div>;
	};
	
	private addBlock = () => {
		const
			newBlock: IEW_AdminBlogConstructorContentBlock = {
				[ID]: GET_RANDOM_ID(),
				[TYPE]: new AbstractCollection(NAME).addItems(this.getTypesCollection()).selectFirst(),
				[BLOCK_SIZE]: new AbstractCollection(NAME).addItems(this.getSizesCollection()),
				[BLOCK_FORMAT]: new AbstractCollection(NAME, true).addItems(this.getFormatsCollection()),
				[TEXT]: new EW_InputModel({
					type: TEXT_INPUT,
					placeholder: 'Text block'
				}),
				[IMAGE]: new EW_FileInputModel()
			},
			previousBlockSize = DIG_OUT(this.blocks.items[this.blocks.size - 1], BLOCK_SIZE, 'selection', NAME);
		
		this.blocks.addItems(newBlock);
		
		previousBlockSize ?
			newBlock[BLOCK_SIZE].select(previousBlockSize) :
			newBlock[BLOCK_SIZE].selectFirst();
	};
	
	componentDidMount() {
		const
			{sharedService, blocks} = this,
			{showBlogConstructor} = sharedService;
		
		this.registerSubscriptions(
			showBlogConstructor.getSubscription(
				state => state && blocks.clearItems()
			)
		);
		
	}
	
	render() {
		const
			{blocks} = this;
		
		return <div className={ROOT}>
			<div className={`${CONSTRUCTOR_BLOCKS}`}>
				<div className={`row`}>
					{blocks.items.map((d: IEW_AdminBlogConstructorContentBlock, i) =>
						<div className={`
							${CONSTRUCTOR_BLOCK}
							${DIG_OUT(d[BLOCK_SIZE].selection, VALUE) || EMPTY_STRING}
							${DIG_OUT(d[TYPE].selection, NAME) === TEXT_BLOCK ?
							(d[BLOCK_FORMAT].selection || []).reduce((str, dd) => str + `${DIG_OUT(dd, VALUE)} `, EMPTY_STRING) :
							EMPTY_STRING
						} padding-v-10`} key={d[ID]}>
							<div className={CONSTRUCTOR_BLOCK_CONTENT}>
								{this.getBlockSettingsPanel(d[ID])}
								<div className={`${CONSTRUCTOR_BLOCK_CONTENT_MAIN} padding-10`}>
									{DIG_OUT(d[TYPE].selection, NAME) === TEXT_BLOCK ?
										<div className={`pos-rel`}>
											<div className={`${CONSTRUCTOR_BLOCK_CONTENT_MAIN_ICON}
													full-height pos-abs top-0 font-size-50 opacity-5`}>
												<MdFormatQuote/>
											</div>
											<div className={`${CONSTRUCTOR_BLOCK_CONTENT_MAIN_CORE} ew-block-height-3`}>
												<EW_Input className={`display-block size-cover`} model={d[TEXT]}/>
											</div>
										</div> :
										<div className={`pos-rel`}>
											<div className={`${CONSTRUCTOR_BLOCK_CONTENT_MAIN_CORE} ew-block-height-3`}>
												<EW_ImageInput className={`display-block full-width full-height`} model={d[IMAGE]}>
													<div className={`size-cover display-flex flex-direction-column align-center opacity-5`}>
														<h4>Add image</h4>
														<MdImage className={`font-size-50`}/>
													</div>
												</EW_ImageInput>
											</div>
										</div>
									}
								</div>
								<div className={`${CONSTRUCTOR_BLOCK_CONTENT_CONTROLS} padding-10 text-right`}>
									<EW_Button onClick={e => blocks.removeItem(d[ID])}>{EW_ButtonModel.buildSquareButton(
										'REMOVE', 'red-button', <IoMdClose/>
									)}</EW_Button>
								</div>
							</div>
						</div>
					)}
					<div className={`${this.getButtonClassname()} padding-v-10`}>
						<div className={`${ADD_BLOCK_BUTTON} display-flex align-center ew-clickable ew-block-height-3 padding-10`}
						     onClick={e => this.addBlock()}>
							<div className={`text-center ew-bg-grey`}>
								<IoMdAdd/>
								<p>Add block</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>;
	}
	
	constructor(props) {
		super(props);
		this.sharedService = props.sharedService;
		this.blocks = props.blocks;
	}
}

