import React, {lazy, Suspense} from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {inject, observer} from "mobx-react";
import {SharedService} from "../../Services/Shared.service";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {IoMdAdd, IoMdCheckmark, IoMdClose} from "react-icons/all";
import {EW_InputModel} from "../../Components/EW_Input/EW_Input.model";
import {EW_Input} from "../../Components/EW_Input/EW_Input";
import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {
	CONTENT,
	FILE,
	ID,
	IMAGE,
	IMAGES,
	NAME,
	TEXT,
	TYPE,
	VALUE
} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {
	BLOCK_SIZE,
	CONTENT_ITEMS,
	I_BlogConstructorBlock,
	I_BlogConstructorBlockContent,
	IMAGE_BANNER,
	IMAGE_BLOCK,
	IMAGE_DECORATION,
	IMAGE_FORMAT,
	TEXT_BLOCK,
	TEXT_DECORATION,
	TEXT_FORMAT
} from "./EW_AdminBlogConstructorContent/EW_AdminBlogConstructorContent";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {AS_ARRAY, DIG_OUT, GET_RANDOM_ID} from "../../Core/Helpers/Helpers.misc";
import {UserService} from "../../Services/User.service";
import {AccessPointService} from "../../Services/AccessPoint.service";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import EW_Spinner from "../../Components/EW_Spinner/EW_Spinner";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";
import {LOGIN} from "../../Common/Constants/EW_User.cnst";
import {POST} from "../../Common/Constants/EW_Routes.cnst";

const
	EW_Modal = lazy(() => import('../../Components/EW_Modal/EW_Modal')),
	EW_AdminBlogConstructorTags = lazy(() => import('./EW_AdminBlogConstructorTags/EW_AdminBlogConstructorTags')),
	EW_AdminBlogConstructorContent = lazy(() => import('./EW_AdminBlogConstructorContent/EW_AdminBlogConstructorContent'));

const
	ROOT = `ew-admin-blog-constructor`,
	TRIGGER = `${ROOT}-trigger`,
	MODAL_FORM = `${ROOT}-modal-form`;

@inject('sharedService', 'userService', 'accessPointService')
@observer
export default class EW_AdminBlogConstructor extends AbstractComponent {
	
	private readonly sharedService: SharedService;
	
	private readonly userService: UserService;
	
	private readonly accessPointService: AccessPointService;
	
	private readonly isLoading: BooleanObservable = new BooleanObservable();
	
	private readonly isFinished: BooleanObservable = new BooleanObservable();
	
	private readonly postHeader: EW_InputModel = new EW_InputModel({
		placeholder: 'Post Header'
	});
	
	private readonly cancelButton: EW_ButtonModel = new EW_ButtonModel();
	
	private readonly saveButton: EW_ButtonModel = new EW_ButtonModel();
	
	private readonly blocks: AbstractCollection = new AbstractCollection();
	
	private readonly tags: AbstractCollection = new AbstractCollection(NAME);
	
	private openBlogConstructor = () => this.sharedService.showBlogConstructor.setValue(true);
	
	componentDidMount() {
		const
			{
				sharedService, userService, accessPointService,
				blocks, tags, saveButton, cancelButton, postHeader,
				isLoading, isFinished
			} = this,
			{showBlogConstructor} = sharedService;
		
		const
			postData = {};
		
		const
			stateObserver: AbstractObserver = new AbstractObserver(() =>
				// !!postHeader.currentValue &&
				// !!blocks.items.length &&
				blocks.items.every((d: I_BlogConstructorBlock) =>
					d[CONTENT_ITEMS].items.every((d: I_BlogConstructorBlockContent) => d[TEXT].currentValue || d[IMAGE].currentValue)
				)
			);
		
		const
			savePostPipe: AbstractPipe = new AbstractPipe([
				() => isLoading.setValue(true),
				() => {
					const
						uid = GET_RANDOM_ID('blog_post'),
						contentBlocks: any[] = [],
						images: any = {};
					
					blocks.items.forEach((d: I_BlogConstructorBlock) => {
						const
							items: any[] = [];
						
						d[CONTENT_ITEMS].items.forEach((dd: I_BlogConstructorBlockContent) => {
							const
								type = DIG_OUT(dd[TYPE].selection, NAME);
						
							if (type === TEXT_BLOCK) {
								const
									renderingSettings = [
										DIG_OUT(dd[TEXT_FORMAT].selection, VALUE)
									].concat(
										AS_ARRAY(dd[TEXT_DECORATION].selection).map(ddd => DIG_OUT(ddd, VALUE))
									).filter(val => !!val);
								
								dd[TEXT].currentValue.split(/\n/).forEach((str: string, i: number) => str && str.length && items.push({
									id: `${dd[ID]}${i ? '_' + i : EMPTY_STRING}`,
									type: type,
									format: renderingSettings,
									data: str
								}));
								
							} else if (type === IMAGE_BLOCK) {
								const
									renderingSettings = [
										DIG_OUT(dd[IMAGE_FORMAT].selection, VALUE)
									].concat(DIG_OUT(dd[IMAGE_DECORATION].selection, VALUE) || []).filter(val => !!val),
									image = dd[IMAGE].currentValue,
									fileName = `uploaded/${uid}/${dd[ID]}.${(DIG_OUT(image, NAME) || '').split('.').slice(-1)[0]}`;
								
								items.push({
									id: dd[ID],
									type: type,
									format: renderingSettings,
									banner: dd[IMAGE_BANNER].currentValue || EMPTY_STRING,
									data: fileName
								});
								
								images[fileName] = DIG_OUT(image, FILE);
							}
						});
						
						items.length && contentBlocks.push({
							id: d[ID],
							size: DIG_OUT(d[BLOCK_SIZE].selection, VALUE),
							content: items
						});
					});
					
					postData[CONTENT] = {
						id: uid,
						author: userService.user[LOGIN],
						header: postHeader.currentValue,
						content: contentBlocks,
						date: new Date().getTime(),
						likes: 0,
						comments: [],
						tags: tags.items.map(d => d[NAME])
					};
					postData[IMAGES] = images;
				},
				() => accessPointService.saveFiles(postData[IMAGES], postData[CONTENT].id),
				() => accessPointService.sendPost(POST.CREATE_POST, postData[CONTENT]),
				() => window.location.reload()
			], false);
		
		this.registerSubscriptions(
			
			showBlogConstructor.getSubscription(
				state => state && postHeader.clearValue()
			),
			
			stateObserver.getSubscription(
				state => saveButton.isDisabled.setValue(!state)
			),
			
			saveButton.onClick.getSubscription(
				() => savePostPipe.run()
			),
			
			cancelButton.onClick.getSubscription(
				() => showBlogConstructor.resetValue()
			),
			
			savePostPipe.catch(
				err => sharedService.growl
					.addMessage(EW_GrowlModel.buildErrorMessage(err), 'error')
					.show()
			),
			
			savePostPipe.finally(
				() => isLoading.setValue(false)
			)
			
		);
	}
	
	render () {
		const
			{props, sharedService, postHeader, cancelButton, saveButton, isLoading} = this,
			{label, className, icon} = props;
		
		return <div className={`${ROOT}`}>
			<EW_Button className={`${TRIGGER}`} onClick={e => this.openBlogConstructor()}>{EW_ButtonModel.buildSquareButton(
				label || 'Create Post',
				className || EMPTY_STRING,
				icon || <IoMdAdd />
			)}</EW_Button>
			<Suspense fallback={EMPTY_STRING}>
				<EW_Modal className={`${MODAL_FORM} ew-bg-white padding-50 margin-v-50 text-left`}
				          showCloseButton={true}
				          isOpened={sharedService.showBlogConstructor}>
					<div className={`container pos-rel`}>
						<div className={`row`}>
							<div className={`col-12`}>
								<h2>Create post</h2>
								<hr />
							</div>
						</div>
						<div className={`row margin-bottom-10`}>
							<div className={`col-3`}>
								<h5 className={`text-right padding-top-10`}>HEADER</h5>
							</div>
							<div className={`col-9`}>
								<EW_Input className={`display-block full-width`} model={postHeader} />
							</div>
						</div>
						<div className={`row margin-bottom-10`}>
							<div className={`col-3`}>
								<h5 className={`text-right padding-top-10`}>TAGS</h5>
							</div>
							<div className={`col-9`}>
								<Suspense fallback={EMPTY_STRING}><EW_AdminBlogConstructorTags tags={this.tags}/></Suspense>
							</div>
							
						</div>
						<div className={`row`}>
							<div className={`col-12`}>
								<hr />
								<Suspense fallback={EMPTY_STRING}><EW_AdminBlogConstructorContent blocks={this.blocks}/></Suspense>
								<hr />
							</div>
							<hr />
						</div>
						<div className={`row`}>
							<div className={`col-12 text-right`}>
								<EW_Button model={cancelButton}>{
									EW_ButtonModel.buildSquareButton('Cancel', 'red-button', <IoMdClose />)
								}</EW_Button>
								<EW_Button className={`margin-left-10`} model={saveButton}>{
									EW_ButtonModel.buildSquareButton('Save', '', <IoMdCheckmark />)
								}</EW_Button>
							</div>
						</div>
						{isLoading.value && <div className={`pos-abs top-0 left-0 size-cover`}>
							<EW_Spinner />
						</div>}
					</div>
				</EW_Modal>
			</Suspense>
		</div>;
	}
	
	constructor(props) {
		super(props);
		this.sharedService = props.sharedService;
		this.userService = props.userService;
		this.accessPointService = props.accessPointService;
	}
	
}

