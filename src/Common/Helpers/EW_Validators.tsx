import {
  AS_ARRAY,
  DIG_OUT,
  GET_TRUE,
  IS_IN_ARRAY,
  IS_NOT_NULL,
  IS_NOT_UNDEFINED
} from "../../Core/Helpers/Helpers.misc";
import {EMAIL_REG_EXP, PHONE_REG_EXP, SIZE_REG_EXP} from "../Constants/EW_RegularExpressions.cnst";
import {DATA, NAME, SIZE, TYPE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {FILE_SIZES} from "./EW_FileSizes";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";

export const
  ALWAYS_TRUE = () => GET_TRUE,
  
  IS_REQUIRED = () => (val) => IS_NOT_NULL(val) && IS_NOT_UNDEFINED(val) && (`${val}`.length > 0),
  
  IS_VALID_REG_EXP = (regExp) => (val) => regExp.test(val),
  
  IS_VALID_EMAIL = () => {
    let validator = IS_VALID_REG_EXP(EMAIL_REG_EXP);
    return val => validator(val);
  },
  
  IS_VALID_PHONE = () => {
    let validator = IS_VALID_REG_EXP(PHONE_REG_EXP);
    return val => validator(val);
  },
  
  IS_VALID_FILE_TYPE = (allowedTypes: string | string[]) => (file) => AS_ARRAY(allowedTypes).some(
    type => type.test(DIG_OUT(file, NAME))
  ),
  
  IS_VALID_FILE_SIZE = (allowedSize: any) => {
    let _allowedSize = 0;
    if (!isNaN(allowedSize)) {
      _allowedSize = allowedSize;
    } else {
      let
        fileStringData = SIZE_REG_EXP.exec(`${allowedSize}`),
        KMGByte = `${DIG_OUT(fileStringData, '1')}`.toUpperCase(),
        multi = FILE_SIZES[KMGByte] || 0;
      
      _allowedSize = +(`${allowedSize}`.toUpperCase().replace(KMGByte, EMPTY_STRING)) * multi;
    }
    return (file) => (DIG_OUT(file, DATA, SIZE) || 0) <= _allowedSize;
  }
  
  

