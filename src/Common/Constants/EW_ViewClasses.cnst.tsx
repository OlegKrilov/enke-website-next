export const
  EW_SECTION_HEADER = 'ew-color-blue text-uppercase font-size-12 letter-spacing-70',
  EW_PRIMARY_BANNER = 'ew-first-class-header',
  EW_SECONDARY_BANNER = 'ew-second-class-header',
  EW_PRIMARY_TEXT = 'ew-color-light font-size-18',
  EW_SECONDARY_TEXT = 'font-size-15',
  EW_DECORATION_BLOCK_BLUE = 'ew-bg-blue ew-block-width ew-block-height pos-abs',
  EW_DECORATION_BLOCK_WHITE = 'ew-bg-white ew-block-width ew-block-height pos-abs',
  EW_SPECIAL_BLOCK_GRAY = 'ew-bg-gray ew-block-width-2 ew-block-height-2 pos-abs',
  EW_SPECIAL_BLOCK_WHITE = 'ew-bg-white ew-block-width-2 ew-block-height-2 pos-abs';

