export interface I_EW_BlogPost {
	id: string,
	author: string,
	header: string,
	content: I_EW_BlogPostContent[],
	date: number,
	likes: number,
	comments: any[],
	tags: string[]
}

export interface I_EW_BlogPostContent {
	id: string,
	type: string,
	size: string,
	format: string[],
	data: any
}
