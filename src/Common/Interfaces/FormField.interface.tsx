import {CLASSNAME, MODEL, NAME, TYPE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EW_InputModel} from "../../Components/EW_Input/EW_Input.model";
import {EW_FileInputModel} from "../../Components/EW_FileInput/EW_FileInput.model";

export interface I_EW_FormField {
  [NAME]: string,
  [MODEL]: EW_InputModel | EW_FileInputModel,
  [CLASSNAME]: string, 
  [TYPE]?: string
}
