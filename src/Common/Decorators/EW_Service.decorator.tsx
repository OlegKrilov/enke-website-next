import {IS_READY} from "../../Core/Constants/ViewClasses.cnst";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {TOGGLE} from "../../Core/Constants/Methods.cnst";

export function EW_ServiceDecorator (obj) {

  const isReady: BooleanObservable = new BooleanObservable();

  Object.defineProperties(obj, {
    [IS_READY]: {
      get: () => isReady.value
    },
    [TOGGLE]: {
      value: () => {
        console.log('asasdasd');
        isReady.toggleValue()
      }
    }
  });

  return obj;
}

