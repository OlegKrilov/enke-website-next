import React from "react";

export enum BlockSizeEnum {
	wide = 'col-12',
	medium = 'col-12 col-lg-6 col-xl-6',
	small = 'col-6 col-lg-4 col-xl-4'
}

export enum BlockFormatEnum {
	bold = 'ew-font-weight-bold',
	italic = 'ew-font-weight-italic',
	underline = 'ew-font-decoration-underline',
	blockquote = 'ew-font-decoration-blockquote'
}

export enum BlockTypeEnum {
	textBlock = 'use-text-block',
	imageBlock = 'use-image-block',
}

export enum TextDecorationEnum {
	bold = 'use-font-weight-bold',
	italic = 'use-font-weight-italic',
	underline = 'use-font-decoration-underline',
	blockquote = 'use-font-decoration-blockquote'
}

export enum TextFormatEnum {
	paragraph = 'render-as-paragraph',
	header1 = 'render-as-header-1',
	header2 = 'render-as-header-2',
	header3 = 'render-as-header-3'
}

export enum ImageDecorationEnum {
	hasLogo = 'image-has-logo',
	hasBanner = 'image-has-banner',
	invertedColor = 'use-inverted-colors'
}

export enum ImageFormatEnum {
	cover = 'display-size-cover',
	contain = 'display-size-contain'
}

