import React, {Suspense, lazy} from "react";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {inject, observer} from "mobx-react";
import {
  EW_DECORATION_BLOCK_BLUE, EW_DECORATION_BLOCK_WHITE,
  EW_PRIMARY_BANNER,
  EW_PRIMARY_TEXT, EW_SECONDARY_BANNER, EW_SECONDARY_TEXT,
  EW_SECTION_HEADER
} from "../../Common/Constants/EW_ViewClasses.cnst";
import {EW_Icon} from "../../Components/EW_Icon/EW_Icon";
import {EW_AnimatedSection} from "../../Components/EW_AnimatedSection/EW_AnimatedSection";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {SharedService} from "../../Services/Shared.service";
import {DIG_OUT, PIPE} from "../../Core/Helpers/Helpers.misc";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {EW_PublicPage} from "../EW_PublicPage";
import {EW_TechnologiesStack} from "../../Main/EW_TechnologiesStack/EW_TechnologiesStack";
import {POST} from "../../Common/Constants/EW_Routes.cnst";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {CASE_STUDY_FORM, CONTACTS_FORM, EW_FormModel} from "../../Components/EW_Form/EW_Form.model";
import {DATA, TOP} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";

const
  EW_Modal = lazy(() => import('../../Components/EW_Modal/EW_Modal')),
  EW_Form = lazy(() => import('../../Components/EW_Form/EW_Form'));

const
  ROOT = `ew-home-page`,
  PAGE_HEADER = `${ROOT}-header`,
  OFFERS_SECTION_TAB = `${ROOT}-offers-section-tab`,
  OFFERS_SECTION_TAB_HEADER = `${OFFERS_SECTION_TAB}-header`,
  OFFERS_SECTION_TAB_TEXT = `${OFFERS_SECTION_TAB}-text`,
  OFFERS_SECTION_TAB_ICON = `${OFFERS_SECTION_TAB}-icon`,
  CONTACT_FORM = `${ROOT}-contact-form`,
  MODAL_FORM = `${ROOT}-modal-form`,
  MODAL_FORM_CONTENT = `${MODAL_FORM}-content`;

const
  DEFAULT_BG = 'Photos/Common/bg.png';
  
@observer
export default class EW_HomePage extends EW_PublicPage {

  private readonly form: EW_FormModel = new EW_FormModel(CONTACTS_FORM);
  
  private readonly modalForm: any = {
    isOpened: new BooleanObservable(),
    core: new EW_FormModel(CASE_STUDY_FORM)
  };
  
  private getOurOffersTabs = HOME_PAGE_DATA => HOME_PAGE_DATA.SIXTH_SECTION.ITEMS.map((d, i) =>
    <div className={`col-12 col-lg-4 col-xl-4 margin-v-10 ew-hidden-bottom transition-duration-10 transition-delay-${6 + (2 * i)}`} key={i}>
      <div className={`${OFFERS_SECTION_TAB} ew-bg-blue ew-color-white full-height padding-50 padding-right-70 pos-rel`}>
        <div className={`pos-abs top-0 right-0 opacity-5 ew-block-width-0-and-half ew-block-height-0-and-half ew-bg-white`} />
        <div className={`pos-abs top-0 right-50 opacity-3 ew-block-width-0-and-half ew-block-height-0-and-half ew-bg-white`} />
        <div className={`pos-abs top-50 right-0 opacity-3 ew-block-width-0-and-half ew-block-height-0-and-half ew-bg-white`} />
        {d.BACKGROUND && <div className={`pos-abs top-0 left-0 full-width full-height`}>
          <EW_BackgroundImage source={d.BACKGROUND} />
        </div>}

        <div className={`${OFFERS_SECTION_TAB_ICON} margin-bottom-40 pos-rel`}>
          <EW_Icon source={d.ICON} />
        </div>
        <div className={`${OFFERS_SECTION_TAB_HEADER} font-size-22 margin-bottom-40 pos-rel`}>
          <b>{d.HEADER}</b>
        </div>
        <div className={`${OFFERS_SECTION_TAB_TEXT} font-size-15 pos-rel`}>
          <b>{d.TEXT}</b>
        </div>
      </div>
    </div>
  );
  
  private scrollToForm = () => {
    const
      formEl = document.querySelector(`.${CONTACT_FORM}`),
      scrollTarget = DIG_OUT((formEl as HTMLDivElement).getBoundingClientRect(), TOP);
    
    window.scroll({
      top: window.scrollY + scrollTarget,
      left: 0,
      behavior: 'smooth'
    });
  };

  private submitForm = data => {
    const
      {accessPointService, form} = this;

    const
      requestPipe = new AbstractPipe([
        () => form.isSubmitted.setValue(true),
        () => accessPointService.sendForm(Object.assign({theme: 'general'}, data), POST.CREATE_GENERAL_MESSAGE)
      ]);

    this
      .registerPipes(requestPipe)
      .registerSubscriptions(
        requestPipe.then(res => form.successMessage.setValue(res.statusText)),
        requestPipe.catch(err => form.errorMessage.setValue(err.statusText)),
        requestPipe.finally(() => form.isSubmitted.setValue(false))
      );
  };
  
  private submitModalForm = data => {
    const
      {accessPointService, modalForm} = this;
    
    const
      requestPipe: AbstractPipe = new AbstractPipe([
        () => modalForm.core.isSubmitted.setValue(true),
        () => accessPointService.sendForm(Object.assign({theme: 'studyCase'}, data), POST.CREATE_CASE_STUDY_MESSAGE)
      ]);
    
    this
      .registerPipes(requestPipe)
      .registerSubscriptions(
        requestPipe.then(res => modalForm.core.successMessage.setValue(res.statusText)),
        requestPipe.catch(err => modalForm.core.errorMessage.setValue(err.statusText)),
        requestPipe.finally(() => modalForm.core.isSubmitted.setValue(false))
      );
  };
  
  componentDidMount(): void {
    super.componentDidMount();
    const {form, modalForm} = this;

    this.registerSubscriptions(
      form.onSubmit.getSubscription(
        e => this.submitForm(e[DATA])
      ),
      modalForm.isOpened.getSubscription(
        state => state && modalForm.core.resetForm()
      ),
      modalForm.core.onSubmit.getSubscription(
        e => this.submitModalForm(e[DATA])
      )
    );
  }

  render () {
    const
      {isActive, pageData, form, modalForm} = this,
      HOME_PAGE_DATA = pageData.value;

    return <div className={`${ROOT}`}>
      {
        pageData.isEmpty ?
          <h3 className={`opacity-3 padding-100`}>IS LOADING...</h3> :
          <article>
            <EW_AnimatedSection isActive={isActive} source={HOME_PAGE_DATA.FIRST_SECTION} children={
              <section className={`${PAGE_HEADER} pos-rel ew-section ew-bg-white padding-bottom-100`}>
                <div className={`pos-abs bottom-0 left-0 full-width ew-bg-grey-light ew-block-height-1-and-quarter`}/>
                {this.sharedService.isWideScreen && <div className={`pos-abs half-width full-height top-0 right-0 hidden-md-down`}>
						      <div className={`full-height pos-rel ew-hidden-right transition-duration-10 transition-delay-3`}>
							      <EW_BackgroundImage source={HOME_PAGE_DATA.FIRST_SECTION.BANNER}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} top-0 left-0 opacity-8`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} top-100 left-0 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} top-0 left-100 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 left-0 opacity-8`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-100 left-0 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 right-100 opacity-8`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 right-200 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-100 right-100 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} full-height bottom-0 right-0`}/>
							      <div className={`${EW_DECORATION_BLOCK_WHITE} bottom-0 right-0 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_WHITE} bottom-100 right-0 opacity-2`}/>
						      </div>
					      </div>}
                <div className={`container`}>
                  <div className={`row`}>
                    <div className={`col-12 col-lg-6`}>
                      <div
                        className={`ew-block-height display-flex ew-hidden-left transition-duration-10 transition-delay-3`}>
                        <b className={`${EW_SECTION_HEADER}`}>{HOME_PAGE_DATA.FIRST_SECTION.SECTION_LABEL}</b>
                      </div>
                      <div className={`${EW_PRIMARY_BANNER} ew-hidden-left transition-duration-10 transition-delay-5`}>
                        <b>{HOME_PAGE_DATA.FIRST_SECTION.HEADER}</b>
                      </div>
                      <div
                        className={`${EW_PRIMARY_TEXT} margin-top-40 padding-right-100 ew-hidden-left transition-duration-10 transition-delay-7`}>
                        <span>{HOME_PAGE_DATA.FIRST_SECTION.TEXT}</span>
                      </div>
                      <div className={`margin-top-40 ew-hidden-bottom transition-duration-10 transition-delay-9`}>
                        <EW_Button onClick={(e) => this.scrollToForm()} children={
                          EW_ButtonModel.buildSquareButton(HOME_PAGE_DATA.FIRST_SECTION.BUTTON_LABEL)
                        }/>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            }/>
      
            <EW_AnimatedSection isActive={isActive} source={HOME_PAGE_DATA.SECOND_SECTION} children={
              <section className={`ew-section ew-bg-grey-light padding-v-100`}>
                <div className={`container`}>
                  <div className={`row`}>
                    <div className={`col-12 col-lg-8`}>
                      <div
                        className={`ew-block-height display-flex ew-hidden-left transition-duration-10 transition-delay-3`}>
                        <b className={`${EW_SECTION_HEADER}`}>{HOME_PAGE_DATA.SECOND_SECTION.SECTION_LABEL}</b>
                      </div>
                      <div className={`${EW_SECONDARY_BANNER} ew-hidden-right transition-duration-10 transition-delay-6`}>
                        <b>{HOME_PAGE_DATA.SECOND_SECTION.HEADER}</b>
                      </div>
                      <div className={`margin-v-40 ew-hidden-left transition-duration-10 transition-delay-9`}>
                        <span className={`${EW_SECONDARY_TEXT}`}>{HOME_PAGE_DATA.SECOND_SECTION.DESCRIPTION}</span>
                      </div>
                    </div>
                    <EW_TechnologiesStack className={`col-12 ew-hidden-right transition-duration-10 transition-delay-12`} />
                  </div>
                </div>
              </section>
            }/>
      
            <EW_AnimatedSection isActive={isActive} source={HOME_PAGE_DATA.THIRD_SECTION} children={
              <section className={`ew-section pos-rel ew-bg-white padding-bottom-100 margin-bottom-200`}>
                <div className={`pos-abs top-0 left-0 full-width ew-block-height-2 ew-bg-grey-light`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-white top-100 right-0 opacity-7`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-white top-0 right-0 opacity-4`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-white top-100 right-100 opacity-4`}/>
                <div className={`full-height pos-abs half-width top-0 left-0`}>
                  {this.sharedService.isWideScreen && <div
							      className={`full-height pos-rel hidden-md-down ew-hidden-bottom transition-duration-10 transition-delay-3`}>
							      <EW_BackgroundImage source={HOME_PAGE_DATA.THIRD_SECTION.BANNER}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light top-0 left-0 opacity-9`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light top-0 right-0 opacity-9`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light top-0 right-100 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light top-100 right-0 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 left-0 opacity-8`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-100 left-0 opacity-4`}/>
						      </div>}
                </div>
                <div className={`container`}>
                  <div className={`row`}>
                    <div className={`col-6 hidden-md-down`}/>
                    <div className={`col-12 col-lg-6 col-xl-6`}>
                      <div className={`padding-left-100`}>
                        <div
                          className={`ew-block-height display-flex ew-hidden-right transition-duration-10 transition-delay-6`}>
                          <EW_Icon source={HOME_PAGE_DATA.THIRD_SECTION.ICON}/>
                        </div>
                        <div
                          className={`ew-block-height display-flex ew-hidden-right transition-duration-10 transition-delay-9`}>
                          <b className={`${EW_SECTION_HEADER}`}>{HOME_PAGE_DATA.THIRD_SECTION.SECTION_LABEL}</b>
                        </div>
                        <div className={`${EW_SECONDARY_BANNER} ew-hidden-right transition-duration-10 transition-delay-12`}>
                          <b>{HOME_PAGE_DATA.THIRD_SECTION.HEADER}</b>
                        </div>
                        <div
                          className={`${EW_SECONDARY_TEXT} margin-top-40 ew-hidden-right transition-duration-10 transition-delay-15`}>
                          <span>{HOME_PAGE_DATA.THIRD_SECTION.TEXT}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            }/>
      
            <EW_AnimatedSection isActive={isActive} source={HOME_PAGE_DATA.FOURTH_SECTION} children={
              <section className={`ew-section pos-rel padding-bottom-200 ew-bg-white`}>
                <div className={`pos-abs bottom-0 left-0 full-width ew-block-height-2-and-quarter ew-bg-grey-light`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light bottom-200 left-0`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light bottom-200 right-0`}/>
                <div className={`hidden-md-down pos-rel full-height`}>
                  <div
                    className={`pos-abs ew-block-width-3 ew-block-height-5 top-0 left-0 ew-hidden-left transition-duration-10 transition-delay-6`}>
                    <EW_BackgroundImage source={HOME_PAGE_DATA.FOURTH_SECTION.BANNER_LEFT}/>
                  </div>
                  <div
                    className={`pos-abs ew-block-width-3 ew-block-height-5 top-0 right-0 ew-hidden-right transition-duration-10 transition-delay-6`}>
                    <EW_BackgroundImage source={HOME_PAGE_DATA.FOURTH_SECTION.BANNER_RIGHT}/>
                  </div>
                </div>
                <div className={`container`}>
                  <div className={`row`}>
                    <div className={`hidden-md-down col-3`}/>
                    <div className={`col-12 col-lg-6 col-xl-6`}>
                      <div className={`display-flex flex-direction-column`}>
                        <div
                          className={`ew-block-height display-flex align-center ew-hidden-top transition-duration-10 transition-delay-9`}>
                          <EW_Icon source={HOME_PAGE_DATA.FOURTH_SECTION.ICON}/>
                        </div>
                        <div
                          className={`ew-block-height display-flex ew-hidden-top transition-duration-10 transition-delay-12`}>
                          <b className={`${EW_SECTION_HEADER}`}>{HOME_PAGE_DATA.FOURTH_SECTION.SECTION_LABEL}</b>
                        </div>
                        <div className={`${EW_SECONDARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-15`}>
                          <b>{HOME_PAGE_DATA.FOURTH_SECTION.HEADER}</b>
                        </div>
                        <div
                          className={`${EW_SECONDARY_TEXT} margin-top-40 ew-hidden-top transition-duration-10 transition-delay-18`}>
                          <span>{HOME_PAGE_DATA.FOURTH_SECTION.TEXT}</span>
                        </div>
                        <div className={`margin-top-40 ew-hidden-bottom transition-duration-10 transition-delay-20`}>
                          <EW_Button onClick={(e) => modalForm.isOpened.toggleValue(true)} children={
                            EW_ButtonModel.buildSquareButton(HOME_PAGE_DATA.FOURTH_SECTION.BUTTON_LABEL)
                          }/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            }/>
      
            <EW_AnimatedSection isActive={isActive} source={HOME_PAGE_DATA.FIFTH_SECTION} children={
              <section className={`pos-rel ew-bg-white margin-bottom-100`}>
                <div className={`pos-abs top-0 left-0 full-width ew-block-height-2 ew-bg-grey-light`}/>
                <div className={`hidden-md-down full-height pos-abs top-0 right-0 half-width`}>
                  {this.sharedService.isWideScreen && <div className={`full-height pos-rel ew-hidden-left transition-duration-10 transition-delay-3`}>
							      <EW_BackgroundImage source={HOME_PAGE_DATA.FIFTH_SECTION.BANNER}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light top-0 left-0`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light top-100 left-0 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} ew-bg-grey-light top-0 left-100 opacity-4`}/>
							      <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 right-0 opacity-8`}/>
						      </div>}
                </div>
                <div className={`container`}>
                  <div className={`row`}>
                    <div className={`col-12 col-lg-6`}>
                      <div
                        className={`ew-block-height display-flex ew-hidden-right transition-duration-10 transition-delay-6`}>
                        <b className={`${EW_SECTION_HEADER}`}>{HOME_PAGE_DATA.FIFTH_SECTION.SECTION_LABEL}</b>
                      </div>
                      <div
                        className={`ew-block-height display-flex ${EW_SECONDARY_BANNER} ew-hidden-right transition-duration-10 transition-delay-9`}>
                        <b>{HOME_PAGE_DATA.FIFTH_SECTION.HEADER}</b>
                      </div>
                      <div
                        className={`${EW_PRIMARY_TEXT} margin-top-40 padding-right-100 ew-hidden-right transition-duration-10 transition-delay-12`}>
                        <span>{HOME_PAGE_DATA.FIFTH_SECTION.TEXT}</span>
                      </div>
                      {/*<div className={`margin-top-40 ew-hidden-right transition-duration-10 transition-delay-15`}>*/}
                      {/*  <EW_Button onClick={(e) => console.log(e)} children={*/}
                      {/*    EW_ButtonModel.buildSquareButton(HOME_PAGE_DATA.FIFTH_SECTION.BUTTON_LABEL)*/}
                      {/*  }/>*/}
                      {/*</div>*/}
                    </div>
                  </div>
                </div>
              </section>
            }/>
      
            <EW_AnimatedSection isActive={isActive} source={HOME_PAGE_DATA.SIXTH_SECTION} children={
              <section className={`ew-section pos-rel padding-top-50 ew-bg-grey-light padding-bottom-100`}>
                <div className={`container`}>
                  <div className={`row`}>
                    <div className={`col-12 text-center margin-bottom-100`}>
                      <div
                        className={`ew-block-height display-flex align-center ew-hidden-top transition-duration-10 transition-delay-3`}>
                        <b className={`${EW_SECTION_HEADER}`}>{HOME_PAGE_DATA.SIXTH_SECTION.SECTION_LABEL}</b>
                      </div>
                      <div
                        className={`${EW_PRIMARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-6`}>
                        <b>{HOME_PAGE_DATA.SIXTH_SECTION.HEADER}</b>
                      </div>
                      <div
                        className={`${EW_PRIMARY_TEXT} margin-top-40 padding-h-50 ew-hidden-top transition-duration-10 transition-delay-9`}>
                        <span>{HOME_PAGE_DATA.SIXTH_SECTION.TEXT}</span>
                      </div>
                    </div>
                    {this.getOurOffersTabs(HOME_PAGE_DATA)}
                  </div>
                </div>
              </section>
            }/>
      
            <EW_AnimatedSection isActive={isActive} source={HOME_PAGE_DATA.SEVENTH_SECTION} children={
              <section className={`ew-section ew-bg-blue pos-rel display-flex padding-v-100`}>
                <div className={`pos-abs top-0 left-0 size-cover`}>
                  <EW_BackgroundImage source={HOME_PAGE_DATA.SEVENTH_SECTION.BANNER}/>
                </div>
                <div className={`container text-center ew-color-white`}>
                  <div
                    className={`ew-block-height display-flex align-center ew-hidden-left transition-duration-10 transition-delay-6`}>
                    <b
                      className={`text-uppercase font-size-12 letter-spacing-70 ew-color-white`}>{HOME_PAGE_DATA.SEVENTH_SECTION.SECTION_LABEL}</b>
                  </div>
                  <div
                    className={`${EW_PRIMARY_BANNER} ew-hidden-right transition-duration-10 transition-delay-9`}>
                    <b>{HOME_PAGE_DATA.SEVENTH_SECTION.HEADER}</b>
                  </div>
                  <div className={`margin-top-50 pos-rel ew-hidden-left transition-duration-10 transition-delay-12`}>
                    <EW_Button onClick={(e) => this.scrollToForm()} children={
                      EW_ButtonModel.buildRoundButton(HOME_PAGE_DATA.SEVENTH_SECTION.BUTTON_LABEL)
                    }/>
                  </div>
                </div>
              </section>
            }/>
      
            <EW_AnimatedSection isActive={isActive} source={HOME_PAGE_DATA.EIGHTH_SECTION} children={
              <section className={`pos-rel ew-bg-grey-light padding-v-100`}>
                <div className={`hidden-md-down full-height pos-abs top-0 right-0 half-width padding-h-100`}>
                  <div className={`full-height pos-rel ew-hidden-bottom transition-duration-10 transition-delay-3`}>
                    <EW_BackgroundImage className={`size-contain`} source={HOME_PAGE_DATA.EIGHTH_SECTION.BANNER}/>
                  </div>
                </div>
                <div className={`container`}>
                  <div className={`row`}>
                    <div className={`col-12 col-lg-6`}>
                      <div
                        className={`ew-block-height display-flex ${EW_SECONDARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-6`}>
                        <b>{HOME_PAGE_DATA.EIGHTH_SECTION.HEADER}</b>
                      </div>
                      <div
                        className={`${EW_PRIMARY_TEXT} margin-top-40 padding-right-100 ew-hidden-top transition-duration-10 transition-delay-9`}>
                        <span>{HOME_PAGE_DATA.EIGHTH_SECTION.TEXT}</span>
                      </div>
                      <div className={`margin-top-40 ew-hidden-bottom transition-duration-10 transition-delay-12`}>
                        <EW_Button onClick={(e) => this.scrollToForm()} children={
                          EW_ButtonModel.buildSquareButton(HOME_PAGE_DATA.EIGHTH_SECTION.BUTTON_LABEL)
                        }/>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            }/>
      
            <EW_AnimatedSection isActive={isActive} children={
              <section className={`${CONTACT_FORM} ew-section pos-rel ew-bg-white margin-top-100 padding-bottom-100`}>
                <div className={`pos-abs size-cover`}>
                  <EW_BackgroundImage className={'size-contain'} source={DEFAULT_BG} />
                </div>
                <div className={`container`}>
                  <Suspense fallback={EMPTY_STRING}>
                    <EW_Form model={form}>
                      <div className={`text-center`}>
                        <div
                          className={`ew-block-height display-flex align-center ew-hidden-top transition-duration-10 transition-delay-3`}>
                          <b className={`${EW_SECTION_HEADER}`}>{HOME_PAGE_DATA.NINTH_SECTION.SECTION_LABEL}</b>
                        </div>
                        <div className={`${EW_SECONDARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-6 margin-bottom-50`}>
                          <b>{HOME_PAGE_DATA.NINTH_SECTION.HEADER}</b>
                        </div>
                      </div>
                    </EW_Form>
                  </Suspense>
                </div>
              </section>
            }/>
          </article>
      }
      <Suspense fallback={EMPTY_STRING}>
        <EW_Modal className={`${MODAL_FORM_CONTENT} ew-bg-white padding-50 margin-top-50`}
                  showCloseButton={true}
                  isOpened={modalForm.isOpened}>
          <Suspense fallback={EMPTY_STRING}><EW_Form model={modalForm.core} /></Suspense>
        </EW_Modal>
      </Suspense>
    </div>;
  }
}
