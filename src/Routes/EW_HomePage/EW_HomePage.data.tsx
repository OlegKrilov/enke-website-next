export const HOME_PAGE_DATA = {

  FIRST_SECTION: {
    SECTION_LABEL: 'Software Development Company',
    HEADER: 'Accelerated Software Engineering',
    TEXT: 'Keep up with the speed at which business moves getting then most out of technologies that define the future',
    BUTTON_LABEL: 'Get in touch',
    BANNER: 'Photos/Home/banner-A.png'
  },

  SECOND_SECTION: {
    SECTION_LABEL: 'How we do it',
    HEADER: 'Engineering the future with these technologies',
    DESCRIPTION: 'At Enke, we modernize legacy systems, develop custom software, automate business processes and design intelligent systems UI and machine learning',
    CATEGORIES: [
      {
        LABEL: 'Frontend Developer',
        ITEMS: {
          'Javascript': 'javascript',
          'React': 'react',
          'Angular': 'angular',
          'React Native': 'react_native',
          'D3': 'd3'
        }
      },
      {
        LABEL: 'Backend Developer',
        ITEMS: {
          'Java': 'java',
          'Python': 'python',
          '.NET': 'ms_dot_net',
          'Objective C': 'objective_c',
          'Node JS': 'node_js'
        }
      },
      {
        LABEL: 'Databases',
        ITEMS: {
          'Redshift': 'amazon_redshift',
          'Postgre SQL': 'postgre_sql',
          'SQL Server': 'sql_server',
          'Cassandra': 'cassandra',
          'Dynamo DB': 'dynamo_db',
          'Mongo DB': 'mongo'
        }
      },
      {
        LABEL: 'Infrastructure',
        ITEMS: {
          'Hadoop': 'hadoop',
          'Spark': 'apache_spark',
          'Kafka': 'kafka',
          'Tensor Flow': 'tensorflow',
          'Office 365': 'office_365',
          'SharePoint': 'sharepoint',
          'Power Apps': 'power_apps',
          'Sail Point': 'sail_point',
          'ServiceNow': 'service_now',
          'Sales Force': 'sales_force',
          'Bizagi': 'bizagi',
          'AWS': 'aws',
          'Azure': 'azure',
          'Google Cloud': 'google_cloud'
        }
      }
    ]
  },

  THIRD_SECTION: {
    SECTION_LABEL: 'Who we are',
    HEADER: 'We\'re building technology solutions to speed up our clients\' growth',
    TEXT: 'We are helping organizations in different industries apply technologies to evolve faster. See how our clients transform, modernize and accelerate their business using our expertise to meet technical challenges.',
    BANNER: 'Photos/Home/banner-B.png',
    ICON: 'ew-rocket'
  },

  FOURTH_SECTION: {
    SECTION_LABEL: 'More possibilities',
    HEADER: 'Accelerating the mortgage loan process',
    TEXT: 'We help a banking organization expand the capabilities of LanIQ, the world\'s leading solution for servicing commercial loans by developing a custom Payoff Request module on top of the LoanIQ\'s SDK',
    BANNER_LEFT: 'Photos/Home/banner-CA.png',
    BANNER_RIGHT: 'Photos/Home/banner-CB.png',
    BUTTON_LABEL: 'Request a case study',
    ICON: 'ew-building'
  },

  FIFTH_SECTION: { 
    SECTION_LABEL: 'You can be faster',
    HEADER: 'Are you fast enough?',
    TEXT: 'We live in an era of unpredictability. Unlike organizations of the past who were organized primarily for efficiency, today\'s companies must also be fast, agile and adaptable. To complete in the business environment you need talented people and innovative technologies. So how do you remake tour organization into a next-generation enterprise?',
    BUTTON_LABEL: 'Learn more',
    BANNER: 'Photos/Home/banner-D.png'
  },

  SIXTH_SECTION: {
    SECTION_LABEL: 'Our offers',
    HEADER: 'We live Big Movement. Make a change take hold.',
    TEXT: 'Technology impacts how we live, work and play. As people around us are adaptting to the digital world, so does business. We work alongside startups and enterprises to change products, services, and organizations fro the better.',
    TABS: [
      {
        HEADER: 'Upgrade legacy systems, become more agile',
        TEXT: 'See how we help enterprise organizations innovate using data, AI, custom systems, automation, and our software engineering services.',
        ICON: 'ew-uploaded'
      },
      {
        HEADER: 'Adopt new technology to be at the top of the game',
        TEXT: 'Automation, security, agility, analytics - see how technologies and our engineering capabilities can help achieve these',
        ICON: 'ew-lamp',
        BACKGROUND: 'Photos/Home/banner-E.png'
      },
      {
        HEADER: 'Transform industries with a disruptive business model',
        TEXT: 'We help you link technology with an overlooked market need. See how we connect emerging technologies to what the marketplace wants',
        ICON: 'ew-sort'
      }
    ]
  },

  SEVENTH_SECTION: {
    SECTION_LABEL: 'Become our partner',
    HEADER: 'We partner with forward-thinking organizations',
    BUTTON_LABEL: 'Get in touch',
    BANNER: 'Photos/Home/banner-G.png'
  },

  EIGHTH_SECTION: {
    HEADER: 'Move fast with Enke Systems',
    TEXT: 'Help your enterprise move forward by leveraging our industry-specific expertise, efficient software delivery approach and the world\'s most talented engineers',
    BUTTON_LABEL: 'Are you ready?',
    BANNER: 'Photos/Home/banner-H.png'
  },

  NINTH_SECTION: {
    SECTION_LABEL: 'We can help you',
    HEADER: 'Contact us',
  }

};

