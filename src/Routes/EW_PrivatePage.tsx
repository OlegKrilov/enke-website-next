import React from "react";
import {AbstractComponent} from "../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import routingService, {RoutingService} from "../Services/Routing.service";
import sharedService, {SharedService} from "../Services/Shared.service";
import userService, {UserService} from "../Services/User.service";
import accessPointService, {AccessPointService} from "../Services/AccessPoint.service";
import {AWAIT, PIPE} from "../Core/Helpers/Helpers.misc";
import {BooleanObservable} from "../Core/Observables/BooleanObservable";
import {AbstractObserver} from "../Core/Abstract/AbstractObserver";

@observer
export class EW_PrivatePage extends AbstractComponent {

  protected readonly isActive: BooleanObservable = new BooleanObservable();

  protected readonly routingService: RoutingService;

  protected readonly sharedService: SharedService;

  protected readonly userService: UserService;

  protected readonly accessPointService: AccessPointService;

  protected isAuthorized: AbstractObserver;

  componentDidMount(): void {
    const
      {sharedService} = this;

    PIPE(
      () => AWAIT(500)
    )
      .finally(() => sharedService.isLoading.setValue(false));
  }

  render () {
    return <div />;
  }

  constructor (props) {
    super(props);
    this.routingService = routingService;
    this.accessPointService = accessPointService;
    this.sharedService = sharedService;
    this.userService = userService;
    this.isAuthorized = new AbstractObserver(
      () => userService.isAuthorized
    );
  }
}

