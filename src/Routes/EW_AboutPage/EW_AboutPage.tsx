import React, {lazy, Suspense} from "react";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {
  EW_DECORATION_BLOCK_BLUE, EW_DECORATION_BLOCK_WHITE,
  EW_PRIMARY_BANNER,
  EW_PRIMARY_TEXT,
  EW_SECONDARY_BANNER, EW_SECONDARY_TEXT, EW_SECTION_HEADER
} from "../../Common/Constants/EW_ViewClasses.cnst";
import {EW_Icon} from "../../Components/EW_Icon/EW_Icon";
import {BODY, CLASSNAME, DATA, HEADER} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EW_AnimatedSection} from "../../Components/EW_AnimatedSection/EW_AnimatedSection";
import {inject, observer} from "mobx-react";
import {EW_PublicPage} from "../EW_PublicPage";
import {CONTACTS_FORM, EW_FormModel} from "../../Components/EW_Form/EW_Form.model";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {POST} from "../../Common/Constants/EW_Routes.cnst";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";

const
  EW_Form = lazy(() => import('../../Components/EW_Form/EW_Form'));

const
  ROOT = `ew-about-page`,
  FIRST_SECTION = `${ROOT}-first-section`, 
  SECOND_SECTION = `${ROOT}-second-section`,
  SECOND_SECTION_ITEM = `${SECOND_SECTION}-item`,
  SECOND_SECTION_ITEM_ICON = `${SECOND_SECTION_ITEM}-icon`,
  SECOND_SECTION_ITEM_HEADER = `${SECOND_SECTION_ITEM}-header`,
  SECOND_SECTION_ITEM_TEXT = `${SECOND_SECTION_ITEM}-text`,
  THIRD_SECTION = `${ROOT}-third-section`,
  FIFTH_SECTION = `${ROOT}-fifth-section`,
  FIFTH_SECTION_ITEM = `${FIFTH_SECTION}-item`;

const
  DEFAULT_BG = 'Photos/Common/bg.png';

@inject('sharedService')
@observer
export default class EW_AboutPage extends EW_PublicPage {
  
  private readonly form: EW_FormModel = new EW_FormModel(CONTACTS_FORM);
  
  private getSecondSectionItems = (items: any[]) => items.map((d, i) =>
    <div className={`${SECOND_SECTION_ITEM} ew-block-width-3 padding-h-50 display-inline-block flex-direction-column align-center ew-hidden-top transition-duration-10 transition-delay-${6 + (i * 2)}`} key={i}>
      <div className={`${SECOND_SECTION_ITEM_ICON} padding-top-50`}>
        <EW_Icon source={d.ICON} />
      </div>
      <div className={`${SECOND_SECTION_ITEM_HEADER} font-size-18 padding-v-30`}>
        <b>{d.HEADER}</b>
      </div>
      <div className={`${SECOND_SECTION_ITEM_TEXT} font-size-15`}>
        <span>{d.TEXT}</span>
      </div>
    </div>
  );

  private getFifthSectionItems = (items: any[]) => items.map((d, i) => {
    let isLastBlock = i === items.length - 1;
    return <div className={`${d.CLASSNAME} padding-v-50`} key={i}>
      <div className={`container ew-hidden-left transition-duration-10 transition-delay-${3 + (i * 3)}`}>
        <div className={`row`} key={i}>
          <div className={`${FIFTH_SECTION_ITEM} ${d.CLASSNAME} col-12 col-xl-${isLastBlock ? '9' : '6'} col-lg-${isLastBlock ? '9' : '6'} `}>
            <div className={`${EW_SECONDARY_BANNER}`}>
              <b>{d.HEADER}</b>
            </div>
            <div className={`${EW_SECONDARY_TEXT} margin-top-40 padding-right-100`}>
              <span>{d.TEXT}</span>
            </div>
          </div>
        </div>
      </div>
    </div>;
  });
  
  private submitForm = data => {
    const
      {accessPointService, form} = this;
  
    const
      requestPipe = new AbstractPipe([
        () => form.isSubmitted.setValue(true),
        () => accessPointService.sendForm(Object.assign({theme: 'general'}, data), POST.CREATE_GENERAL_MESSAGE)
      ]);
  
    this
      .registerPipes(requestPipe)
      .registerSubscriptions(
        requestPipe.then(res => form.successMessage.setValue(res.statusText)),
        requestPipe.catch(err => form.errorMessage.setValue(err.statusText)),
        requestPipe.finally(() => form.isSubmitted.setValue(false))
      );
  };
  
  componentDidMount(): void {
    super.componentDidMount();
    const {form} = this;
    
    this.registerSubscriptions(
      form.onSubmit.getSubscription(
        e => this.submitForm(e[DATA])
      )
    );
  }

  render () {
    const
      {isActive, pageData, form} = this,
      ABOUT_PAGE_DATA = pageData.value;

    return pageData.isEmpty ?
      <h3 className={`opacity-3 padding-100`}>IS LOADING...</h3> :
      <article className={`${ROOT} margin-bottom-100`}>

        <EW_AnimatedSection isActive={isActive} source={ABOUT_PAGE_DATA.FIRST_SECTION} children={
          <section className={`${FIRST_SECTION} pos-rel padding-v-100 ew-bg-white`}>
            <div className={`full-height hidden-md-down half-width pos-abs top-0 right-0`}>
              {this.sharedService.isWideScreen && <div className={`pos-rel full-height ew-hidden-top transition-duration-10 transition-delay-3`}>
                <EW_BackgroundImage source={ABOUT_PAGE_DATA.FIRST_SECTION.BANNER}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} top-0 left-0 opacity-8`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} top-100 left-0 opacity-4`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} top-0 left-100 opacity-4`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 right-0 opacity-8`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-100 right-0 opacity-4`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 right-100 opacity-4`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-100 right-100 opacity-4`}/>
              </div>}
            </div>

            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-12 col-lg-6 col-xl-6`}>
                  <div className={`padding-right-100 padding-bottom-100`}>
                    <div className={`${EW_PRIMARY_BANNER}  ew-hidden-bottom transition-duration-10 transition-delay-6`}>
                      <b>{ABOUT_PAGE_DATA.FIRST_SECTION.HEADER}</b>
                    </div>
                    <div className={`${EW_PRIMARY_TEXT} margin-top-40  ew-hidden-top transition-duration-10 transition-delay-9`}>
                      <span>{ABOUT_PAGE_DATA.FIRST_SECTION.TEXT}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        }/>

        <EW_AnimatedSection isActive={isActive} source={ABOUT_PAGE_DATA.SECOND_SECTION} children={
          <section className={`${SECOND_SECTION} ew-section ew-bg-grey-light padding-v-100`}>
            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-12`}>
                  <div className={`${EW_SECONDARY_BANNER} ew-block-height display-flex align-center padding-h-100  ew-hidden-left transition-duration-10 transition-delay-3`}>
                    <b>{ABOUT_PAGE_DATA.SECOND_SECTION.HEADER}</b>
                  </div>
                  <div className={`${EW_SECONDARY_TEXT} text-center padding-h-100 ew-hidden-right transition-duration-10 transition-delay-6`}>
                    <span>{ABOUT_PAGE_DATA.SECOND_SECTION.TEXT}</span>
                  </div>
                </div>
                <div className={`col-12`}>
                  <div className={`full-width text-center`}>{this.getSecondSectionItems(ABOUT_PAGE_DATA.SECOND_SECTION.ITEMS)}</div>
                </div>
              </div>
            </div>
          </section>
        }/>

        <EW_AnimatedSection isActive={isActive} source={ABOUT_PAGE_DATA.THIRD_SECTION} children={
          <section className={`${THIRD_SECTION} ew-section pos-rel`}>

            <div className={`full-width pos-abs top-0 left-0 ew-bg-grey-light ew-block-height-0-and-half`}/>
            <div className={`full-width pos-abs bottom-0 left-0 ew-bg-grey-light ew-block-height-0-and-half`}/>
            <div className={`ew-section-background hidden-md-down half-width pos-abs top-0 right-0`}>
              {this.sharedService.isWideScreen && <div className={`pos-rel full-height ew-hidden-right transition-duration-10 transition-delay-3`}>
                <EW_BackgroundImage source={ABOUT_PAGE_DATA.THIRD_SECTION.BANNER}/>
              </div>}
            </div>

            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-12 col-lg-6 col-xl-6`}>
                  <div className={`${EW_SECONDARY_BANNER} padding-top-100 ew-hidden-left transition-duration-10 transition-delay-6`}>
                    <b>{ABOUT_PAGE_DATA.THIRD_SECTION.HEADER}</b>
                  </div>
                  <div className={`${EW_SECONDARY_TEXT} margin-top-40 padding-right-100 ew-hidden-left transition-duration-10 transition-delay-9`}>
                    <span>{ABOUT_PAGE_DATA.THIRD_SECTION.TEXT}</span>
                  </div>
                </div>
              </div>
            </div>
          </section>
        }/>

        <EW_AnimatedSection isActive={isActive} source={ABOUT_PAGE_DATA.FOURTH_SECTION} children={
          <section className={`${FIRST_SECTION} ew-section pos-rel margin-v-100 ew-bg-white`}>

            <div className={`ew-bg-grey-light pos-abs top-neg-100 left-0 ew-block-height full-width`}/>
            <div className={`ew-bg-grey-light pos-abs top-0 left-0 ew-block-height full-width`}/>
            <div className={`hidden-md-down full-height half-width pos-abs top-0 left-0`}>
              {this.sharedService.isWideScreen && <div className={`pos-rel full-height ew-hidden-left transition-duration-10 transition-delay-3`}>
                <EW_BackgroundImage source={ABOUT_PAGE_DATA.FOURTH_SECTION.BANNER}/>
                <div className={`${EW_DECORATION_BLOCK_WHITE} ew-bg-grey-light full-height top-0 left-0`}/>
                <div className={`${EW_DECORATION_BLOCK_WHITE} ew-bg-grey-light top-100 left-0 opacity-4`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 left-0 opacity-8`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-100 left-0 opacity-6`}/>
                <div className={`${EW_DECORATION_BLOCK_BLUE} bottom-200 left-0 opacity-4`}/>
                <div className={`${EW_DECORATION_BLOCK_WHITE} ew-bg-grey-light top-0 right-0`}/>
                <div className={`${EW_DECORATION_BLOCK_WHITE} ew-bg-grey-light top-0 right-100 opacity-6`}/>
                <div className={`${EW_DECORATION_BLOCK_WHITE} top-100 right-0 opacity-8`}/>
              </div>}
            </div>

            <div className={`container`}>
              <div className={`row`}>
                <div className={`hidden-md-down col-lg-6 col-xl-6`}/>
                <div className={`col-12 col-lg-6 col-xl-6 padding-left-100`}>
                  <div className={`${EW_SECONDARY_BANNER} ew-block-height display-flex ew-hidden-top transition-duration-10 transition-delay-6`}>
                    <b>{ABOUT_PAGE_DATA.FOURTH_SECTION.HEADER}</b>
                  </div>
                  <div className={`${EW_SECONDARY_TEXT} margin-top-40 padding-right-100 ew-hidden-bottom transition-duration-10 transition-delay-9`}>
                    <span>{ABOUT_PAGE_DATA.FOURTH_SECTION.TEXT}</span>
                  </div>
                </div>
              </div>
            </div>
          </section>
        }/>

        <EW_AnimatedSection isActive={isActive} source={ABOUT_PAGE_DATA.FIFTH_SECTION} children={
          <section className={`ew-section pos-rel padding-bottom-100`}>
            {this.getFifthSectionItems(ABOUT_PAGE_DATA.FIFTH_SECTION.ITEMS)}
            {<div className={`pos-abs top-0 left-0 full-width full-height hidden-md-down`}>
              <div className={`container full-height`}>
                <div className={`row  full-height`}>
                  <div className={`col-6`}/>
                  <div className={`col-6 padding-left-0 full-height`}>
                    <div className={`row`}>
                      <div className={`col-12 padding-10`}>
                        <div className={`ew-block-height-5 pos-rel ew-hidden-bottom transition-duration-10 transition-delay-3`}>
                          <EW_BackgroundImage source={ABOUT_PAGE_DATA.FIFTH_SECTION.BANNERS[0]}/>
                          <div className={`${EW_DECORATION_BLOCK_WHITE} top-0 right-0`}/>
                          <div className={`${EW_DECORATION_BLOCK_WHITE} top-0 right-100 opacity-8`}/>
                          <div className={`${EW_DECORATION_BLOCK_WHITE} top-100 right-0 opacity-8`}/>
                        </div>
                      </div>
                      <div className={`col-7 padding-10`}>
                        <div className={`ew-block-height-5 ew-hidden-bottom transition-duration-10 transition-delay-6`}>
                          <EW_BackgroundImage source={ABOUT_PAGE_DATA.FIFTH_SECTION.BANNERS[1]}/>
                        </div>
                      </div>
                      <div className={`col-5 padding-10`}>
                        <div className={`ew-block-height-3 margin-bottom-20 ew-hidden-bottom transition-duration-10 transition-delay-12`}>
                          <EW_BackgroundImage source={ABOUT_PAGE_DATA.FIFTH_SECTION.BANNERS[2]}/>
                        </div>
                        <div className={`ew-block-height-5 ew-hidden-bottom transition-duration-10 transition-delay-18`}>
                          <EW_BackgroundImage source={ABOUT_PAGE_DATA.FIFTH_SECTION.BANNERS[3]}/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>}

          </section>
        }/>

        <EW_AnimatedSection isActive={isActive} source={ABOUT_PAGE_DATA.SIXTH_SECTION} children={
          <section className={`ew-section pos-rel ew-bg-white margin-top-100 padding-bottom-100`}>
            <div className={`pos-abs size-cover`}>
              <EW_BackgroundImage className={'size-contain'} source={DEFAULT_BG} />
            </div>
            <div className={`container`}>
              <Suspense fallback={EMPTY_STRING}>
                <EW_Form model={form}>
                  <div className={`text-center`}>
                    <div
                      className={`ew-block-height display-flex align-center ew-hidden-top transition-duration-10 transition-delay-3`}>
                      <b className={`${EW_SECTION_HEADER}`}>{ABOUT_PAGE_DATA.SIXTH_SECTION.HEADER}</b>
                    </div>
                    <div className={`${EW_SECONDARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-6 margin-bottom-50`}>
                      <b>{ABOUT_PAGE_DATA.SIXTH_SECTION.TEXT}</b>
                    </div>
                  </div>
                </EW_Form>
              </Suspense>
            </div>
          </section>
        }/>
      </article>;
  }
}

