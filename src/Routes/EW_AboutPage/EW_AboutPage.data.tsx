import {BODY, CLASSNAME, HEADER} from "../../Core/Constants/PropertiesAndAttributes.cnst";

const IMG_SOURCE_FOLDER = '/Photos/About';

export const ABOUT_PAGE_DATA = {

  FIRST_SECTION: { 
    HEADER: 'About us',
    TEXT: 'What is Enke Systems as a company? A combination of culture, people, processes and platforms. Learn more about how we bring all these pieces together',
    BANNER: `Photos/About/banner-A.png`
  },

  SECOND_SECTION: { 
    HEADER: 'Company',
    TEXT: 'Enke Systems is a company with a clear vision. We are building a business that pushes industries forward. To get there, we\'re growing a world-class engineering team and investing in new technologies.',
    ITEMS: [
      {
        HEADER: 'Culture',
        TEXT: 'Our culture is about effective teamwork to achieve great results and encourage innovation.',
        ICON: 'ew-handshake'
      },

      {
        HEADER: 'Processes',
        TEXT: 'We adapt to our clients\' existing processes to make changes feel less stressful for their organizations.',
        ICON: 'ew-process'
      },

      {
        HEADER: 'Expertise',
        TEXT: 'We specialize in enterprise automation and have a strong focus on big data and machine learning.',
        ICON: 'ew-brain'
      },

      {
        HEADER: 'People',
        TEXT: 'We hire talented people who do great work fast and demonstrate passion discipline and commitment.',
        ICON: 'ew-people'
      },

      {
        HEADER: 'Platforms',
        TEXT: 'Platforms are ready-made solutions that allow our clients to develop their products faster and cheaper.',
        ICON: 'ew-window'
      }
    ]
  },

  THIRD_SECTION: {
    HEADER: 'Company',
    TEXT: 'Enke Systems was founded in 2015 and quickly entered the software engineering market. Our ambition is to build software faster, cheaper and easier than others because this is what the world needs today. Our clients are large and small organizations operating on every continent of the planet. We accelerate software engineering with modern technologies, well-tuned processes and talented people.',
    BANNER: `${IMG_SOURCE_FOLDER}/banner-B.png`
  },

  FOURTH_SECTION: {
    HEADER: 'Culture',
    TEXT: 'Our culture is built on values that create effective teamwork needed to achieve great results, and encourage innovation. We prioritize employee autonomy, and open doors to people with a strong intrinsic motivation to be great at their job. It doesn\'t matter where we come from as long as we all share the same values inside our company. These values are honestly, self-discipline, high performance and commitment.',
    BANNER: `${IMG_SOURCE_FOLDER}/banner-C.png`
  },

  FIFTH_SECTION: {
    TEXT_BLOCKS: [
      {
        [HEADER]: 'Process',
        [BODY]: 'Our clients\' organizations have their own processes that govern things get done. Adopting new technologies and processes is a change full of complexities.',
        [CLASSNAME]: 'ew-bg-white'
      },
      {
        [HEADER]: 'Expertise',
        [BODY]: 'As a software engineering company we don\'t just bring technical capabilities to the table, but also business knowledge. We\'ve accelerated and transformed many enterprises in the media, finance, and utility industries. Our technical expertise lies within the fields of custom software enterprise solutions, analytics and Big Data, AI and machine learning. We are strong in DevOps and QA automation',
        [CLASSNAME]: 'ew-bg-grey-light'
      },
      {
        [HEADER]: 'People',
        [BODY]: 'People who work on Enke Systems are high performing individuals with a passion fir their job. We simulate their desire for mastery by giving them autonomy: our employees have control over what they don and how they do it. We hire people from all over the world to find unique talents. Such a geographically dispersed team also allows us to work around the clock so production never stops.',
        [CLASSNAME]: 'ew-bg-grey-light'
      },
      {
        [HEADER]: 'Platforms',
        [BODY]: 'Over a few years of running our business, we achieved solid growth. This is a good time for our economy, so we started investing in ready-made technology solutions. Platforms are technical frameworks that serve as the foundation fro building multiple products. They allow our clients to get software faster and cheaper than if thy built it stand-alone, and differentiate us from other software engineering companies.',
        [CLASSNAME]: 'ew-bg-white'
      }
    ],
    BANNERS: 'DEFG'.split('').map(n => `${IMG_SOURCE_FOLDER}/banner-${n}.png`)
  },

  SIXTH_SECTION: {
    HEADER: 'Want to work with us?',
    TEXT: 'Send us some details about your project and we will get back to you within one working day.'
  }
};
