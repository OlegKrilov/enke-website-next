import React, {Suspense, lazy} from "react";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {observer, inject} from "mobx-react";
import {EW_Icon} from "../../Components/EW_Icon/EW_Icon";
import {
  EW_SPECIAL_BLOCK_GRAY,
  EW_SPECIAL_BLOCK_WHITE,
  EW_DECORATION_BLOCK_BLUE,
  EW_DECORATION_BLOCK_WHITE,
  EW_PRIMARY_BANNER,
  EW_PRIMARY_TEXT, EW_SECONDARY_BANNER, EW_SECONDARY_TEXT,
  EW_SECTION_HEADER
} from "../../Common/Constants/EW_ViewClasses.cnst";
import {
  BODY,
  CLASSNAME,
  DATA,
  HEADER,
  MODEL,
  SOURCE,
  TOP,
  TYPE
} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EW_AnimatedSection} from "../../Components/EW_AnimatedSection/EW_AnimatedSection";
import {DIG_OUT, PIPE} from "../../Core/Helpers/Helpers.misc";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";
import {EW_PublicPage} from "../EW_PublicPage";
import {CV_FORM, EW_FormModel} from "../../Components/EW_Form/EW_Form.model";
import {EW_FileInputModel} from "../../Components/EW_FileInput/EW_FileInput.model";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {POST} from "../../Common/Constants/EW_Routes.cnst";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";

const
  EW_Form = lazy(() => import('../../Components/EW_Form/EW_Form'));

const
  ROOT = `ew-people-page`,
  CV_FORM_SECTION = `${ROOT}-cv-form`;

const
  DEFAULT_BG = 'Photos/Common/bg.png';

@inject('sharedService')
@observer
export default class EW_PeoplePage extends EW_PublicPage {
  
  private readonly form: EW_FormModel = new EW_FormModel(CV_FORM);
  
  private scrollToForm = () => {
    const
      formEl = document.querySelector(`.${CV_FORM_SECTION}`),
      scrollTarget = DIG_OUT((formEl as HTMLDivElement).getBoundingClientRect(), TOP);
    
    window.scroll({
      top: window.scrollY + scrollTarget,
      left: 0,
      behavior: 'smooth'
    });
  };
  
  private submitForm = (data) => {
    const
      {accessPointService, form} = this;
  
    const
      requestPipe = new AbstractPipe([
        () => form.isSubmitted.setValue(true),
        () => accessPointService.sendForm(Object.assign({theme: 'lookingForJob'}, data), POST.CREATE_JOB_SEEKING_MESSAGE)
      ]);
  
    this
      .registerPipes(requestPipe)
      .registerSubscriptions(
        requestPipe.then(res => form.successMessage.setValue(res.statusText)),
        requestPipe.catch(err => form.errorMessage.setValue(err.statusText)),
        requestPipe.finally(() => form.isSubmitted.setValue(false))
      );
  };
  
  private firstTabs = PEOPLE_PAGE_DATA => PEOPLE_PAGE_DATA.THIRD_SECTION.ITEMS.map((d, i) =>
    <div
      className={`col-12 col-lg-6 col-xl-6 margin-v-10 ew-hidden-left transition-duration-10 transition-delay-${6 + (2 * i)}`}
      key={i}>
      <div className={`ew-color-black full-height padding-10 padding-right-10`}>
        <div className={`display-flex align-left font-size-18 margin-bottom-20 margin-left-30`}>
          <EW_Icon source={d.ICON}/>
          <b className={`margin-left-10`}>{d.HEADER}</b>
        </div>
        <div className={`${EW_SECONDARY_TEXT} opacity-8 margin-left-30 `}>
          <span>{d.TEXT}</span>
        </div>
      </div>
    </div>
  );
  
  private secondTabs = PEOPLE_PAGE_DATA => PEOPLE_PAGE_DATA.FOURTH_SECTION.ITEMS.map((d, i) =>
    <div
      className={`col-12 col-lg-6 col-xl-6 margin-v-10 ew-hidden-right transition-duration-10 transition-delay-${6 + (2 * i)}`}
      key={i}>
      <div className={`ew-color-black full-height padding-10 padding-right-10`}>
        <div className={`display-flex align-left font-size-18 margin-bottom-20 margin-left-30`}>
          <EW_Icon source={d.ICON}/>
          <b className={`margin-left-10`}>{d.HEADER}</b>
        </div>
        <div className={`${EW_SECONDARY_TEXT} opacity-8 margin-left-30 `}>
          <span>{d.TEXT}</span>
        </div>
      </div>
    </div>
  );
  
  private movingForwardSectionItems = PEOPLE_PAGE_DATA => PEOPLE_PAGE_DATA.FIFTH_SECTION.ITEMS.map((d, i) =>
    <div
      className={`padding-0 col-6 col-lg-2 col-xl-2 ew-block-height-2 ew-hidden-right transition-duration-10 transition-delay-${6 + (2 * i)} ${d['CLASSNAME']}`}
      key={i}>
      {d['TYPE'] === 'TEXT' && <div className={'padding-20'}>
				<b className={`padding-bottom-10`}>{d['HEADER']}</b>
				<br/>
				<span>{d['TEXT']}</span>
			</div>}
      {d['TYPE'] === 'IMAGE' && <EW_BackgroundImage source={d['SOURCE']}/>}
      {d['TYPE'] === 'EMPTY' && <div/>}
    </div>
  );
  
  private movingForwardSectionHeader = PEOPLE_PAGE_DATA => <div
    className={`display-flex flex-direction-column ew-block-height-2`}>
    <div className={`${EW_SECONDARY_BANNER}`}>
      <b>{PEOPLE_PAGE_DATA.FIFTH_SECTION.HEADER}</b>
    </div>
    <div className={`${EW_SECONDARY_TEXT} margin-top-40`}>
      <span>{PEOPLE_PAGE_DATA.FIFTH_SECTION.TEXT}</span>
    </div>
  </div>;
  
  componentDidMount(): void {
    super.componentDidMount();
    
    const
      {form, sharedService} = this,
      {growl, isLoading} = sharedService,
      fileInput: EW_FileInputModel = DIG_OUT(form.fields.getItem('file'), MODEL);
    
    this.registerSubscriptions(
      fileInput && fileInput.onError.getSubscription(
      e => {
        growl.addMessage(EW_GrowlModel.buildErrorMessage(e[DATA])).show();
        fileInput.clearValue();
      }),
      
      form.onSubmit.getSubscription(
        e => this.submitForm(e[DATA])
      )
    );
  }
  
  render() {
    const
      {isActive, pageData, form} = this,
      PEOPLE_PAGE_DATA = pageData.value;
    
    
    return pageData.isEmpty ?
      <h3 className={`opacity-3 padding-100`}>IS LOADING...</h3> :
      
      <article className={`${ROOT}`}>
        
        <EW_AnimatedSection isActive={this.isActive} source={PEOPLE_PAGE_DATA.FIRST_SECTION} children={
          <section className={` pos-rel padding-bottom-100`}>
            {this.sharedService.isWideScreen &&
						<div className={`full-height hidden-md-down half-width pos-abs top-0 right-0`}>
							<div className={`full-height pos-rel ew-hidden-right transition-duration-10 transition-delay-3`}>
								<EW_BackgroundImage source={PEOPLE_PAGE_DATA.FIRST_SECTION.BANNER}/>
								<div className={`${EW_DECORATION_BLOCK_BLUE} top-0 left-0 opacity-8`}/>
								<div className={`${EW_DECORATION_BLOCK_BLUE} top-100 left-0 opacity-4`}/>
								<div className={`${EW_DECORATION_BLOCK_BLUE} top-0 left-100 opacity-4`}/>
								<div className={`${EW_DECORATION_BLOCK_BLUE} top-100 left-100 opacity-4`}/>
							</div>
						</div>}
            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-12 col-lg-6 col-xl-6 ew-hidden-left transition-duration-10 transition-delay-3`}>
                  <div className={`${EW_PRIMARY_BANNER} padding-top-100`}>
                    <b>{PEOPLE_PAGE_DATA.FIRST_SECTION.HEADER}</b>
                  </div>
                  <div
                    className={`${EW_PRIMARY_TEXT} margin-top-40 padding-right-100 ew-hidden-left transition-duration-10 transition-delay-7`}>
                    <span>{PEOPLE_PAGE_DATA.FIRST_SECTION.TEXT}</span>
                  </div>
                </div>
              </div>
            </div>
          </section>
        }/>
        
        <EW_AnimatedSection isActive={this.isActive} source={PEOPLE_PAGE_DATA.SECOND_SECTION} children={
          <section className={` ew-bg-grey-light pos-rel padding-bottom-100`}>
            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-9 padding-right-0`}>
                  <div
                    className={`${EW_SECONDARY_BANNER} padding-top-100 ew-hidden-bottom transition-duration-10 transition-delay-3`}>
                    <b>{PEOPLE_PAGE_DATA.SECOND_SECTION.HEADER}</b>
                  </div>
                  <div
                    className={`${EW_SECONDARY_TEXT} margin-top-40 padding-right-100 ew-hidden-bottom transition-duration-10 transition-delay-3`}>
                    <span>{PEOPLE_PAGE_DATA.SECOND_SECTION.TEXT}</span>
                  </div>
                </div>
                {this.sharedService.isWideScreen && <div
									className={`full-height col-3 padding-right-0 hidden-md-down half-width pos-abs top-0 right-0`}>
									<div className={`full-height pos-rel  ew-hidden-right transition-duration-10 transition-delay-4`}>
										<EW_BackgroundImage source={PEOPLE_PAGE_DATA.SECOND_SECTION.BANNER}/>
										<div className={`${EW_DECORATION_BLOCK_BLUE} bottom-0 right-0 opacity-4`}/>
									</div>
								</div>}
              </div>
            </div>
          </section>
        }/>
        
        <EW_AnimatedSection isActive={this.isActive} source={PEOPLE_PAGE_DATA.THIRD_SECTION} children={
          <section className={`ew-section pos-rel ew-bg-grey-light padding-v-100`}>
            <div
              className={`${EW_DECORATION_BLOCK_BLUE} hidden-md-down top-0 right-0 opacity-4 ew-hidden-right transition-duration-10 transition-delay-4`}/>
            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-12 text-center margin-bottom-100`}/>
                {this.firstTabs(PEOPLE_PAGE_DATA)}
                {this.secondTabs(PEOPLE_PAGE_DATA)}
              </div>
            </div>
          </section>
        }/>
        
        <EW_AnimatedSection isActive={this.isActive} source={PEOPLE_PAGE_DATA.FIFTH_SECTION} children={
          <section className={`ew-section pos-rel margin-top-100`}>
            {this.sharedService.isWideScreen &&
						<div className={`hidden-md-down half-width full-height pos-abs top-0 right-neg-100`}>
							<div className={`pos-rel full-height ew-hidden-right transition-duration-10 transition-delay-3`}>
								<EW_BackgroundImage source={PEOPLE_PAGE_DATA.FIFTH_SECTION.BANNER}/>
							</div>
						</div>}
            <div className={`container pos-rel`}>
              <div
                className={`row hidden-lg-up ew-hidden-left transition-duration-10 transition-delay-3`}>{this.movingForwardSectionHeader(PEOPLE_PAGE_DATA)}</div>
              <div
                className={`pos-abs col-6 hidden-md-down top-0 left-0 ew-hidden-left transition-duration-10 transition-delay-5`}>{this.movingForwardSectionHeader(PEOPLE_PAGE_DATA)}</div>
              <div
                className={`row margin-0 ew-hidden-right transition-duration-10 transition-delay-7`}>{this.movingForwardSectionItems(PEOPLE_PAGE_DATA)}</div>
            </div>
          </section>
        }/>
        
        <EW_AnimatedSection isActive={this.isActive} source={PEOPLE_PAGE_DATA.SIXTH_SECTION} children={
          <section
            className={`margin-v-100 pos-rel padding-bottom-100 ew-hidden-left transition-duration-10 transition-delay-5`}>
            <div className={`${EW_DECORATION_BLOCK_BLUE} hidden-md-down top-neg-100 right-0 opacity-8`}/>
            <div className={`${EW_DECORATION_BLOCK_BLUE} hidden-md-down top-neg-0 right-0 opacity-4`}/>
            <div className={`${EW_DECORATION_BLOCK_BLUE} hidden-md-down top-neg-100 right-100 opacity-4`}/>
            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-lg-6 col-xl-6 hidden-md-down`}/>
                <div
                  className={`col-12 col-lg-6 col-xl-6 padding-left-100 ew-hidden-right transition-duration-10 transition-delay-4`}>
                  <div className={`${EW_SECONDARY_BANNER} padding-top-100`}>
                    <b>{PEOPLE_PAGE_DATA.SIXTH_SECTION.HEADER}</b>
                  </div>
                  <div
                    className={`${EW_PRIMARY_TEXT} margin-top-40 padding-right-100 ew-hidden-right transition-duration-10 transition-delay-6`}>
                    <span>{PEOPLE_PAGE_DATA.SIXTH_SECTION.TEXT}</span>
                  </div>
                </div>
              </div>
              
              <div>
                {this.sharedService.isWideScreen && <div
									className={`ew-people-page-first-section-bg full-height hidden-md-down half-width pos-abs top-0 left-0`}>
									<div className={`pos-rel full-height`}>
										<EW_BackgroundImage source={PEOPLE_PAGE_DATA.SIXTH_SECTION.BANNER}/>
										<div className={`${EW_DECORATION_BLOCK_WHITE} bottom-0 right-0 opacity-8`}/>
										<div className={`${EW_DECORATION_BLOCK_WHITE} bottom-100 right-0 opacity-4`}/>
										<div className={`${EW_DECORATION_BLOCK_WHITE} bottom-0 right-100 opacity-4`}/>
									</div>
								</div>}
              </div>
            </div>
          </section>
        }/>
        
        <EW_AnimatedSection isActive={this.isActive} source={PEOPLE_PAGE_DATA.SEVENTH_SECTION} children={
          <section className={`ew-section ew-bg-blue pos-rel display-flex padding-v-100`}>
            {this.sharedService.isWideScreen && <div className={`pos-abs top-0 left-0 size-cover`}>
							<EW_BackgroundImage source={PEOPLE_PAGE_DATA.SEVENTH_SECTION.BACKGROUND}/>
						</div>}
            <div className={`container text-center ew-color-white`}>
              <div
                className={`${EW_PRIMARY_BANNER} ew-hidden-right transition-duration-10 transition-delay-5`}>
                <b>{PEOPLE_PAGE_DATA.SEVENTH_SECTION.HEADER}</b>
              </div>
              <div
                className={`${EW_SECONDARY_TEXT} margin-top-30 ew-hidden-left transition-duration-10 transition-delay-7`}>
                <span>{PEOPLE_PAGE_DATA.SEVENTH_SECTION.TEXT}</span>
              </div>
              <div className={`margin-top-50 pos-rel ew-hidden-left transition-duration-10 transition-delay-9`}>
                <EW_Button onClick={() => this.scrollToForm()} children={
                  EW_ButtonModel.buildRoundButton(PEOPLE_PAGE_DATA.SEVENTH_SECTION.BUTTON_LABEL)
                }/>
              </div>
            </div>
          </section>
        }/>
        
        <EW_AnimatedSection isActive={this.isActive} children={
          <section className={`ew-section margin-top-100 padding-bottom-100 ${CV_FORM_SECTION}`}>
            <div className={`pos-abs size-cover`}>
              <EW_BackgroundImage className={'size-contain'} source={DEFAULT_BG}/>
            </div>
            <Suspense fallback={EMPTY_STRING}><EW_Form model={form}>
              <div className={`text-center`}>
                <div
                  className={`ew-block-height display-flex align-center ew-hidden-top transition-duration-10 transition-delay-3`}>
                  <b className={`${EW_SECTION_HEADER}`}>Tell us about yourself</b>
                </div>
                <div className={`${EW_SECONDARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-6 margin-bottom-50`}>
                  <b>Fill the above form and add your CV</b>
                </div>
              </div>
            </EW_Form></Suspense>
          </section>
        }/>
      
      </article>
  }
  
  constructor(props) {
    super(props);
    
  }
  
}

