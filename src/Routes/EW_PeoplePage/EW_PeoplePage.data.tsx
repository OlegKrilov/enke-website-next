import {
  EW_SECONDARY_TEXT,
  EW_SPECIAL_BLOCK_GRAY,
  EW_SPECIAL_BLOCK_WHITE
} from "../../Common/Constants/EW_ViewClasses.cnst";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import React from "react";
import {BODY, CLASSNAME, HEADER, SOURCE, TYPE} from "../../Core/Constants/PropertiesAndAttributes.cnst";



export const PEOPLE_PAGE_DATA = {
    PAGE_HEADER: {
        HEADER: 'People',
        TEXT: 'Great technology is built by talented people, regardless of where they come from. We work with the best best talents from around the globe. How do we make effective teamwork, get great results, and encourage innovation? Wise hiring decisions.',
        BANNER: 'Photos/People/photo-1527192491265-7e15c55b1ed2.png'
      },

    OPEN_DOORS_SECTION: {
        HEADER: 'We open our doors to team players who demonstrate high performance, passion, autonomy, and honesty.',
        TEXT: 'Like every company, we try to hire people excellent expertise  and knowledge of the job. But unlike many, we are precise in what personal traits we seek in new hires. For us, these are high-performance, passion, autonomy, and honesty.',
        BANNER: 'Photos/People/photo-1517841905240-472988babdf9.png'
    },

    FIRST_TABS:{
      TABS: [
        {
          HEADER: 'High-performance',
          TEXT: 'Software developers often prefer a single way of solving things in a project. However, this approach often leads to over-engineering, under-engineering, and poor performance. There is never a single way to solve a problem. We prefer the simplest and cheapest ways possible. We analyze requirements and find optimal solutions using our-of-the-box tools where possible.',
          ICON: 'ew-chart'
        },    
        {
          HEADER: 'Passion',
          TEXT: 'Rather than hiring people who focus their talents on a single area, a specific task, or within a specific environment, we hire people who demonstrate passion for what they do, be it engineering, management, design or quality. People with passion always look at the “bigger picture” and understand fundamental principles and concepts in their field of knowledge.', 
          ICON: 'ew-fire'
        },
      ]
    },

    SECOND_TABS:{
      TABS: [
        {
          HEADER: 'Autonomy',
          TEXT: 'We stay away from micromanagement. Our people have the confidence to perform tasks on their own and take responsibility for the results. Everybody on the team has the freedom to suggest improvements, tell about risks and solve problems without constant guidance. Self-discipline creates a healthy work environment and encourages proactivity.',
          ICON: 'ew-cog'
        },    
        {
          HEADER: 'Honesty',
          TEXT: 'Being transparent isn\'t about sending progress reports. It\'s about being honest about everything that is happening on the project. If something doesn\'t work as expected, we tell our client about it. We don\'t promise things we aren\'t sure we can deliver. We don\'t make a problem sound "nicer" than it actually is. We are an honest company. We work with honest people.', 
          ICON: 'ew-heart'
        },
      ]
    },

    MOVING_FORWARD_SECTION: {
      HEADER: 'Moving forward is a team\'s effort',
      TEXT: 'Our clients\' projects are run by small groups of people who bring different skills to the table: engineering, design, testing, management, DevOps. Everybody on the team collaborates with others to achieve project goals. We do great things together.',
      BANNER: 'Photos/People/beardedGuy.png',
    

      ITEMS: [

        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-white'
        },
        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-grey-light',
          [HEADER]: 'Team leader',
          [BODY]: 'How do we create high-quality software?'
        },
        {
          [TYPE]: 'IMAGE',
          [CLASSNAME]: 'hidden-lg-up',
          [SOURCE]: 'Photos/People/Group5.png'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        

        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-white',
          [HEADER]: 'Quality assurance specialist',
          [BODY]: 'Will this work properly?'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },

        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-grey-light',
          [HEADER]: 'Solution architect',
          [BODY]: 'How will it work?'
        },
        {
          [TYPE]: 'IMAGE',
          [CLASSNAME]: 'hidden-lg-up',
          [SOURCE]: 'Photos/People/Group5copy.png'
        },
        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-white',
          [HEADER]: 'UI/UX designer',
          [BODY]: 'Will users like it?'
        },
        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-grey-light',
          [HEADER]: 'Software engineer',
          [BODY]: 'How do we build it?'
        },
        {
          [TYPE]: 'IMAGE',
          [CLASSNAME]: 'hidden-lg-up',
          [SOURCE]: 'Photos/People/whiteBoard.png'
        },
        {
          [TYPE]: 'IMAGE',
          [CLASSNAME]: 'hidden-md-down', 
          [SOURCE]: 'Photos/People/Group5.png'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },

        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-white',
          [HEADER]: 'Data engineer',
          [BODY]: 'How do we make sense of data?'
        },
        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-grey-light',
          [HEADER]: 'Project manager',
          [BODY]: 'How do we achieve the project\'s goals?'
        },
        {
          [TYPE]: 'IMAGE',
          [CLASSNAME]: 'hidden-lg-up',
          [SOURCE]: 'Photos/People/group_with_board.png'
        },
        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-white',
          [HEADER]: 'Business analyst',
          [BODY]: 'What solution do we need to build?'
        },
        {
          [TYPE]: 'TEXT',
          [CLASSNAME]: 'ew-bg-grey-light',
          [HEADER]: 'DevOps engineer',
          [BODY]: 'How do we deliver value to users more efficiently?'
        },
        {
          [TYPE]: 'IMAGE',
          [CLASSNAME]: 'hidden-lg-up',
          [SOURCE]: 'Photos/People/beardedGuy.png'
        },
        {
          [TYPE]: 'IMAGE',
          [CLASSNAME]: 'hidden-md-down',
          [SOURCE]: 'Photos/People/Group5Copy.png'
        },
        {
          [TYPE]: 'EMPTY',
          [CLASSNAME]: 'hidden-md-down ew-bg-transparent'
        },
      ]
    },

    OUR_LEADERSHIP_SECTION: {
      HEADER: 'Our leadership',
      TEXT: 'Enke is run by two software engineers with 12+ years of experience in software development and people management. Both our co-founders have previously worked at international companies and helped clients such as Microsoft, Bosh, and Deloitte deliver software solutions. ',
      BANNER: 'Photos/People/whiteBoard.png',
      
      
    },

    JOIN_TEAM_SECTION: {
      HEADER: 'Want to join our team?',
      TEXT: 'To become part of Enke Systems, send us your CV. We\ll get back to you if your experience matches our current needs.',
      BUTTON_LABEL: 'SEND US YOUR CV',
      BACKGROUND: 'Photos/People/photo-1519074002996-a69e7ac46a42.png'
    }
};

