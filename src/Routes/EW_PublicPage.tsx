import React from "react";
import {AbstractComponent} from "../Core/Abstract/AbstractComponent";
import {BooleanObservable} from "../Core/Observables/BooleanObservable";
import {inject, observer} from "mobx-react";
import sharedService, {SharedService} from "../Services/Shared.service";
import routingService, {RoutingService} from "../Services/Routing.service";
import userService, {UserService} from "../Services/User.service";
import accessPointService, {AccessPointService} from "../Services/AccessPoint.service";
import {DIG_OUT, AWAIT, PIPE} from "../Core/Helpers/Helpers.misc";
import {NAME} from "../Core/Constants/PropertiesAndAttributes.cnst";
import {StructureObservable} from "../Core/Observables/StructureObservable";
import {GET} from "../Common/Constants/EW_Routes.cnst";
import {EW_GrowlModel} from "../Components/EW_Growl/EW_Growl.model";
import {AbstractPipe} from "../Core/Abstract/AbstractPipe";

const
  ROOT = `ew-public-page`;

@observer
export class EW_PublicPage extends AbstractComponent {

  protected readonly isActive: BooleanObservable = new BooleanObservable();

  protected readonly pageData: StructureObservable = new StructureObservable();

  protected readonly sharedService: SharedService;

  protected readonly routingService: RoutingService;

  protected readonly accessPointService: AccessPointService;

  componentDidMount(): void {
    const
      {props, sharedService, routingService, isActive, pageData} = this,
      {isLoading, growl, servicesAreReady} = sharedService;

    const
      dataPipe = new AbstractPipe([
        () => DIG_OUT(routingService, 'currentRoute', NAME),
        page => accessPointService.sendGet(GET.GET_PAGE, {page}),
        data => pageData.setValue(data),
        () => isActive.setValue(true)
      ], false);

    this.registerSubscriptions(

      servicesAreReady.getSubscription(state => state && dataPipe.run()),

      dataPipe.catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show()),

      dataPipe.finally(() => PIPE(
        () => AWAIT(200),
        () => isLoading.setValue(false)
      ))
    );
  }

  render () {
    return <div className={ROOT} />;
  }

  constructor (props) {
    super(props);
    this.sharedService = sharedService;
    this.routingService = routingService;
    this.accessPointService = accessPointService;
  }

}

