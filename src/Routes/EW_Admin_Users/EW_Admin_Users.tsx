import React from "react";
import {EW_PrivatePage} from "../EW_PrivatePage";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {POST} from "../../Common/Constants/EW_Routes.cnst";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";

export class EW_Admin_Users extends EW_PrivatePage {

  componentDidMount(): void {
    const
      {sharedService, accessPointService, userService, routingService, isActive, isAuthorized} = this,
      {isLoading, servicesAreReady, growl} = sharedService;

    const
      dataPipe = new AbstractPipe([
        () => isLoading.setValue(true),
        () => accessPointService.sendPost(POST.CHECK_TOKEN),
        () => isActive.setValue(true)
      ], false);

    this
      .registerPipes(dataPipe)
      .registerSubscriptions(
        isAuthorized.getSubscription(
          state => state && dataPipe.run()
        ),

        servicesAreReady.getSubscription(
          state => state && !isAuthorized.currentValue && isLoading.setValue(false)
        ),

        dataPipe.catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show()),

        dataPipe.finally(() => isLoading.setValue(false))
      );

  }

  render () {
    return <div className={`ew-admin-messages`}>
      <h1>USERS</h1>
    </div>;
  }

}

