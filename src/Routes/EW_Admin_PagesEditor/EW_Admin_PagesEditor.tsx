import React from "react";
import {observer} from "mobx-react";
import {EW_PrivatePage} from "../EW_PrivatePage";
import {
  AS_ARRAY,
  AS_FUNCTION,
  DIG_OUT,
  GET_RANDOM_ID, IS_NOT_NULL,
  AWAIT,
  PIPE
} from "../../Core/Helpers/Helpers.misc";
import {GET, POST} from "../../Common/Constants/EW_Routes.cnst";
import {_GET_NESTED_, _IS_SELECTED_, _TOGGLE_, AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {
  CONTENT,
  DATA,
  DATUM,
  ID, IMAGE,
  INDEX,
  LABEL,
  MODEL,
  NAME,
  SOURCE, TYPE
} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {EMPTY_STRING, IS_SELECTED} from "../../Core/Constants/ViewClasses.cnst";
import {EW_InputModel, I_EW_InputSettings} from "../../Components/EW_Input/EW_Input.model";
import {STRING_INPUT, TEXT_INPUT} from "../../Common/Constants/EW_InputTypes.cnst";
import {EW_Input} from "../../Components/EW_Input/EW_Input";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {EW_AnimatedSection} from "../../Components/EW_AnimatedSection/EW_AnimatedSection";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";
import {AbstractCollectionItem} from "../../Core/Abstract/AbstractCollectionItem";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {EW_FileInputModel, I_EW_FileInputSettings} from "../../Components/EW_FileInput/EW_FileInput.model";
import {EW_FileInput} from "../../Components/EW_FileInput/EW_FileInput";

const
  ROOT = `ew-admin-pages-editor`,
  SCROLLABLE_BLOCK = `${ROOT}-scrollable-block`,
  SCROLLABLE_BLOCK_ITEM = `${SCROLLABLE_BLOCK}-item`;

const
  getTextProps = (): string[] => ['SECTION_LABEL', 'HEADER', 'TEXT', 'DESCRIPTION', 'BUTTON_LABEL'],

  getFileInputProps = (): string[] => ['FILES'],
  
  getImagesInputProps = (): string[] => ['BANNER', 'BANNER_LEFT', 'BANNER_RIGHT', 'SOURCE', 'BACKGROUND'];

interface I_EW_Admin_PageEditorItem {
  [ID]: string,
  [NAME]: string,
  [MODEL]: EW_InputModel
}

@observer
export class EW_Admin_PagesEditor extends EW_PrivatePage {

  private readonly editableBlockIsReady: BooleanObservable = new BooleanObservable();

  private readonly pages: AbstractCollection = new AbstractCollection(NAME);

  private readonly sections: AbstractCollection = new AbstractCollection(NAME);

  private readonly editorBlock: AbstractCollection = new AbstractCollection(ID);

  private readonly editorBlockContent: AbstractCollection = new AbstractCollection(ID);

  private readonly saveButton: EW_ButtonModel = new EW_ButtonModel();

  private readonly cancelButton: EW_ButtonModel = new EW_ButtonModel();

  private readonly resetButton: EW_ButtonModel = new EW_ButtonModel();

  private onPageBlockClick = d => AS_FUNCTION(d[_TOGGLE_], true);

  private onSectionBlockClick = d => AS_FUNCTION(d[_TOGGLE_], true);

  private getEditableItems = () => {
    const
      {editorBlock} = this,
      levels = ['Section Header', 'Section Item', 'Item Content'];

    const
      _getEditableItems = (collection: AbstractCollection, level: number = 0) => collection ? <div className={`row`}>{collection.items.map((d, i) =>
        <div className={`${(level % 2) ? 'col-4' : 'col-12'}`} key={i}>
          <div className={`row margin-top-50`} key={i}>
            <h4 className={`col-12`}>{(level >= levels.length) ? 'Properties' : levels[level]}</h4>
            <div className={`col-12`}>{getTextProps().filter(prop => d.hasOwnProperty(prop)).map((prop, ii) =>
              <div className={`margin-top-10`} key={ii}>
                <EW_Input className={`display-block full-width`} model={d[prop][MODEL]}/>
              </div>
            )}</div>
            <div className={`col-12`}>{getFileInputProps().filter(prop => d.hasOwnProperty(prop)).map((prop, ii) =>
              <div key={ii}>
                <h4 className={`margin-top-30`}>Upload file</h4>
                <EW_FileInput className={`margin-top-10 display-block full-width`} model={d[prop][MODEL]} children={
                  <div>
                    <p>Drop file here or click to browse</p>
                    {/*<small>Allowed types=</small>*/}
                  </div>
                }/>
              </div>
            )}</div>
            <div className={`col-12`}>{getImagesInputProps().filter(prop => d.hasOwnProperty(prop)).map((prop, ii) => <div key={ii}>
                <div className={`margin-top-10`} key={ii}>
                  {!ii && <h4 className={`margin-top-30`}>Section Images</h4>}
                  <EW_Input className={`display-block full-width`} model={d[prop][MODEL]}/>
                </div>
              </div>
            )}</div>
            <div className={`col-12`}>{_getEditableItems(AS_FUNCTION(d[_GET_NESTED_], false), level + 1)}</div>
          </div>
        </div>
      )}</div> : EMPTY_STRING;

    return _getEditableItems(editorBlock);
  };

  componentDidMount(): void {
    const
      {
        isActive, isAuthorized, accessPointService, sharedService, editableBlockIsReady,
        pages, sections, editorBlock, editorBlockContent, saveButton, cancelButton, resetButton
      } = this,
      {isLoading, growl, servicesAreReady} = sharedService;

    const
      refreshPagesList = d => pages.replaceItems(Object.keys(d).map(key => d[key])),

      refreshSectionsList = d => sections.replaceItems(Object.keys(d).map(key => Object.assign({
        [NAME]: key,
        [LABEL]: key,
        [SOURCE]: d[key]
      }, d[key]))),

      refreshEditorBlock = d => {
        const
          refreshEditableItems = (d, collection) => AS_ARRAY(d).forEach((_d, i) => {
            if (!collection) return;

            if (collection.hasParent) {
              const type = DIG_OUT(_d, 'TYPE');
              if (['IMAGE', 'EMPTY'].some(t => t === type)) return;
            }

            const
              id = GET_RANDOM_ID(),
              item: any = Object.assign(
                {
                  [ID]: id,
                  [SOURCE]: collection.hasParent ? _d : _d[SOURCE]
                },
                getTextProps().reduce((textFields, prop) => Object.assign(textFields, _d.hasOwnProperty(prop) ? {
                  [prop]: {
                    [NAME]: `${DIG_OUT(pages.selection, NAME)}~${DIG_OUT(sections.selection, NAME)}~${prop}`,
                    [MODEL]: new EW_InputModel({
                      placeholder: prop,
                      type: ['TEXT', 'DESCRIPTION'].some(n => n === prop) ? TEXT_INPUT : STRING_INPUT
                    } as I_EW_InputSettings, _d[prop])
                  }
                }: {}), {}),
                getFileInputProps().reduce((fileInputFields, prop) => Object.assign(fileInputFields, _d.hasOwnProperty(prop) ? {
                  [prop]: {
                    [NAME]: `${DIG_OUT(pages.selection, NAME)}~${DIG_OUT(sections.selection, NAME)}~${prop}`,
                    [MODEL]: new EW_FileInputModel({
                      placeholder: 'FILE'
                    } as I_EW_FileInputSettings, _d[prop])
                  }
                } : {}), {}),
                getImagesInputProps().reduce((imageInputFields, prop) => Object.assign(imageInputFields, _d.hasOwnProperty(prop) ? {
                  [prop]: {
                    [NAME]: `${DIG_OUT(pages.selection, NAME)}~${DIG_OUT(sections.selection, NAME)}~${prop}`,
                    [MODEL]: new EW_InputModel({
                      placeholder: prop,
                      type: STRING_INPUT
                    } as I_EW_InputSettings, _d[prop])
                  }
                } : {}), {})
              );

            collection.addItems(item);

            _d.hasOwnProperty('ITEMS') && refreshEditableItems(
              _d['ITEMS'],
              (collection.getItem(id, false) as AbstractCollectionItem).createNestedCollection()
            );
          });

        return PIPE(
          editableBlockIsReady.setValue(false),
          () => editorBlock.clearItems(),
          () => AWAIT(),
          () => refreshEditableItems(d, editorBlock),
          () => AWAIT(),
          () => editableBlockIsReady.setValue(true)
        );
      },

      onSaveButtonClick = () => {
        const
          page = DIG_OUT(pages.selection, NAME),
          section = DIG_OUT(sections.selection, NAME),
          data = DIG_OUT(sections.selection, SOURCE),
          filesToUpload: any[] = [];

        const
          updateContent = collection => collection && collection.getItems(false).forEach((item: AbstractCollectionItem) => {
            const datum = item[DATUM];

            getTextProps()
              .filter(prop => datum.hasOwnProperty(prop))
              .forEach(prop => datum[prop][MODEL].hasChanges && (datum[SOURCE][prop] = datum[prop][MODEL].currentValue));

            getFileInputProps()
              .filter(prop => datum.hasOwnProperty(prop))
              .forEach(prop => {
                if (datum[prop][MODEL].hasChanges) {
                  let file = DIG_OUT(datum[prop][MODEL], 'currentValue', DATA);
                  datum[SOURCE][prop] = {
                    [NAME]: datum[prop][MODEL].fileName,
                    [TYPE]: DIG_OUT(file, TYPE)
                  };
                  filesToUpload.push(file);
                }
              });
            
            getImagesInputProps()
              .filter(prop => datum.hasOwnProperty(prop))
              .forEach(prop => datum[prop][MODEL].hasChanges && (datum[SOURCE][prop] = datum[prop][MODEL].currentValue));
  
            updateContent(item.nestedCollection);
          });

        updateContent(editorBlock);

        const
          saveChangesPipe = new AbstractPipe(([
              () => isLoading.setValue(true)
            ] as any[])
            .concat(filesToUpload.map(file => () => accessPointService.sendFile(file)))
            .concat([
              () => accessPointService.sendPost(POST.UPDATE_PAGE, {page, section, data}),
              () => accessPointService.sendGet(GET.GET_PAGES),
              res => refreshPagesList(res),
              () => pages.select(page),
              () => AWAIT(),
              () => sections.select(section),
              () => isLoading.setValue(false)
            ])
          );

        this
          .registerPipes(saveChangesPipe)
          .registerSubscriptions(
            saveChangesPipe.then(res => console.log(res)),
            saveChangesPipe.catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show()),
            saveChangesPipe.finally(() => isLoading.setValue(false))
          );
      },

      onResetButtonClick = () => {
        const
          page = DIG_OUT(pages.selection, NAME),
          section = DIG_OUT(sections.selection, NAME);

        const
          resetSectionPipe = new AbstractPipe([
            () => isLoading.setValue(true),
            () => accessPointService.sendPost(POST.RESET_SECTION, {page, section}),
            () => accessPointService.sendGet(GET.GET_PAGES),
            res => refreshPagesList(res),
            () => pages.select(page),
            () => AWAIT(),
            () => sections.select(section),
            () => isLoading.setValue(false)
          ]);

        this
          .registerPipes(resetSectionPipe)
          .registerSubscriptions(
            resetSectionPipe.catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show()),
            resetSectionPipe.finally(() => isLoading.setValue(false))
          );
      };

    const
      dataPipe = new AbstractPipe([
        () => isLoading.setValue(true),
        () => accessPointService.sendPost(POST.CHECK_TOKEN),
        () => accessPointService.sendGet(GET.GET_PAGES),
        res => refreshPagesList(res),
        () => isActive.setValue(true),
        () => isLoading.setValue(true),
      ], false);

    const
      formChangeObserver = new AbstractObserver(
        () => {
          let
            hasChanges = false;

          const
            checkCollection = (collection) => collection && collection.getItems(false).forEach((item: AbstractCollectionItem) => {
              if (hasChanges) return;

              const
                d = item[DATUM];

              [getTextProps(), getFileInputProps(), getImagesInputProps()]
                .forEach(
                  props => props
                    .filter(prop => d.hasOwnProperty(prop))
                    .forEach(prop => hasChanges = hasChanges || d[prop][MODEL].hasChanges)
                );

              checkCollection(item.nestedCollection);
            });

          checkCollection(editorBlock);

          return hasChanges;
        }
      );

    this
      .registerPipes(dataPipe)
      .registerSubscriptions(

        isAuthorized.getSubscription(state => state && dataPipe.run()),

        servicesAreReady.getSubscription(state => state && !isAuthorized.currentValue && sharedService.isLoading.setValue(false)),

        dataPipe.catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show()),

        dataPipe.finally(() => isLoading.setValue(false)),

        pages.subscribeOnSelectionChange(selection => selection && refreshSectionsList(selection[CONTENT])),

        sections.subscribeOnSelectionChange(selection => selection && refreshEditorBlock(selection)),

        formChangeObserver.getSubscription(val => saveButton.toggleDisabled(!val)),

        saveButton.onClick.getSubscription(() => onSaveButtonClick()),

        cancelButton.onClick.getSubscription(() => sections.clearSelection()),

        resetButton.onClick.getSubscription(() => onResetButtonClick())
      );
  }

  render() {
    const
      {isAuthorized, isActive, pages, sections, editorBlock, editableBlockIsReady, saveButton, cancelButton, resetButton} = this;

    return isAuthorized.currentValue ? <EW_AnimatedSection isActive={isActive} children={
      <div className={`${ROOT} padding-bottom-100`}>
        <div className={`container`}>
          <div className={`row margin-top-50`}>
            <h2 className={`col-12 ew-hidden-bottom transition-duration-10 transition-delay-3`}>PAGES</h2>
            <div className={`col-12 ew-hidden-bottom transition-duration-10 transition-delay-6`}>
              <div className={`${SCROLLABLE_BLOCK}`}>
                {pages.items.map((d, i) =>
                  <div className={`${SCROLLABLE_BLOCK_ITEM} ${AS_FUNCTION(d[_IS_SELECTED_]) ? IS_SELECTED + ' ew-clickable': EMPTY_STRING}
                 ew-block-width-3 ew-block-height-2 margin-10 display-inline-flex ew-clickable padding-50`}
                       key={i} onClick={() => this.onPageBlockClick(d)}>
                    <h4 className={``}>{d[LABEL]}</h4>
                  </div>
                )}
              </div>
            </div>
          </div>
          {!!pages.selection && <div className={`row margin-top-50`}>
            <h2 className={`col-12`}>SECTIONS</h2>
            <div className={`col-12`}>
              <div className={`${SCROLLABLE_BLOCK}`}>
                {sections.items.map((d, i) =>
                  <div className={`${SCROLLABLE_BLOCK_ITEM} ${AS_FUNCTION(d[_IS_SELECTED_]) ? IS_SELECTED + ' ew-clickable': EMPTY_STRING}
                 ew-block-width-3 ew-block-height-2 margin-10 display-inline-flex ew-clickable padding-50`}
                       key={i} onClick={() => this.onSectionBlockClick(d)}>
                    <h4 className={``}>{d[LABEL]}</h4>
                  </div>
                )}
              </div>
            </div>
          </div>}
        </div>
        {!!sections.selection && <div className={`container`}>
          <div className={`row margin-top-50`}>
            <h2 className={`col-12`}>CONTENT</h2>
            <div className={`col-12`}>{editableBlockIsReady.value ? this.getEditableItems() : false}</div>
          </div>
          <div className={`row margin-top-50`}>
            <div className={`col-12 text-right`}>
              <EW_Button model={cancelButton} children={
                EW_ButtonModel.buildSquareButton('Cancel', 'red-button')
              }/>
              <EW_Button className={`margin-left-10`} model={resetButton} children={
                EW_ButtonModel.buildSquareButton('Reset')
              }/>
              <EW_Button className={`margin-left-10`} model={saveButton} children={
                EW_ButtonModel.buildSquareButton('Save')
              }/>
            </div>
          </div>
        </div>}
      </div>}
    /> : <div />;
  }
}

