import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {CLASSNAME, MODEL, NAME, TYPE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EW_InputModel} from "../../Components/EW_Input/EW_Input.model";
import {EW_FileInputModel} from "../../Components/EW_FileInput/EW_FileInput.model"
import {
  IS_REQUIRED,
  IS_VALID_EMAIL,
  IS_VALID_PHONE,
  IS_VALID_FILE_TYPE, IS_VALID_FILE_SIZE
} from "../../Common/Helpers/EW_Validators";
import {STRING_INPUT, TEXT_INPUT, FILE_INPUT} from "../../Common/Constants/EW_InputTypes.cnst";
import {I_EW_FormField} from "../../Common/Interfaces/FormField.interface";
import {computed} from "mobx";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";

export class EW_CV_Model {

  public readonly allowedFileTypes: any = {
    WORD: 'application/msword',
    POWER_POINT: 'application/vnd.ms-powerpoint',
    PDF: 'application/pdf'
  };

  public fields: AbstractCollection = new AbstractCollection(NAME).addItems([
    {
      [NAME]: 'email',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED(), IS_VALID_EMAIL()],
        type: STRING_INPUT,
        placeholder: 'Your email'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'phone',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED(), IS_VALID_PHONE()],
        type: STRING_INPUT,
        placeholder: 'Phone number'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'name',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'Name'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'Surname',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'Surname'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'CV',
      [TYPE]: 'fileInput',
      [MODEL]: new EW_FileInputModel({
        placeholder: 'The file should be ',
        validators: {
          isRequired: IS_REQUIRED(),
          isValidType: IS_VALID_FILE_TYPE(
            Object.keys(this.allowedFileTypes).map(key => this.allowedFileTypes[key])
          ),
          isValidSize: IS_VALID_FILE_SIZE("15MB")
        },
        
      }),
      [CLASSNAME]: 'col-12'
    }
  ] as I_EW_FormField[]);

  public onSubmit: AbstractEventObserver = new AbstractEventObserver('submit');

  public resetForm = () => {
    this.fields.items.forEach((d: I_EW_FormField) => d[MODEL].clearValue());
    return this;
  };

  @computed
  public get isValid() {
    return this.fields.items.reduce(
      (isValid: boolean, d: I_EW_FormField) => isValid ? d[MODEL].isValid : isValid, true
    );
  }

  @computed
  public get value() {
    return this.isValid ?
      this.fields.items.reduce((agg, d: I_EW_FormField) => Object.assign(agg, {[d[NAME]]: d[MODEL].currentValue || undefined}), {}) : null;
  }
}
