import React from "react";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {inject, observer} from "mobx-react";
import {SharedService} from "../../Services/Shared.service";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {DATA} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";
import {CLASSNAME, MODEL, NAME} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {I_EW_FormField} from "../../Common/Interfaces/FormField.interface";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {EW_CV_Model} from "./EW_CV_Model";
import {EW_Input} from "../../Components/EW_Input/EW_Input";
import {AS_FUNCTION, DIG_OUT} from "../../Core/Helpers/Helpers.misc";
import {EW_FileInput} from "../../Components/EW_FileInput/EW_FileInput"
import {EW_FileInputModel} from "../../Components/EW_FileInput/EW_FileInput.model";

const
  ROOT = `ew-cv-page`;
const
  DEFAULT_BG = 'Photos/Common/bg.png';


@inject('sharedService')
@observer
export class EW_CV_Page extends AbstractComponent {
  
  private sharedService: SharedService;
  
  private form: EW_CV_Model = new EW_CV_Model();
  
  private submitButton: EW_ButtonModel = new EW_ButtonModel();
  
  componentDidMount(): void {
    const
      {sharedService, submitButton, props, form} = this,
      {isLoading, servicesAreReady, growl} = sharedService;
    
    const
      fileInput: EW_FileInputModel = DIG_OUT(form.fields.getItem('CV'), MODEL);
    
    const
      formStateObserver = new AbstractObserver(
        () => form.isValid
      );
    
    this.registerSubscriptions(
      servicesAreReady.getSubscription(state => state && isLoading.setValue(false)),
      
      formStateObserver.getSubscription(state => submitButton.toggleDisabled(!state)),
      
      fileInput && fileInput.onError.getSubscription(
      e => {
        console.log(e[DATA]);
        growl.addMessage(EW_GrowlModel.buildErrorMessage(e[DATA])).show();
      }
      ),
      
      submitButton.onClick.getSubscription(
        e => {
          // if (!form.isValid) return;
          // let {value} = form;
          //
          // CHECK_AND_CALL(DIG_OUT(props, 'onSubmit'), value);
          // form.onSubmit.next(e, value);
        }
      )
    );
  }
  
  
  render() {
    const {props, form} = this,
      {className, children, image} = props;
    
    
    return <article className={`${ROOT}`}>
      <section className={`ew-section`}>
        <div className={`pos-abs size-cover`}>
          <EW_BackgroundImage className={'size-contain'} source={image || DEFAULT_BG}/>
        </div>
        <div className={`container`}>
          <div className={`row`}>
            <div className={`col-12`}>
              <div className={``}>{children}</div>
            </div>
            <div className={`col-2 hidden-md-down`}/>
            <div className={`col-12 col-lg-8 col-xl-8`}>
              <div className={` row padding-top-50`}>
                {form.fields.items.map((d: I_EW_FormField, i) =>
                  <div
                    className={`${d[CLASSNAME]} margin-bottom-20 ew-hidden-bottom transition-duration-10 transition-delay-${6 + (i * 2)}`}
                    key={i}>
                    <div className={`ew-form-input`}>
                      {d.type === 'fileInput' ?
                        <EW_FileInput className={`full-width`} model={d[MODEL]} children={
                          <div>
                            File should be .doc, .docx, .ppt or .pdf format. Max file size -
                            10Mb
                          </div>}/> :
                        <EW_Input className={`full-width`} model={d[MODEL]}/>}
                    
                    </div>
                  </div>
                )}
                <div
                  className={`col-12 text-center ew-hidden-bottom transition-duration-10 transition-delay-15`}>
                  <EW_Button model={this.submitButton}
                             children={EW_ButtonModel.buildSquareButton('Submit')}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </article>
  }
  
  constructor(props) {
    super(props);
    this.sharedService = props.sharedService;
  }
}

