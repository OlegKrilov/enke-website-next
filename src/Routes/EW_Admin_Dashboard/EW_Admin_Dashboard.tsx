import moment from "moment";
import React from "react";
import {EW_PrivatePage} from "../EW_PrivatePage";
import {observer} from "mobx-react";
import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {CLASSNAME, CONTENT, DB_ID, LABEL, NAME} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {ROUTES} from "../../Services/Routing.service";
import {EW_AnimatedSection} from "../../Components/EW_AnimatedSection/EW_AnimatedSection";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {GET, POST} from "../../Common/Constants/EW_Routes.cnst";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {IoMdMail, IoMdPerson} from "react-icons/all";
import {AS_FUNCTION, DIG_OUT, PIPE} from "../../Core/Helpers/Helpers.misc";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";

const
  ROOT = `ew-admin-dashboard`,
  TAB = `${ROOT}-tab`,
  TAB_HEADER = `${TAB}-header`,
  TAB_BODY = `${TAB}-body`,
  TAB_FOOTER = `${TAB}-footer`;

const
  DATE_FORMAT = 'MM/DD/YYYY hh:mm:ss';

interface I_EW_AdminDashboardTab {
  [NAME]: string,
  [LABEL]: string,
  [CLASSNAME]: string,
  [CONTENT]: any
}

@observer
export class EW_Admin_Dashboard extends EW_PrivatePage {

  private readonly messages: AbstractCollection = new AbstractCollection(DB_ID);

  private readonly tabs: AbstractCollection = new AbstractCollection(NAME).addItems([
    {
      [NAME]: ROUTES.USERS,
      [LABEL]: 'Users',
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6 margin-bottom-20 ew-hidden-bottom transition-duration-10 transition-delay-9',
      [CONTENT]: () => <div className={`display-flex`}>
        <div className={`ew-block-height ew-block-width`}>
          <IoMdPerson className={`size-cover`}/>
        </div>
        <div className={`margin-left-20`}>
          <b className={`font-size-20`}>{DIG_OUT(this.userService, 'user', 'login')}</b>
          <br />
          <i className={`font-size-14`}>{DIG_OUT(this.userService, 'user', 'role')}</i>
        </div>
      </div>
    },
    {
      [NAME]: ROUTES.PAGES_EDITOR,
      [LABEL]: 'Pages',
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6 margin-bottom-20 ew-hidden-bottom transition-duration-10 transition-delay-6',
      [CONTENT]: () => <div className={``}>{this.routingService.publicPages.map((d, i) =>
        <div className={`margin-v-10`} key={i}>
          <b className={`margin-right-10`}>{i + 1}.</b>
          <b>{d[LABEL]}</b>
        </div>
      )}</div>
    },
    {
      [NAME]: ROUTES.MESSAGES,
      [LABEL]: 'Messages',
      [CLASSNAME]: 'col-12 margin-bottom-20 ew-hidden-bottom transition-duration-10 transition-delay-12',
      [CONTENT]: () => <div className={``}>{
        this.messages.size ? this.messages.items.map((d, i) =>
          <div className={`row`} key={i}>
            <div className={`col-2`}>
              <IoMdMail />
            </div>
            <div className={`col-5`}>
              <span>{d['text']}</span>
            </div>
            <div className={`col-5 text-right`}>
              <i>{moment(d['created']).format(DATE_FORMAT)}</i>
            </div>
          </div>
        ) : <div />
      }</div>
    }
  ] as I_EW_AdminDashboardTab[]);

  private onLogOutClick = () => {
    const
      {sharedService, userService, routingService} = this,
      {isLoading, growl} = sharedService;

    PIPE(
      isLoading.setValue(true),
      () => userService.logout(),
      () => routingService.goTo(ROUTES.HOME)
    )
      .catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show())
  };

  private onDropDatabaseClick = () => {
    const
      {sharedService, userService, accessPointService} = this,
      {isLoading, growl} = sharedService;

    const dropDatabasePipe = new AbstractPipe([
      () => isLoading.setValue(true),
      () => accessPointService.sendPost(POST.RESET_DB),
      res => console.log(res),
      () => userService.logout(),
      res => console.log(res)
    ]);

    this
      .registerPipes(dropDatabasePipe)
      .registerSubscriptions(
        dropDatabasePipe.finally(() => isLoading.setValue(false)),
        dropDatabasePipe.catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show())
      );
  };

  componentDidMount(): void {
    const
      {sharedService, accessPointService, userService, isAuthorized, isActive, messages} = this,
      {isLoading, servicesAreReady, growl} = sharedService;

    const
      dataPipe = new AbstractPipe([
        () => isLoading.setValue(true),
        () => accessPointService.sendPost(POST.CHECK_TOKEN),
        () => accessPointService.sendGet(GET.GET_PAGES),
        () => accessPointService.sendGet(GET.GET_MESSAGES, {isRead: false}),
        messagesData => messages.replaceItems(messagesData),
        () => isActive.setValue(true)
      ], false);

    this.registerSubscriptions(
      isAuthorized.getSubscription(
        state => state && dataPipe.run()
      ),

      servicesAreReady.getSubscription(
        state => (state && !isAuthorized.currentValue) && sharedService.isLoading.setValue(false)
      ),

      dataPipe.catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show()),

      dataPipe.finally(() => isLoading.setValue(false))

    );

  }

  render () {
    const
      {isAuthorized, routingService, isActive, tabs} = this;

    return isAuthorized.currentValue ?
      <EW_AnimatedSection className={`${ROOT}`} isActive={isActive} children={
        <div className={`container margin-v-50`}>
          <div className={`row`}>{tabs.items.map((d: I_EW_AdminDashboardTab, i: number) =>
            <div className={`${d[CLASSNAME]}`} key={i}>
              <div className={`${TAB} pos-rel`}>
                <div className={`${TAB_HEADER} ew-bg-blue ew-block-height-0-and-half display-flex ew-color-white padding-h-30`}>
                  <b>{d[LABEL]}</b>
                </div>
                <div className={`${TAB_BODY} padding-30 ew-block-height-2`}>{AS_FUNCTION(d[CONTENT])}</div>
                <div className={`${TAB_FOOTER} pos-abs bottom-0 left-0 full-width ew-block-height display-flex align-right padding-h-30`}>
                  <EW_Button onClick={() => routingService.goTo(d[NAME])} children={
                    EW_ButtonModel.buildSquareButton('See more')
                  }/>
                </div>
              </div>
            </div>
          )}</div>
          <div className={`row`}>
            <div className={`col-12 ew-hidden-bottom transition-duration-10 transition-delay-9`}>
              <div className={`display-flex align-right ew-block-height`}>
                <EW_Button className={`margin-right-10`} onClick={() => this.onDropDatabaseClick()} children={
                  EW_ButtonModel.buildSquareButton('Drop Database', 'red-button')
                }/>
                <EW_Button onClick={() => this.onLogOutClick()} children={
                  EW_ButtonModel.buildSquareButton('Sign Out')
                }/>
              </div>
            </div>
          </div>
        </div>
      }/> : <div/>;
  }
}

