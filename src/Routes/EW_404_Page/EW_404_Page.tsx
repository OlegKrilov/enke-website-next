import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {IAppRoute, IS_MENU_ITEM, ROUTES, RoutingService} from "../../Services/Routing.service";
import {inject, observer} from "mobx-react";
import {
    EW_DECORATION_BLOCK_WHITE,
    EW_PRIMARY_BANNER,
    EW_PRIMARY_TEXT, EW_SECONDARY_BANNER, EW_SECONDARY_TEXT,
    EW_SECTION_HEADER
  } from "../../Common/Constants/EW_ViewClasses.cnst";
import {AWAIT} from "../../Core/Helpers/Helpers.misc";

const
  ROOT = `ew-404-page`;
  
@inject('routingService', 'sharedService')
@observer
export default class EW_404_PAGE extends AbstractComponent {

  private routingService: RoutingService;

  componentDidMount(): void {
    AWAIT(200).then(() => this.props.sharedService.isLoading.setValue(false));
  }

  render () {
    return <div className={ROOT}>
      <section className={`ew-section ew-block-height-7 pos-rel padding-top-100`}>
      <div className={`container`}>
        <div className={`row ew-bg-blue ew-color-white`}>
          <div className={`col-12 text-center margin-bottom-200 margin-top-150 `}>
            <div className={`${EW_PRIMARY_BANNER}`}>
              <b>ERROR 404</b>
            </div>
            <div className={`${EW_SECONDARY_TEXT} font-size-20`}>Looks like you are in the wrong place</div>
            <div className={`margin-top-40`}>
                  <EW_Button onClick={(e) => this.routingService.goTo()} children={
                    EW_ButtonModel.buildSquareButton("GO HOME")
                  }/>
            </div>
          </div>
        </div>
      </div>
      </section>
    </div>;
  }
  constructor (props) {
    super(props);
    this.routingService = props.routingService;
  }
}
