import React from "react";
import {AbstractComponent} from "../../../Core/Abstract/AbstractComponent";
import {AbstractCollection} from "../../../Core/Abstract/AbstractCollection";
import {DATA, DATE, DB_ID, NAME, TEXT, VALUE} from "../../../Core/Constants/PropertiesAndAttributes.cnst";
import {BooleanObservable} from "../../../Core/Observables/BooleanObservable";
import {inject, observer} from "mobx-react";
import {EW_InputModel} from "../../../Components/EW_Input/EW_Input.model";
import {TEXT_INPUT} from "../../../Common/Constants/EW_InputTypes.cnst";
import {EW_ButtonModel} from "../../../Components/EW_Button/EW_Button.model";
import EW_Spinner from "../../../Components/EW_Spinner/EW_Spinner";
import {EW_Input} from "../../../Components/EW_Input/EW_Input";
import {EW_Button} from "../../../Components/EW_Button/EW_Button";
import {AbstractPipe} from "../../../Core/Abstract/AbstractPipe";
import {I_EW_BlogPost} from "../../../Common/Interfaces/BlogPost.interface";
import {StructureObservable} from "../../../Core/Observables/StructureObservable";
import {EW_Section} from "../../../Components/EW_Section/EW_Section";
import {EW_SectionModel} from "../../../Components/EW_Section/EW_Section.model";
import {DIG_OUT} from "../../../Core/Helpers/Helpers.misc";
import {AccessPointService} from "../../../Services/AccessPoint.service";
import {POST} from "../../../Common/Constants/EW_Routes.cnst";
import {SharedService} from "../../../Services/Shared.service";
import {EW_GrowlModel} from "../../../Components/EW_Growl/EW_Growl.model";
import {ADD_COMMENT_FORM, EW_FormModel} from "../../../Components/EW_Form/EW_Form.model";
import EW_Form from "../../../Components/EW_Form/EW_Form";
import Moment from "react-moment";

const
	DATE_FORMAT = `LL`;

const
	ROOT = `ew-blog-post-page-comments`;

@inject('sharedService', 'accessPointService', 'routingService')
@observer
export default class EW_BlogPostPageComments extends AbstractComponent {
	
	private readonly model: StructureObservable;
	
	private readonly section: EW_SectionModel = new EW_SectionModel();
	
	private readonly isLoading: BooleanObservable = new BooleanObservable(true);
	
	private readonly commentSubmitted: BooleanObservable = new BooleanObservable();
	
	private readonly commentReceived: BooleanObservable = new BooleanObservable();
	
	private readonly textInput: EW_InputModel = new EW_InputModel({
		type: TEXT_INPUT,
		placeholder: 'Your comment'
	});
	
	private readonly submitButton: EW_ButtonModel = new EW_ButtonModel();
	
	componentDidMount() {
		const
			{model, isLoading, textInput, submitButton, section, props, commentSubmitted, commentReceived} = this,
			{sharedService, accessPointService, routingService} = props;
		
		const
			addCommentPipe: AbstractPipe = new AbstractPipe([
				() => commentSubmitted.toggleValue(true),
				() => (accessPointService as AccessPointService).sendPost(POST.COMMENT_POST, {
					id: DIG_OUT(model.value, DB_ID),
					text: textInput.currentValue
				}),
				commentsList => model.setValue(Object.assign(model.value, {comments: commentsList})),
				() => commentReceived.toggleValue(true),
				() => textInput.clearValue(),
				
			], false);
		
		section.isActive.setValue(true);
		
		isLoading.setValue(false);
		
		submitButton.toggleDisabled(true)
		
		this.registerSubscriptions(
			textInput.onChange.getSubscription(
				e => submitButton.toggleDisabled(!DIG_OUT(e, DATA))
			),
			
			submitButton.onClick.getSubscription(
				e => addCommentPipe.run()
			),
			
			addCommentPipe.catch(err => (sharedService as SharedService).growl
				.addMessage(EW_GrowlModel.buildErrorMessage(err), 'error')
				.show()
			),
			
			addCommentPipe.finally(() => commentSubmitted.toggleValue(false))
		
		);
	}
	
	render () {
		const
			{model, isLoading, textInput, submitButton, section, commentReceived, commentSubmitted} = this;
		
		const
			comments = [...DIG_OUT(model.value, 'comments') || []].reverse(),
			hasData = comments.length > 0;
		
		return isLoading.value ?
			<div className={`ew-block-height`}>
				<EW_Spinner />
			</div> :
			<EW_Section model={section}>
				<div className={`${ROOT}`}>
					<div className={`padding-right-100 pos-rel`}>
						<h4 className={`padding-v-20`}>Comments ({comments.length})</h4>
						{!commentReceived.value && <div className={`ew-hidden-bottom transition-duration-10 transition-delay-3`}>
							<EW_Input className={`display-flex full-width ew-block-height-2`} model={textInput}/>
						</div>}
						{!commentReceived.value && <div className={`ew-hidden-bottom transition-duration-10 transition-delay-6 text-right padding-v-20`}>
							<EW_Button model={submitButton}>{
								EW_ButtonModel.buildSquareButton('POST COMMENT')
							}</EW_Button>
						</div>}
						{commentReceived.value && <h5 className={`padding-bottom-100 opacity-5`}>Thank you! Your comment was sent.</h5>}
						{commentSubmitted.value && <div className={`pos-abs top-0 left-0 size-cover`}>
							<EW_Spinner />
						</div>}
					</div>
					{comments.map((d, i) =>
						<div className={`ew-hidden-${i % 2 ? 'left' : 'right'}
							padding-${i % 2 ? 'left' : 'right'}-50
							transition-duration-10 transition-delay-${(i % 20) + 1} font-size-12`} key={i}>
							<small><Moment format={DATE_FORMAT} date={d[DATE]}/></small>
							<p className={`padding-bottom-10`}>{d[TEXT]}</p>
						</div>
					)}
				</div>
			</EW_Section>;
	}
	
	constructor(props) {
		super(props);
		this.model = props.model;
	}
	
}

