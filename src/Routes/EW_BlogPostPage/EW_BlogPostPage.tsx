import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {UserService} from "../../Services/User.service";
import {SharedService} from "../../Services/Shared.service";
import {AccessPointService} from "../../Services/AccessPoint.service";
import {inject, observer} from "mobx-react";
import {RoutingService} from "../../Services/Routing.service";
import EW_Spinner from "../../Components/EW_Spinner/EW_Spinner";
import {AS_ARRAY, DIG_OUT, GET_RANDOM_ID} from "../../Core/Helpers/Helpers.misc";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {GET} from "../../Common/Constants/EW_Routes.cnst";
import {StructureObservable} from "../../Core/Observables/StructureObservable";
import {IoIosBriefcase} from "react-icons/all";
import Moment from "react-moment";
import {I_EW_BlogPost} from "../../Common/Interfaces/BlogPost.interface";
import {CONTENT, DATA, FORMAT, ID, TYPE, VALUE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EW_Section} from "../../Components/EW_Section/EW_Section";
import {EW_SectionModel} from "../../Components/EW_Section/EW_Section.model";
import {IMAGE_BLOCK, TEXT_BLOCK} from "../../Main/EW_AdminBlogConstructor/EW_AdminBlogConstructorContent/EW_AdminBlogConstructorContent";
import EW_SocialLinks from "../../Main/EW_SocialLinks/EW_SocialLinks";
import EW_BlogPostPageComments from "./EW_BlogPostPageComments/EW_BlogPostPageComments";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {MdFormatQuote} from "react-icons/md/index";
import {TextDecorationEnum} from "../../Common/Enums/BlockViewClasses.enum";

const
	DATE_FORMAT = `LL`;

const
	ROOT = `ew-blog-post-page`,
	POST_HEADER = `${ROOT}-header`,
	POST_BODY = `${ROOT}-body`,
	POST_BODY_ITEM = `${POST_BODY}-item`,
	POST_BODY_ITEM_CONTENT = `${POST_BODY_ITEM}-content`,
	POST_BODY_ITEM_DECORATION = `${POST_BODY_ITEM}-decoration`,
	POST_FOOTER = `${ROOT}-footer`,
	POST_FOOTER_COMMENTS_BLOCK = `${ROOT}-comments-block`;

@inject('userService', 'sharedService', 'accessPointService', 'routingService')
@observer
export default class EW_BlogPostPage extends AbstractComponent {
	
	private readonly userService: UserService;
	
	private readonly sharedService: SharedService;
	
	private readonly accessPointService: AccessPointService;
	
	private readonly routingService: RoutingService;

	private readonly data: StructureObservable = new StructureObservable();
	
	private readonly section: EW_SectionModel = new EW_SectionModel();
	
	private getSection = (d) => <div className={`pos-rel`}>{
		AS_ARRAY(DIG_OUT(d, CONTENT) || []).map((dd, i) => {
			const
				id = DIG_OUT(dd, ID) || GET_RANDOM_ID(),
				type = DIG_OUT(dd, TYPE) || TEXT_BLOCK,
				data = DIG_OUT(dd, DATA) || EMPTY_STRING,
				formats = AS_ARRAY(DIG_OUT(dd, FORMAT) || []),
				isQuote = !!formats.find(str => str === TextDecorationEnum.blockquote);
			
			console.log(isQuote, formats);
			
			return <EW_Section className={`pos-rel`} model={this.section} key={id}>
				<div className={`${POST_BODY_ITEM_CONTENT} ew-hidden-bottom transition-duration-10 transition-delay-${6 + i}`}>
					{type === TEXT_BLOCK && <p className={`${formats.join(' ')}`}>{data}</p>}
					{type === IMAGE_BLOCK && <EW_BackgroundImage className={`ew-block-height-3 margin-bottom-30`} source={data} />}
					{isQuote && <div className={`${POST_BODY_ITEM_DECORATION}
							ew-block-width ew-block-height
							ew-hidden-left transition-duration-10 transition-delay-12
							pos-abs top-0 left-0 text-right font-size-50`}>
						<MdFormatQuote className={`opacity-4`} />
					</div>}
				</div>
			</EW_Section>;
		})
	}</div>;
		
		
		
		// <EW_Section className={`pos-rel size-cover`} model={this.section}>
		// <div className={`size-cover ew-hidden-bottom transition-duration-10 transition-delay-9`}>{
		// 	AS_ARRAY(DIG_OUT(d, CONTENT) || []).map((dd, i) =>
		// 		<div className={`${DIG_OUT(d, TYPE)} ${AS_ARRAY(dd.format)}`} key={i}>{dd.data}</div>
		// 	)
		// }</div>
		
		
		// {/*<div className={`size-cover ew-hidden-bottom transition-duration-10 transition-delay-9`}>*/}
		// {/*	{d.type === TEXT_BLOCK && <p className={`padding-v-50`}>{d.data}</p>}*/}
		// {/*	{d.type === IMAGE_BLOCK && <EW_BackgroundImage source={d.data} />}*/}
		// {/*</div>*/}
		// {/*<div className={`${POST_BODY_ITEM_DECORATION}*/}
		// {/*	ew-hidden-left transition-duration-10 transition-delay-12*/}
		// {/*	pos-abs top-0 text-right font-size-50*/}
		// {/*`}>*/}
		// {/*	<MdFormatQuote className={`opacity-4`} />*/}
		// {/*</div>*/}
	// </EW_Section>;
	
	componentDidMount() {
		const
			{userService, sharedService, accessPointService, data, section} = this,
			{isActive} = section;
			
		const
			postID = DIG_OUT(this.routingService.currentParams, 'postId');

		const
			dataPipe: AbstractPipe = new AbstractPipe([
				() => sharedService.isLoading.setValue(true),
				() => accessPointService.sendGet(GET.READ_POST, {id: postID}),
				res => data.setValue(res),
				() => console.log(data.value),
				() => isActive.setValue(true)
			], false);

		this.registerSubscriptions(
			dataPipe.catch(
				err => console.log(err)
			),

			dataPipe.finally(
				() => sharedService.isLoading.setValue(false)
			)
		);
		
		dataPipe.run();
	}
	
	render () {
		const
			{data, section} = this,
			currentValue: I_EW_BlogPost = DIG_OUT(data, VALUE);
		
		return data.isEmpty ?
			<div className={`ew-block-height`}>
				<EW_Spinner />
			</div> : <div className={`${ROOT} padding-bottom-50`}>
				<EW_Section className={`${POST_HEADER}`} model={section}>
					<div className={`container`}>
						<div className={`row`}>
							<div className={`col-12`}>
								<div className={`padding-v-20 display-flex align-left ew-hidden-right transition-duration-10 transition-delay-3`}>
									<b>{currentValue.author}</b>
									<IoIosBriefcase />
									<span className={`margin-h-10`}>|</span>
									<Moment date={currentValue.date} format={DATE_FORMAT} />
								</div>
							</div>
							<div className={`col-12`}>
								<h2 className={`ew-hidden-left transition-duration-10 transition-delay-6`}>{currentValue.header}</h2>
							</div>
						</div>
					</div>
				</EW_Section>
				<div className={`${POST_BODY} container margin-v-50`}><div className={`row`}>{
					AS_ARRAY(currentValue.content || []).map((d, i) =>
						<div className={`${POST_BODY_ITEM} ${d.size}`} key={i}>{
							this.getSection(d)
						}</div>)
				}</div></div>
				<div className={`${POST_FOOTER} container`}>
					<div className={`row`}>
						<div className={`col-12`}>
							<hr />
							<EW_Section model={section}>
								<EW_SocialLinks className={`ew-hidden-right transition-duration-10 transition-delay-12`}/>
							</EW_Section>
							<hr />
						</div>
						<div className={`${POST_FOOTER_COMMENTS_BLOCK} col-12`}>
							<EW_BlogPostPageComments model={data} />
						</div>
					</div>
				</div>
			</div>;
	}
	
	constructor(props) {
		super(props);
		this.accessPointService = props.accessPointService;
		this.sharedService = props.sharedService;
		this.userService = props.userService;
		this.routingService = props.routingService;
	}
}
