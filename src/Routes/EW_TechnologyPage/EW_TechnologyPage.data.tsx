export const TECHNOLOGY_PAGE_DATA = {
  PAGE_HEADER: {
    HEADER: 'What do we really mean when we talk about technology',
    BUTTON_LABEL: 'VIEW CASE',
    TEXT: 'In a casual conversation, technology usually means something like a device or a software system. When we`re talking about technology, we are talking about the value it brings to organizations of all sizes. We measured the impact of technology on some of our clients` buisnesses and here are the results we got:',
    TABS: [
      {
        SECTION_LABEL: 'Case studie',
        HEADER: 'Farm Credit Bank',
        ICON: '',
        LINES: [
          {
            QUESTION: 'PROBLEM:',
            ANSWER: 'Inefficent software delivery',
          },
          {
            QUESTION: 'SOLUTION:',
            ANSWER: 'DevOps',
          },
          {
            QUESTION: 'RESULT:',
            ANSWER: '8x faster Deployment',
          },
        ]
      },
      {
        SECTION_LABEL: 'Case studie',
        HEADER: 'Banking Organization',
        ICON: '',
        LINES: [
          {
            QUESTION: 'PROBLEM:',
            ANSWER: 'A large number of post-release defects',
          },
          {
            QUESTION: 'SOLUTION:',
            ANSWER: 'QA automation',
          },
          {
            QUESTION: 'RESULT:',
            ANSWER: '50% reduction of post-release issues',
          },
        ]
      },
      {
        SECTION_LABEL: 'Case studie',
        HEADER: 'Banking Organization',
        ICON: '',
        LINES: [
          {
            QUESTION: 'PROBLEM:',
            ANSWER: 'Loan Payoff Requests processing takes 4 hours',
          },
          {
            QUESTION: 'SOLUTION:',
            ANSWER: 'Automation',
          },
          {
            QUESTION: 'RESULT:',
            ANSWER: 'Loan Payoff Requests processing takes 15 minutes ',
          },
        ]
      }
    ],

  },
  TECHNOLOGIES_STACK: {
    SECTION_LABEL: 'How we do it',
    HEADER: 'Engineering the future with these technologies',
    DESCRIPTION: 'At Enke, we modernize legacy systems, develop custom software, automate business processes and design intelligent systems UI and machine learning',
    CATEGORIES: [
      {
        LABEL: 'Frontend Developer',
        ITEMS: {
          'Javascript': 'javascript',
          'React': 'react',
          'Angular': 'angular',
          'React Native': 'react_native',
          'D3': 'd3'
        }
      },
      {
        LABEL: 'Backend Developer',
        ITEMS: {
          'Java': 'java',
          'Python': 'python',
          '.NET': 'ms_dot_net',
          'Objective C': 'objective_c',
          'Node JS': 'node_js'
        }
      },
      {
        LABEL: 'Databases',
        ITEMS: {
          'Redshift': 'amazon_redshift',
          'Postgre SQL': 'postgre_sql',
          'SQL Server': 'sql_server',
          'Cassandra': 'cassandra',
          'Dynamo DB': 'dynamo_db',
          'Mongo DB': 'mongo'
        }
      },
      {
        LABEL: 'Infrastructure',
        ITEMS: {
          'Hadoop': 'hadoop',
          'Spark': 'apache_spark',
          'Kafka': 'kafka',
          'Tensor Flow': 'tensorflow',
          'Office 365': 'office_365',
          'SharePoint': 'sharepoint',
          'Power Apps': 'power_apps',
          'Sail Point': 'sail_point',
          'ServiceNow': 'service_now',
          'Sales Force': 'sales_force',
          'Bizagi': 'bizagi',
          'AWS': 'aws',
          'Azure': 'azure',
          'Google Cloud': 'google_cloud'
        }
      }
    ]
  },
  PICK_OUT: {
    HEADER: 'Pick out technologies wisely',
    BUTTON_LABEL: 'HOW TO CHOOSE A TECH STACK',
    BACKGROUND: 'Photos/Technology/photo-1454165804606-c3d57bc86b40.png'
  },

  BUILD_SOFTWARE: {
    HEADER: 'Build software faster and cheaper with our ready-made solution',
    DESCRIPTION: 'We developed our own frameworks that allow for implementing intelligent automation solutions across finance, media, and utility industries.',
    BUTTON_LABEL: 'SEE MORE',
    TABS: [
      {
        HEADER: 'Banking Platform',
        TEXT: 'With the Banking Platform, financial organizations can automate their workflow and integrate systems needed to run business operations. Our framework provides integration with Loan Accounting systems, CRM, File Storage, DocuSign and so other types of software. We support both batch-based and real-time data integration.',
        ICON: 'ew-building-alt',
        // BACKGROUND: ''
      },
      {
        HEADER: 'Utility Platform',
        TEXT: 'Utility Platform helps electric companies automatically identify outage areas. The system captures information about electricity consumption from smart meters and transmits it back to the electric company enabling more efficient management of energy networks. Our platform is cloud-agnostic and has unlimited scaling capabilities.',
        ICON: 'ew-radio',
        // BACKGROUND: ''
      },
      {
        HEADER: 'Media Platform',
        TEXT: 'Media Platform uses Big Data analytics and machine learning to allow companies in the media and entertainment industry to understand consumers’ media consumption preferences and predict what content, shows, movies, and music consumers want. Our solution improves the way media companies monetize their content.',
        ICON: 'ew-heart-img',
        // BACKGROUND: ''
      }
    ]
  },
  OUR_RECENT_PROJECTS: {
    HEADER: 'Our recent projects',
    BUTTON_LABEL: 'MORE DETAILS',
    DESCRIPTION: 'Check out some of our recent works to learn more about our technical expertise.',
    TABS: [
      {
        HEADER: 'Log Management',
        TEXT: 'Our log management solution lets IT teams prevent issues before they happen. It analyzes alerts across multiple systems including SolarWinds, Dynatrace, and Nagiosand, and helps developers easily pinpoint the root cause of any software error, within a single query. The tool creates tickets in Jira and/or ServiceNow to report issues.',
      },
      {
        HEADER: 'Voice Over Spreadsheet',
        TEXT: 'We integrated Amazon’s Alexa digital assistant with Google Sheets to help executives get data on anything from new hires to the status of customer orders using voice commands. Our Alexa skill for Amazon Echo is customized to parse reports in Google spreadsheets and pull information whenever you need it.',
      },
    ]
  },
  TECHNOLOGY_IS_A_BUSINESS: {
    HEADER: 'Technology is a business decision',
    BUTTON_LABEL: 'CONTACT US',
    DESCRIPTION: 'All technologies are born out of purpose - to make something better than it was before. If you invest in technology, ground your decision on a simple question: how will my business benefit from this? Great technology is one that gets more job done faster, better, cheaper, and safer.',
    TABS: [
      {
        HEADER: 'Combat declining employee productivity',
        TEXT: 'All technologies are born out of purpose - to make something better than it was before. If you invest in technology, ground your decision on a simple question: how will my business benefit from this? Great technology is one that gets more job done faster, better, cheaper, and safer.'
      },
      {
        HEADER: 'Protect your data from increasing threats',
        TEXT: 'Cyber attacks are increasing, both in number and sophistication. The average cost of data breach today is almost $4 million. With software architect and developed based on core security principles, you can protect the data assets your business needs to operate.'
      },
    ]
  },

  PAGE_BANNER: {
    HEADER: 'Automate your business to cut costs',
    TEXT: 'Technology makes business more efficent. And time saved means extra cash. Move your IT infrastructure to the cloud, automate routing work, build software with an "API-first" mindset to facilitate integration and you will manage to do more with fewer resources',
    TAB_HEADER: 'Transform your business using technology',
    TAB_TEXT: 'We can help you translate your business needs into technical requirements and build valuable solutions for your organization. ',
    TAB_BUTTON_LABEL: 'CONTACT US',
    BANNER: 'Photos/Technology/White-board.png',
    HEADER_TWO: 'Transform your business using technology ',
    TEXT_TWO: 'We can help you translate your business needs into technical requirements and build valuable solutions for your organization.'
  },
};
