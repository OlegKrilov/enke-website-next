import React, {lazy, Suspense} from "react";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import {inject, observer} from "mobx-react";
import {EW_Icon} from "../../Components/EW_Icon/EW_Icon";
import {
  EW_DECORATION_BLOCK_WHITE,
  EW_PRIMARY_BANNER,
  EW_PRIMARY_TEXT, EW_SECONDARY_BANNER, EW_SECONDARY_TEXT,
  EW_SECTION_HEADER
} from "../../Common/Constants/EW_ViewClasses.cnst";
import {EW_AnimatedSection} from "../../Components/EW_AnimatedSection/EW_AnimatedSection";
import {AS_ARRAY, DIG_OUT} from "../../Core/Helpers/Helpers.misc";
import {EW_PublicPage} from "../EW_PublicPage";
import {EW_TechnologiesStack} from "../../Main/EW_TechnologiesStack/EW_TechnologiesStack";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {CONTACTS_FORM, EW_FormModel} from "../../Components/EW_Form/EW_Form.model";
import {DATA, TOP} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {POST} from "../../Common/Constants/EW_Routes.cnst";

const
  EW_Form = lazy(() => import('../../Components/EW_Form/EW_Form'));

const
  ROOT = `ew-technology-page`,
  HEADER_SECTION_TAB = `${ROOT}-headers-section-tab`,
  HEADER_SECTION_TAB_LABEL = `${HEADER_SECTION_TAB}-little-label`,
  HEADER_SECTION_TAB_HEADER = `${HEADER_SECTION_TAB}-header`,
  HEADER_SECTION_TAB_BODY = `${HEADER_SECTION_TAB}-body`,
  BUILD_SOFTWARE_TAB = `${ROOT}`,
  BUILD_SOFTWARE_TAB_HEADER = `${BUILD_SOFTWARE_TAB}`,
  BUILD_SOFTWARE_TAB_TEXT = `${BUILD_SOFTWARE_TAB}`,
  OUR_RECENT_PROJECTS_TAB = `${ROOT}`,
  OUR_RECENT_PROJECTS_TAB_HEADER = `${OUR_RECENT_PROJECTS_TAB}`,
  OUR_RECENT_PROJECTS_TAB_TEXT = `${OUR_RECENT_PROJECTS_TAB}`,
  TECHNOLOGY_IS_A_BUSINESS_TAB = `${ROOT}`,
  TECHNOLOGY_IS_A_BUSINESS_TAB_HEADER = `${TECHNOLOGY_IS_A_BUSINESS_TAB}`,
  TECHNOLOGY_IS_A_BUSINESS_TAB_TEXT = `${TECHNOLOGY_IS_A_BUSINESS_TAB}`,
  CONTACTS_FORM_SECTION = `${ROOT}-contacts-form`;

const
  DEFAULT_BG = 'Photos/Common/bg.png';

@inject('sharedService')
@observer
export default class EW_TechnologyPage extends EW_PublicPage {
  
  private readonly form: EW_FormModel = new EW_FormModel(CONTACTS_FORM);
  
  private scrollToForm = () => {
    const
      formEl = document.querySelector(`.${CONTACTS_FORM_SECTION}`),
      scrollTarget = DIG_OUT((formEl as HTMLDivElement).getBoundingClientRect(), TOP);
  
    window.scroll({
      top: window.scrollY + scrollTarget,
      left: 0,
      behavior: 'smooth'
    });
  };

  private headerUpperTabs = TECHNOLOGY_PAGE_DATA => TECHNOLOGY_PAGE_DATA.FIRST_SECTION.ITEMS.map((d, i) =>
    <div
      className={`col-12 col-lg-4 col-xl-4 padding-top-50 padding-bottom-100 ew-hidden-right transition-duration-10 transition-delay-${6 + (2 * i)}`}
      key={i}>
      <div>
        <div
          className={`${HEADER_SECTION_TAB} ew-bg-blue ew-color-white ew-block-height-2 padding-30`}>
          <div className={`${HEADER_SECTION_TAB_LABEL} font-size-15 margin-bottom-10 opacity-7`}>
            <span>Case study</span>
          </div>
          <div className={`${HEADER_SECTION_TAB_HEADER} font-size-30 padding-right-50`}>
            <b>{d.HEADER}</b>
          </div>
        </div>
      </div>

      <div className={`${HEADER_SECTION_TAB_BODY} ew-bg-white ew-color-black`}>
        {d.ITEMS.map((q, j) =>
          <div className={`padding-20 font-size-14`} key={j}>
            <div className={`row`}>
              <div className={`col-4 opacity-4 padding-0 text-right`}>
                <b>{q['HEADER']}</b>
              </div>
              <div className={`col-8`}>
                <b>{q['TEXT']}</b>
              </div>
            </div>
          </div>
        )}
        {/*<div className={`pos-abs bottom-0 left-0 padding-bottom-30 padding-left-40`}>*/}
        {/*  {d['FILES'] ? <EW_Button onClick={() => this.accessPointService.loadFile(d['FILES'])} children={*/}
        {/*    EW_ButtonModel.buildSquareButton(TECHNOLOGY_PAGE_DATA.FIRST_SECTION.BUTTON_LABEL)*/}
        {/*  }/> : EMPTY_STRING}*/}
        {/*</div>*/}
      </div>
    </div>
  );

  private buildSoftwareTabs = TECHNOLOGY_PAGE_DATA => TECHNOLOGY_PAGE_DATA.FOURTH_SECTION.ITEMS.map((d, i) =>
    <div
      className={`padding-top-50 col-12 col-lg-4 col-xl-4 margin-v-10 ew-hidden-left transition-duration-10 transition-delay-${6 + (2 * i)}`}
      key={i}>
      <div className={`${BUILD_SOFTWARE_TAB} ew-bg-white ew-color-black full-height padding-30 padding-bottom-100 pos-rel`}>
        <div className={``}>
          <EW_Icon source={d.ICON}/>
        </div>
        <div className={`${BUILD_SOFTWARE_TAB_HEADER} font-size-30 margin-bottom-10`}>
          <b>{d.HEADER}</b>
        </div>
        <div className={`${BUILD_SOFTWARE_TAB_TEXT} font-size-15 opacity-4 `}>
          <span>{d.TEXT}</span>
        </div>
        {/*<div className={`pos-abs bottom-0 left-0 padding-30 padding-bottom-40`}>*/}
        {/*  <EW_Button onClick={(e) => console.log(e)} children={*/}
        {/*    EW_ButtonModel.buildLinkButton(TECHNOLOGY_PAGE_DATA.FOURTH_SECTION.BUTTON_LABEL)*/}
        {/*  }/>*/}
        {/*</div>*/}
      </div>
    </div>
  );

  private ourRecentProjectsTabs = TECHNOLOGY_PAGE_DATA => TECHNOLOGY_PAGE_DATA.FIFTH_SECTION.ITEMS.map((d, i) =>
    <div
      className={`col-12 col-lg-6 col-xl-6 margin-v-10 ew-hidden-bottom transition-duration-10 transition-delay-${6 + (2 * i)}`}
      key={i}>
      <div
        className={`${OUR_RECENT_PROJECTS_TAB} pos-rel ew-bg-blue ew-color-white full-height padding-50 padding-bottom-150`}>
        <div className={`${EW_DECORATION_BLOCK_WHITE} top-0 right-0 opacity-3`}/>
        <div className={`${EW_DECORATION_BLOCK_WHITE} top-100 right-0 opacity-1`}/>
        <div className={`${EW_DECORATION_BLOCK_WHITE} top-0 right-100 opacity-1`}/>
        <div className={`${OUR_RECENT_PROJECTS_TAB_HEADER} font-size-56 margin-bottom-20 padding-right-130`}>
          <span>{d.HEADER}</span>
        </div>
        <div className={`${OUR_RECENT_PROJECTS_TAB_TEXT} font-size-18 `}>
          <span>{d.TEXT}</span>
        </div>

        {/*<div className={`pos-abs bottom-0 left-0 padding-bottom-50 padding-left-50`}>*/}
        {/*  <EW_Button onClick={(e) => console.log(e)} children={*/}
        {/*    EW_ButtonModel.buildRoundButton(TECHNOLOGY_PAGE_DATA.FIFTH_SECTION.BUTTON_LABEL)*/}
        {/*  }/>*/}
        {/*</div>*/}

      </div>
    </div>
  );

  private technologyIsBusiness = TECHNOLOGY_PAGE_DATA => TECHNOLOGY_PAGE_DATA.SIXTH_SECTION.ITEMS.map((d, i) =>
    <div
      className={`col-12 col-lg-6 col-xl-6 margin-v-10 ew-hidden-bottom transition-duration-10 transition-delay-${6 + (2 * i)}`}
      key={i}>
      <div
        className={`${TECHNOLOGY_IS_A_BUSINESS_TAB} ew-bg-grey-light ew-color-black full-height`}>
        <div className={`${TECHNOLOGY_IS_A_BUSINESS_TAB_HEADER} font-size-18 margin-bottom-20`}>
          <b>{d.HEADER}</b>
        </div>
        <div className={`${TECHNOLOGY_IS_A_BUSINESS_TAB_TEXT} padding-right-100 font-size-15`}>
          <span>{d.TEXT}</span>
        </div>
      </div>
    </div>
  );
  
  private submitForm = data => {
    const
      {accessPointService, form} = this;
  
    const
      requestPipe = new AbstractPipe([
        () => form.isSubmitted.setValue(true),
        () => accessPointService.sendForm(Object.assign({theme: 'general'}, data), POST.CREATE_GENERAL_MESSAGE)
      ]);
  
    this
      .registerPipes(requestPipe)
      .registerSubscriptions(
        requestPipe.then(res => form.successMessage.setValue(res.statusText)),
        requestPipe.catch(err => form.errorMessage.setValue(err.statusText)),
        requestPipe.finally(() => form.isSubmitted.setValue(false))
      );
  };

  componentDidMount(): void {
    super.componentDidMount();
  
    const {form} = this;
  
    this.registerSubscriptions(
      form.onSubmit.getSubscription(
        e => this.submitForm(e[DATA])
      )
    );
  }

  render() {
    const
      {isActive, pageData, form} = this,
      TECHNOLOGY_PAGE_DATA = pageData.value;

    return pageData.isEmpty ?
      <h3 className={`opacity-3 padding-100`}>IS LOADING...</h3> :

      <article className={`${ROOT}`}>
  
        <EW_AnimatedSection isActive={this.isActive} source={TECHNOLOGY_PAGE_DATA.SIXTH_SECTION} children={
          <section className={`ew-bg-grey-light padding-top-100 padding-bottom-50`}>
            <div className={`container `}>
              <div className={`row `}>
                <div className={`col-12 col-lg-10`}>
                  <div className={`${EW_SECONDARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-6`}>
                    <b>{TECHNOLOGY_PAGE_DATA.SIXTH_SECTION.HEADER}</b>
                  </div>
                  <div className={`margin-v-40 ew-hidden-top transition-duration-10 transition-delay-9`}>
                  <span
                    className={`${EW_SECONDARY_TEXT}`}>{TECHNOLOGY_PAGE_DATA.SIXTH_SECTION.TEXT}</span>
                  </div>
                </div>
                {this.technologyIsBusiness(TECHNOLOGY_PAGE_DATA)}
              </div>
            </div>
          </section>
        }/>
  
        <EW_AnimatedSection isActive={this.isActive} source={TECHNOLOGY_PAGE_DATA.SEVENTH_SECTION} children={
          <section className={`${TECHNOLOGY_IS_A_BUSINESS_TAB} pos-rel ew-section ew-bg-grey-light`}>
            <div className={`pos-abs bottom-0 left-0 full-width ew-bg-grey-light ew-block-height-0-and-half`}/>
            {this.sharedService.isWideScreen && <div className={`pos-abs half-width full-height top-0 right-0 hidden-md-down`}>
				      <div className={`full-height pos-rel ew-hidden-right transition-duration-10 transition-delay-3`}>
					      <EW_BackgroundImage source={TECHNOLOGY_PAGE_DATA.SEVENTH_SECTION.BANNER}/>
					      <div className={`${EW_DECORATION_BLOCK_WHITE} top-0 left-0 opacity-8`}/>
					      <div className={`${EW_DECORATION_BLOCK_WHITE} top-100 left-0 opacity-4`}/>
					      <div className={`${EW_DECORATION_BLOCK_WHITE} top-0 left-100 opacity-4`}/>
				      </div>
			      </div>}
            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-12 col-lg-6`}>
                  <div className={`font-size-18 ew-hidden-left transition-duration-10 transition-delay-5`}>
                    <b>{TECHNOLOGY_PAGE_DATA.SEVENTH_SECTION.HEADER}</b>
                  </div>
                  <div
                    className={`font-size-15  padding-bottom-100  padding-top-30 padding-right-100 ew-hidden-left transition-duration-10 transition-delay-7`}>
                    <span>{TECHNOLOGY_PAGE_DATA.SEVENTH_SECTION.TEXT}</span>
                  </div>
                </div>
              </div>
      
            </div>
      
            <div className={`ew-bg-white padding-v-100`}>
              <div className={`container`}>
                <div className={`row`}>
                  {AS_ARRAY(TECHNOLOGY_PAGE_DATA.SEVENTH_SECTION.ITEMS || []).map((d, i) =>
                    <div className={`col-12 col-lg-6 padding-right-20`} key={i}>
                      <div
                        className={`${EW_SECONDARY_BANNER} ew-hidden-left transition-duration-10 transition-delay-5`}>
                        <b>{d['HEADER']}</b>
                      </div>
                      <div
                        className={`font-size-15 opacity-4 margin-top-40 margin-bottom-100 padding-right-100 ew-hidden-left transition-duration-10 transition-delay-7`}>
                        <span>{d['TEXT']}</span>
                      </div>
                      {/*<div*/}
                      {/*  className={`padding-bottom-150 margin-top-50 pos-rel ew-hidden-left transition-duration-10 transition-delay-12`}>*/}
                      {/*  <EW_Button onClick={(e) => this.scrollToForm()} children={*/}
                      {/*    EW_ButtonModel.buildSquareButton(d.BUTTON_LABEL)*/}
                      {/*  }/>*/}
                      {/*</div>*/}
                    </div>
                  )}
                </div>
              </div>
            </div>
          </section>
        }/>
        
        <EW_AnimatedSection isActive={this.isActive} source={TECHNOLOGY_PAGE_DATA.SECOND_SECTION} children={
          <section className={`ew-section padding-top-100 ew-bg-grey-light`}>
            <div className={`container`}>
              <div className={`row`}>
                <div className={`col-12 col-lg-8`}>
                  <div
                    className={`ew-block-height display-flex ew-hidden-left transition-duration-10 transition-delay-3`}>
                    <b className={`${EW_SECTION_HEADER}`}>{TECHNOLOGY_PAGE_DATA.SECOND_SECTION.SECTION_LABEL}</b>
                  </div>
                  <div className={`${EW_SECONDARY_BANNER} ew-hidden-right transition-duration-10 transition-delay-6`}>
                    <b>{TECHNOLOGY_PAGE_DATA.SECOND_SECTION.HEADER}</b>
                  </div>
                  <div className={`margin-v-40 ew-hidden-left transition-duration-10 transition-delay-9`}>
                    <span className={`${EW_SECONDARY_TEXT}`}>{TECHNOLOGY_PAGE_DATA.SECOND_SECTION.TEXT}</span>
                  </div>
                </div>
                <EW_TechnologiesStack className={`col-12 ew-hidden-right transition-duration-10 transition-delay-12`}/>
              </div>

            </div>
          </section>
        }/>

        <EW_AnimatedSection isActive={this.isActive} source={TECHNOLOGY_PAGE_DATA.THIRD_SECTION} children={
          <section className={`ew-section ew-bg-blue pos-rel display-flex padding-v-100`}>
            <div className={`pos-abs top-0 left-0 size-cover`}>
              <EW_BackgroundImage source={TECHNOLOGY_PAGE_DATA.THIRD_SECTION.BACKGROUND}/>
            </div>
            <div className={`container text-center ew-color-white`}>
              <div
                className={`${EW_PRIMARY_BANNER} ew-hidden-right transition-duration-10 transition-delay-9`}>
                <b>{TECHNOLOGY_PAGE_DATA.THIRD_SECTION.HEADER}</b>
              </div>
              {/*<div className={`margin-top-50 pos-rel ew-hidden-left transition-duration-10 transition-delay-12`}>*/}
              {/*  <EW_Button onClick={(e) => console.log(e)} children={*/}
              {/*    EW_ButtonModel.buildRoundButton(TECHNOLOGY_PAGE_DATA.THIRD_SECTION.BUTTON_LABEL)*/}
              {/*  }/>*/}
              {/*</div>*/}
            </div>
          </section>
        }/>

        <EW_AnimatedSection isActive={this.isActive} source={TECHNOLOGY_PAGE_DATA.FOURTH_SECTION} children={
          <section className={`ew-section padding-v-100 ew-bg-grey-light`}>
            <div className={`container`}>
              <div className={`row `}>
                <div className={`col-12 col-lg-10`}>
                  <div className={`${EW_SECONDARY_BANNER} ew-hidden-left transition-duration-10 transition-delay-12`}>
                    <b>{TECHNOLOGY_PAGE_DATA.FOURTH_SECTION.HEADER}</b>
                  </div>
                  <div
                    className={`padding-right-150 padding-top-30 ew-hidden-right transition-duration-10 transition-delay-12`}>
                    <span className={`${EW_SECONDARY_TEXT}`}>{TECHNOLOGY_PAGE_DATA.FOURTH_SECTION.TEXT}</span>
                  </div>
                </div>
                {this.buildSoftwareTabs(TECHNOLOGY_PAGE_DATA)}
              </div>
            </div>
          </section>
        }/>

        <EW_AnimatedSection isActive={this.isActive} source={TECHNOLOGY_PAGE_DATA.FIFTH_SECTION} children={
          <section className={`ew-section pos-rel ew-bg-grey-light`}>
            <div className={`top-0 left-0 full-width ew-two-three-heigth ew-bg-white pos-abs`}/>
            <div className={`container padding-top-100`}>
              <div className={`row`}>
                <div className={`col-12 text-center margin-bottom-100`}>
                  <div className={`${EW_SECONDARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-9`}>
                    <b>{TECHNOLOGY_PAGE_DATA.FIFTH_SECTION.HEADER}</b>
                  </div>
                  <div
                    className={`${EW_PRIMARY_TEXT} margin-top-40 padding-right-100 margin-left-70 ew-hidden-left transition-duration-10 transition-delay-12`}>
                    <span>{TECHNOLOGY_PAGE_DATA.FIFTH_SECTION.TEXT}</span>
                  </div>
                </div>
                {this.ourRecentProjectsTabs(TECHNOLOGY_PAGE_DATA)}
              </div>
            </div>
          </section>
        }/>
  
        {/*<EW_AnimatedSection isActive={this.isActive} source={TECHNOLOGY_PAGE_DATA.FIRST_SECTION} children={*/}
        {/*  <section className={`ew-section pos-rel ew-bg-grey-light padding-v-50`}>*/}
        {/*    <div className={`container`}>*/}
        {/*      <div className={`row`}>*/}
        {/*        <div*/}
        {/*          className={`col-12 text-center  ew-hidden-right transition-duration-10 transition-delay-9`}>*/}
        {/*          <div className={`${EW_PRIMARY_BANNER} padding-bottom-50`}>*/}
        {/*            <b>{TECHNOLOGY_PAGE_DATA.FIRST_SECTION.HEADER}</b>*/}
        {/*          </div>*/}
        {/*          <div*/}
        {/*            className={`${EW_PRIMARY_TEXT} padding-h-70 margin-bottom-50 ew-hidden-left transition-duration-10 transition-delay-12`}>*/}
        {/*            <span>{TECHNOLOGY_PAGE_DATA.FIRST_SECTION.TEXT}</span>*/}
        {/*          </div>*/}
        {/*        </div>*/}
        {/*      </div>*/}
        {/*      /!*<div className={`row`}>{this.headerUpperTabs(TECHNOLOGY_PAGE_DATA)}</div>*!/*/}
        {/*    </div>*/}
        {/*  </section>*/}
        {/*}/>*/}
  
        <EW_AnimatedSection isActive={isActive} children={
          <section className={`${CONTACTS_FORM_SECTION} ew-section pos-rel ew-bg-white margin-top-100 padding-bottom-100`}>
            <div className={`pos-abs size-cover`}>
              <EW_BackgroundImage className={'size-contain'} source={DEFAULT_BG} />
            </div>
            <div className={`container`}>
              <Suspense fallback={EMPTY_STRING}>
                <EW_Form model={form}>
                  <div className={`text-center`}>
                    <div
                      className={`ew-block-height display-flex align-center ew-hidden-top transition-duration-10 transition-delay-3`}>
                      <b className={`${EW_SECTION_HEADER}`}>Contact us</b>
                    </div>
                    <div className={`${EW_SECONDARY_BANNER} ew-hidden-top transition-duration-10 transition-delay-6 margin-bottom-50`}>
                      <b>We will be happy to help you</b>
                    </div>
                  </div>
                </EW_Form>
              </Suspense>
            </div>
          </section>
        }/>
        
      </article>;
  }

  constructor(props) {
    super(props);

  }

}



