import moment from "moment";
import React from "react";
import {EW_PrivatePage} from "../EW_PrivatePage";
import {observer} from "mobx-react";
import {_IS_SELECTED_, _TOGGLE_, AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {DB_ID, HEADER} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {GET, POST} from "../../Common/Constants/EW_Routes.cnst";
import {EW_AnimatedSection} from "../../Components/EW_AnimatedSection/EW_AnimatedSection";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {AS_FUNCTION, PIPE} from "../../Core/Helpers/Helpers.misc";
import {IoIosArrowDown, IoIosArrowUp, IoMdArrowDown, IoMdArrowUp, IoMdMail, IoMdMailOpen} from "react-icons/all";
import {EMPTY_STRING, IS_SELECTED} from "../../Core/Constants/ViewClasses.cnst";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";

const
  ROOT = `ew-admin-messages`,
  MESSAGE = `${ROOT}-item`,
  MESSAGE_HEADER = `${MESSAGE}-header`,
  MESSAGE_BODY = `${MESSAGE}-body`;

const
  DATE_FORMAT = 'MM/DD/YYYY hh:mm:ss';

interface I_EW_Message {
  _id: string,
  phone: string,
  theme: string,
  email: string,
  text: string,
  isRead: BooleanObservable,
  created: string
}

@observer
export class EW_Admin_Messages extends EW_PrivatePage {

  private readonly messages: AbstractCollection = new AbstractCollection(DB_ID);

  private toggleRow = (d: I_EW_Message) => AS_FUNCTION(d[_TOGGLE_]);

  componentDidMount(): void {
    const
      {isActive, isAuthorized, accessPointService, sharedService, messages} = this,
      {isLoading, growl, servicesAreReady} = sharedService;

    const
      refreshMessages = data => messages.replaceItems(
        data.map((d, i) => ({
        _id: d['_id'],
        phone: d['phone'],
        theme: d['theme'],
        email: d['email'],
        text: d['text'],
        isRead: new BooleanObservable(d['isRead']),
        created: moment(d['created']).format(DATE_FORMAT)
      } as I_EW_Message)));

    const
      dataPipe = new AbstractPipe([
        () => isLoading.setValue(true),
        () => accessPointService.sendPost(POST.CHECK_TOKEN),
        () => accessPointService.sendGet(GET.GET_MESSAGES),
        data => refreshMessages(data),
        () => isLoading.setValue(false),
        () => isActive.setValue(true)
      ], false);

    this
      .registerPipes(dataPipe)
      .registerSubscriptions(
        isAuthorized.getSubscription(
          state => state && dataPipe.run()
        ),

        servicesAreReady.getSubscription(
          state => state && !isAuthorized.currentValue && sharedService.isLoading.setValue(false)
        ),

        dataPipe.catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show()),

        dataPipe.finally(() => isLoading.setValue(false)),

        messages.subscribeOnSelectionChange(
          (selection: I_EW_Message | null) => selection && !selection.isRead.value && PIPE(
            accessPointService.sendPost(POST.UPDATE_MESSAGE, {
              _id: selection._id,
              data: {isRead: true}
            })
              .then(() => selection.isRead.setValue(true))
              .catch(err => growl.addMessage(EW_GrowlModel.buildErrorMessage(err)).show())
          )
        )
      );
  }

  render () {
    const
      {isActive, isAuthorized, messages} = this;

    return isAuthorized.currentValue ? <EW_AnimatedSection className={`${ROOT}`} isActive={isActive} children={
      <div className={`container`}>
        <div className={`row`}>
          <h2 className={`col-12 ew-hidden-bottom transition-duration-10 transition-delay-3 margin-v-50`}>MESSAGES</h2>
          <div className={`col-12 ew-hidden-bottom transition-duration-10 transition-delay-3`}>{messages.items.map((d: I_EW_Message, i) =>
            <div className={`${MESSAGE} ${AS_FUNCTION(d[_IS_SELECTED_]) ? IS_SELECTED : EMPTY_STRING} margin-v-10`} key={i}>
              <div className={`${MESSAGE_HEADER}`}>
                <div className={`row ew-clickable`} onClick={() => this.toggleRow(d)}>
                  <div className={`col-5 ew-block-height-0-and-half display-flex`}>
                    <div className={`padding-h-20`}>
                      {d.isRead.value ? <IoMdMailOpen/> : <IoMdMail/>}
                      <span className={`margin-left-10`}>{d.email}</span>
                    </div>
                  </div>
                  <div className={`col-5 ew-block-height-0-and-half display-flex`}>
                    <b className={`padding-h-20`}>{d.theme}</b>
                  </div>
                  <div className={`col-2 ew-block-height-0-and-half display-flex align-right`}>
                    <div className={`padding-h-20`}>
                      {AS_FUNCTION(d[_IS_SELECTED_]) ? <IoIosArrowUp/> : <IoIosArrowDown />}
                    </div>
                  </div>
                </div>
              </div>
              {AS_FUNCTION(d[_IS_SELECTED_]) && <div className={`${MESSAGE_BODY} padding-50`}>
                <div className={`row`}>
                  <div className={`col-8`}>
                    <span>{d.text}</span>
                  </div>
                  <div className={`col-4 text-right`}>
                    <i>{d.created}</i>
                  </div>
                </div>
              </div>}
            </div>
          )}</div>
        </div>
      </div>
    } /> : <div />;
  }

}

