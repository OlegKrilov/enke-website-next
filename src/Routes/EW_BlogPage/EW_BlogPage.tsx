import React, {Suspense, lazy} from "react";
import {inject, observer} from "mobx-react";
import routingService, {ROUTES, RoutingService} from "../../Services/Routing.service";
import sharedService, {SharedService} from "../../Services/Shared.service";
import userService, {UserService} from "../../Services/User.service";
import accessPointService, {AccessPointService} from "../../Services/AccessPoint.service";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {_REMOVE_, AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {EW_PRIMARY_BANNER, EW_SECONDARY_BANNER, EW_SECTION_HEADER} from "../../Common/Constants/EW_ViewClasses.cnst";
import {IoIosArrowUp, IoIosBriefcase, IoMdHeartEmpty, IoMdSearch, IoMdClose} from "react-icons/all";
import {EW_BackgroundImage} from "../../Components/EW_BackgroundImage/EW_BackgroundImage";
import Moment from "react-moment";
import {AS_ARRAY, DIG_OUT, AWAIT, PIPE, AS_FUNCTION, IS_NOT_NULL} from "../../Core/Helpers/Helpers.misc";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {EW_Button} from "../../Components/EW_Button/EW_Button";
import {EW_ButtonModel} from "../../Components/EW_Button/EW_Button.model";
import {EW_SectionModel} from "../../Components/EW_Section/EW_Section.model";
import {EW_Section} from "../../Components/EW_Section/EW_Section";
import EW_Spinner from "../../Components/EW_Spinner/EW_Spinner";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {BLOG_ITEMS, POST} from "../../Common/Constants/EW_Routes.cnst";
import {AnimationFrameObserver} from "../../Core/Observers/AnimationFrameObserver";
import {BOTTOM, CONTENT, DATA, DB_ID, ID, TYPE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {I_EW_BlogPostContent} from "../../Common/Interfaces/BlogPost.interface";
import {
	IMAGE_BLOCK,
	TEXT_BLOCK
} from "../../Main/EW_AdminBlogConstructor/EW_AdminBlogConstructorContent/EW_AdminBlogConstructorContent";
import {EW_GrowlModel} from "../../Components/EW_Growl/EW_Growl.model";

const
	EW_AdminBlogConstructor = lazy(() => import('../../Main/EW_AdminBlogConstructor/EW_AdminBlogConstructor'));

const
	ROOT = `ew-blog-page`,
	HEADER = `${ROOT}-header`,
	SEARCH_BOX = `${ROOT}-search-box`,
	ARTICLES = `${ROOT}-articles`,
	ARTICLE = `${ROOT}-article`,
	ARTICLE_MAIN_IMAGE = `${ARTICLE}-main-image`,
	ARTICLE_HEADER = `${ARTICLE}-header`,
	ARTICLE_BODY = `${ARTICLE}-body`,
	ARTICLE_FOOTER = `${ARTICLE}-footer`,
	LOADER_BLOCK = `${ROOT}-loader-block`,
	LOADER_BLOCK_ITEM = `${ROOT}-loader-block-item`;

const
	DATE_FORMAT = `LL`;

export interface EW_BlogPageItem {
	data: EW_BlogArticle,
	section: EW_SectionModel,
	animationDelay: number
}

export interface EW_BlogArticle {
	id: string,
	author: string,
	banner: string,
	header: string,
	content: EW_BlogArticleContent[],
	date: number,
	likes: number,
	comments: any[]
}

export interface EW_BlogArticleContent {
	type: string,
	value: any,
	className?: string,
	content?: EW_BlogArticleContent[]
}

@inject('routingService', 'sharedService')
@observer
export default class EW_BlogPage extends AbstractComponent {
	
	private readonly routingService: RoutingService;
	
	private readonly isLoading: BooleanObservable = new BooleanObservable();
	
	private readonly isFinished: BooleanObservable = new BooleanObservable();
	
	private readonly articles: AbstractCollection = new AbstractCollection();
	
	private readonly scrollMarker: any = React.createRef();
	
	private readonly frameObserver: AnimationFrameObserver = new AnimationFrameObserver().run();
	
	private showArticleDetails = (d: EW_BlogArticle) => this.routingService.goTo(
		ROUTES.BLOG_POST, {postId: DIG_OUT(d, DB_ID)}
	);
	
	private deleteArticle = (d) => PIPE(
		() => sharedService.isLoading.setValue(true),
		() => accessPointService.sendPost(POST.DELETE_POST, {id: DIG_OUT(d, DATA, DB_ID)}),
		() => AS_FUNCTION(d[_REMOVE_])
	)
		.catch(err => this.props.sharedService.growl.addMessage(EW_GrowlModel.buildErrorMessage(err), 'error').show())
		.finally(() => sharedService.isLoading.setValue(false));
	
	private getArticleBanner = (d: EW_BlogArticle) => {
		const
			blocks = AS_ARRAY(DIG_OUT(d, CONTENT) || []);
		
		let
			image = null;
		
		const
			_checkNextItem = (items, i: number = 0) => {
				if (IS_NOT_NULL(image)) return true;
				else {
					const
						item = items[i];

					if (!item) return true;
					else {
						const
							type = DIG_OUT(item, TYPE);

						if (type === IMAGE_BLOCK) image = DIG_OUT(item, DATA);

						return _checkNextItem(items, i + 1);
					}
				}
			},
			_checkNextBlock = (i: number = 0) => {
				if (IS_NOT_NULL(image)) return image;
				else {
					const
						block = blocks[i];

					if (!block) return image;
					else return (_checkNextItem(AS_ARRAY(DIG_OUT(block, CONTENT) || [])) && _checkNextBlock(i + 1));
				}
			};
		
		// return null;
		
		return _checkNextBlock();
		
			
			// len = AS_ARRAY(DIG_OUT(d, CONTENT)).length;
		
		
		//
		// for (; i < len;) {
		//
		//
		//
		//
		//
		// }
		
		
		// const
		// 	content = AS_ARRAY(DIG_OUT(d, CONTENT) || []);
		//
		// console.log(
		// 	DIG_OUT(DIG_OUT(d, CONTENT, 0, CONTENT, 0))
		//
		//
		// );
		//
		// return DIG_OUT(content.find((dd: I_EW_BlogPostContent) => dd.type === IMAGE_BLOCK), DATA);
	};
	
	private getArticlePreview = (d: EW_BlogArticle) => {
		const
			blocks = AS_ARRAY(DIG_OUT(d, CONTENT) || []);
		
		let
			charsLeft = 200,
			paragraphs: string[] = [];
		
		const
			_renderText = () => <div>{paragraphs.map((str, i) => <p key={i}>{str}</p>)}</div>,
			_checkNextItem = (items, i: number = 0) => {
				const
					item = items[i];
				
				if (!item || charsLeft <= 0) return true;
				else {
					const
						type = DIG_OUT(item, TYPE);
					
					if (type === TEXT_BLOCK) {
						let
							fullStr: string = DIG_OUT(item, DATA) || EMPTY_STRING,
							len = fullStr.length,
							nextCharsLeft = charsLeft - len;
						
						if (nextCharsLeft < 0)
							fullStr = fullStr.substr(0, len - Math.abs(nextCharsLeft)) + '...';
						
						charsLeft -= len;
						paragraphs.push(fullStr);
					}

					return _checkNextItem(items,i + 1);
				}
			},
			_checkNextBlock = (i: number = 0) => {
				const
					block = blocks[i];
				
				return !block || charsLeft <= 0 ?
					_renderText() :
					_checkNextItem(AS_ARRAY(DIG_OUT(block, CONTENT) || [])) && _checkNextBlock(i + 1);
			};
			
		return _checkNextBlock();
		
		// if (IS_NOT_NULL(image)) return true;
		// else {
		// 	const
		// 		item = items[i];
		//
		// 	if (!item) return true;
		// 	else {
		// 		const
		// 			type = DIG_OUT(item, TYPE);
		//
		// 		if (type === IMAGE_BLOCK) image = DIG_OUT(item, DATA);
		//
		// 		return _checkNextItem(items, i + 1);
		// 	}
		// }
		
		
		// const
		// 	maxLen = 200,
		// 	content = AS_ARRAY(DIG_OUT(d, CONTENT) || []),
		// 	textToShow = DIG_OUT(content.find((dd: I_EW_BlogPostContent) => dd.type === TEXT_BLOCK), DATA);
		//
		// return textToShow ? <p>
		// 	<span>{textToShow.substr(0, maxLen)}</span>
		// 	<span>{textToShow.length > maxLen ? <small>...</small> : EMPTY_STRING}</span>
		// </p> : <div />;
	};
	
	componentDidMount() {
		const
			{articles, isLoading, isFinished, scrollMarker, frameObserver} = this,
			elem: HTMLElement = scrollMarker.current,
			fetchLimit = 10;
		
		const
			dataPipe = new AbstractPipe([
				() => accessPointService.sendGet(BLOG_ITEMS, {
					skip: articles.size,
					limit: fetchLimit
				}),
				(data) => {
					const
						newItems = AS_ARRAY(data).map((d, i) => ({
							data: d,
							section: new EW_SectionModel(),
							animationDelay: i
						}) as EW_BlogPageItem);
					
					console.log(newItems);
					
					newItems.length < fetchLimit && isFinished.setValue(true);
					return articles.addItems(newItems);
				}
			], false);
		
		this.registerSubscriptions(
			
			frameObserver.getSubscription(
				() =>
					!isFinished.value &&
					!isLoading.value &&
					(DIG_OUT(elem.getBoundingClientRect(), BOTTOM) < window.innerHeight) &&
					isLoading.setValue(true)
			),
			
			isLoading.getSubscription(
				state => state && dataPipe.run()
			),
			
			articles.subscribeOnCollectionChange(
				() => articles.getNewItems().forEach((d: EW_BlogPageItem) => d.section.isActive.setValue(true))
			),
			
			dataPipe.catch(
				err => sharedService.growl
					.addMessage(EW_GrowlModel.buildErrorMessage(err), 'error')
					.show()
			),
			
			dataPipe.finally(() => AWAIT(300).then(() => isLoading.setValue(false)))
		);
		
		AWAIT().then(
			() => sharedService.isLoading.setValue(false)
		);
	}
	
	componentWillUnmount() {
		super.componentWillUnmount();
		this.frameObserver.stop();
	}
	
	render () {
		const
			{isLoading, isFinished, articles, scrollMarker} = this;
		
		const
			hasData = !!articles.size;
		
		return <div className={ROOT}>
			<div className={`${HEADER} container`}>
				<div className={`row`}>
					<div className={`col-6`}>
						<div className={`${EW_SECONDARY_BANNER} ew-block-height display-flex align-left`}>BLOG</div>
					</div>
					<div className={`col-6`}>
						{/*<div className={`${SEARCH_BOX} text-right`}>*/}
						{/*	<IoMdSearch />*/}
						{/*</div>*/}
					</div>
					{userService.isAuthorized &&
						<div className={`col-12 text-right padding-v-20`}>
							<Suspense fallback={EMPTY_STRING}>
								<EW_AdminBlogConstructor />
							</Suspense>
						</div>
					}
				</div>
			</div>
			<div className={`${ARTICLES} container`}>{articles.items.map((d: EW_BlogPageItem, i: number) =>
				<div className={`${ARTICLE}`} key={i}>
					<EW_Section model={d.section} className={`row margin-bottom-50`}>
						<div className={`col-12 col-lg-6 col-xl-6`}>
							<div className={`${ARTICLE_MAIN_IMAGE} full-height ew-hidden-left transition-duration-10 transition-delay-${d.animationDelay}`}>
								<EW_BackgroundImage source={this.getArticleBanner(d.data)} />
							</div>
						</div>
						<div className={`col-12 col-lg-6 col-xl-6`}>
							<div className={`${ARTICLE_HEADER} display-flex align-left padding-h-40 ew-hidden-right transition-duration-10 transition-delay-${d.animationDelay}`}>
								<b className={`margin-right-10`}>{d.data.author}</b>
								<IoIosBriefcase />
								<span className={`margin-h-10`}>|</span>
								<Moment date={d.data.date} format={DATE_FORMAT} />
							</div>
							<div className={`${ARTICLE_BODY} padding-h-40`}>
								<div className={`margin-v-10 font-size-26 ew-hidden-right transition-duration-10 transition-delay-${d.animationDelay + 3}`}>
									<b>{d.data.header}</b>
								</div>
								<div className={`font-size-18 opacity-7 ew-hidden-right transition-duration-10 transition-delay-${d.animationDelay + 4}`}>
									{this.getArticlePreview(d.data)}
								</div>
							</div>
							<div className={`${ARTICLE_FOOTER} padding-left-40`}>
								<div className={`row ew-block-height`}>
									<div className={`col-3 full-height ew-hidden-bottom transition-duration-10 transition-delay-${d.animationDelay + 5}`}>
										<div className={`full-height display-flex text-right font-size-18`}>
											<span>{d.data.likes || EMPTY_STRING}</span>
											<IoMdHeartEmpty className={`ew-color-red ew-clickable`}/>
										</div>
									</div>
									<div className={`col-9 full-height ew-hidden-bottom transition-duration-10 transition-delay-${d.animationDelay + 6}`}>
										<div className={`full-height display-flex align-right`}>
											{userService.isAuthorized && <EW_Button onClick={e => this.deleteArticle(d)}>
												{EW_ButtonModel.buildSquareButton('Delete', 'red-button', <IoMdClose/>)}
											</EW_Button>}
											<EW_Button className={`margin-left-10`} onClick={e => this.showArticleDetails(d.data)}>
												{EW_ButtonModel.buildSquareButton('Read More')}
											</EW_Button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</EW_Section>
				</div>
			)}</div>
			<div className={`
				${LOADER_BLOCK}
				${isLoading.value ? EMPTY_STRING : 'opacity-4'}
				display-flex align-center ew-block-height`
			} ref={scrollMarker}>{isFinished.value ?
				<p className={`ew-clickable`} onClick={() => window.scroll({
					top: 0,
					left: 0,
					behavior: 'smooth'
				})}>
					{hasData && <IoIosArrowUp/>}
					<span className="margin-h-10">{hasData ? 'Scroll up' : 'No data'}</span>
					{hasData && <IoIosArrowUp/>}
				</p> :
				<EW_Spinner />
			}</div>
		</div>;
	}
	
	constructor(props) {
		super(props);
		this.routingService = props.routingService;
	}
 
}
