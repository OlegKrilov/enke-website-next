import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {EW_UnderConstructionPage} from "../../Components/EW_UnderConstructionPage/EW_UnderConstructionPage"
import {observer} from "mobx-react";

const
  ROOT = `ew-policy-page`;

@observer
export class EW_PolicyPage extends AbstractComponent {

  render () {
    return <div className={ROOT}>
      <EW_UnderConstructionPage/>
    </div>;
  }

}
