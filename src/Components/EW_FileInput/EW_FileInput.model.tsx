import {StringObservable} from "../../Core/Observables/StringObservable";
import {StructureObservable} from "../../Core/Observables/StructureObservable";
import {AbstractInput} from "../../Core/Abstract/AbstractInput";
import {computed, observable, keys} from "mobx";
import {
  AS_ARRAY,
  AS_FUNCTION,
  IS_FUNCTION,
  IS_NOT_NULL,
  DIG_OUT,
  IS_NULL,
  GET_TRUE
} from "../../Core/Helpers/Helpers.misc";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {DATA, NAME, SIZE, TYPE, PLACEHOLDER, FILE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {ALWAYS_TRUE} from "../../Common/Helpers/EW_Validators";

export interface I_EW_FileInputValidators {
  isRequired?: any,
  isValidType?: any,
  isValidSize?: any
}

export interface I_EW_FileInputSettings {
  placeholder?: string,
  validators?: I_EW_FileInputValidators | null
}

export class EW_FileInputModel extends AbstractInput {

  private readonly _file: StructureObservable = new StructureObservable();

  public readonly defaultValue: any;

  public readonly settings: any;
  
  public setFile = d => {
    const
      {onError, settings, _file} = this,
      validators: I_EW_FileInputValidators = settings.validators;

    if (!AS_FUNCTION(validators.isValidSize, d)) onError.next('File upload error', {
      error: true,
      statusText: 'File size is too big'
    });

    else if (!AS_FUNCTION(validators.isValidType, d)) onError.next('File upload error', {
      error: true,
      statusText: 'File type is not allowed'
    });

    else _file.setValue(d);

    return this;
  };

  public clearValue = () => {
    this._file.resetValue();
    return this;
  };

  public getDefaultSettings = (): I_EW_FileInputSettings => ({
    placeholder: EMPTY_STRING,
    validators: {
      isRequired: ALWAYS_TRUE(),
      isValidType: ALWAYS_TRUE(),
      isValidSize: ALWAYS_TRUE()
    }
  });

  @computed
  public get currentValue () {
    return this._file.isEmpty ? null : this._file.value;
  }

  @computed
  public get fileName () {
    return IS_NULL(this.currentValue) ? EMPTY_STRING : DIG_OUT(this.currentValue, NAME);
  }

  @computed
  public get hasChanges () {
    return `${this.fileName}`.trim() !== `${this.defaultValue}`.trim();
  }

  @computed
  public get isValid (): boolean {
    const
      {_file, settings} = this,
      {validators} = settings,
      val = _file.value.data;

    return IS_NOT_NULL(validators) ? Object.keys(validators).every(key => AS_FUNCTION(validators[key], val)) : true;
  }
  
  constructor (customSettings: I_EW_FileInputSettings = {}, defaultValue: any = null) {
    
    super();
    this.settings = this.getDefaultSettings();
    Object.keys(this.settings.validators).forEach(key => {
      let customValidator = DIG_OUT(customSettings, 'validators', key);
      this.settings.validators[key] = IS_FUNCTION(customValidator) ? customValidator : this.settings.validators[key];
    });
    if (IS_NOT_NULL(defaultValue)) {
      const
        fileName = DIG_OUT(defaultValue, NAME);

      if (fileName) {
        this.setFile({
          [NAME]: fileName,
          [FILE]: defaultValue
        });
        this.defaultValue = fileName;
      }
    }
  }
}

