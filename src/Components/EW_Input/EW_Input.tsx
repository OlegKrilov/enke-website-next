import * as d3 from 'd3';
import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {EW_InputModel} from "./EW_Input.model";
import {EMPTY_STRING, IS_DISABLED, IS_EMPTY, IS_FOCUSED} from "../../Core/Constants/ViewClasses.cnst";
import {AS_FUNCTION, DIG_OUT, IS_FUNCTION} from "../../Core/Helpers/Helpers.misc";
import {DATA, ENTER_KEY} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {PASSWORD_INPUT, STRING_INPUT, TEXT_INPUT} from "../../Common/Constants/EW_InputTypes.cnst";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";

const
  ROOT = `ew-input`,
  CORE = `${ROOT}-core`,
  INPUT_PLACEHOLDER = `${ROOT}-placeholder`; 

@observer
export class EW_Input extends AbstractComponent {

  private readonly model: EW_InputModel;

  private readonly root: any = React.createRef();

  private _onChange = (e) => {
    let
      {model} = this,
      {target} = e,
      val = `${target.value}`;
    
    if (val !== `${model.currentValue}`) {
      model.setValue(val);
      model.onChange.next('change', val || EMPTY_STRING);
    }
  };
  
  private _onKeyPress = (e) => {
    const
      {model} = this;
    
    if (e.which === ENTER_KEY) model.onSubmit.next(e, model.currentValue);
  };

  private _onFocus = (e) => {
    const
      {model} = this,
      {isFocused, onFocus} = model;

    if (!isFocused.value) {
      isFocused.toggleValue(true);
      onFocus.next(e);
    }
  };

  private _onBlur = (e) => {
    const {isFocused, onBlur} = this.model;
    if (isFocused.value) {
      isFocused.setValue(false);
      onBlur.next(e);
    }
  };

  componentDidMount(): void {
    const
      {model, props} = this,
      root = d3.select(this.root.current),
      core = root.select(`.${CORE}`);

    const
      valueObserver = new AbstractObserver(() => model.currentValue);

    this.registerSubscriptions(
      valueObserver.getSubscription(
        () => {
          let
            {currentValue, onChange} = model,
            inputCoreNode = core.node(),
            val = inputCoreNode.value;

          if (`${val}` !== `${currentValue}`) {
            inputCoreNode.value = model.currentValue;
            IS_FUNCTION(DIG_OUT(props, 'onChange')) && AS_FUNCTION(props['onChange'], currentValue);
          }
        }
      )
    );
  }

  render () {
    const
      {model, props, root} = this,
      {className} = props,
      {placeholder, type} = model.settings;

    const
      isFocused = model.isFocused.value,
      isDisabled = model.isDisabled.value,
      isEmpty = !model.currentValue;

    return <label className={`${ROOT} ew-input-base
        ${isDisabled ? IS_DISABLED : EMPTY_STRING} 
        ${isFocused ? IS_FOCUSED : EMPTY_STRING}
        ${isEmpty ? IS_EMPTY : EMPTY_STRING} 
        display-inline-block pos-rel font-size-15 margin-0 ${className || EMPTY_STRING}`} ref={root}>
      <i className={`${INPUT_PLACEHOLDER} pos-abs top-0`}>{placeholder}</i>

      {(type === STRING_INPUT || type === PASSWORD_INPUT) &&
      <input className={`${CORE} display-block full-width padding-h-10 padding-top-20`}
             type={type === PASSWORD_INPUT ? 'password' : 'text'}
             disabled={isDisabled}
             onKeyPress={e => this._onKeyPress(e)}
             onChange={e => this._onChange(e)}
             onBlur={e => this._onBlur(e)}
             onFocus={e => this._onFocus(e)} />}

      {type === TEXT_INPUT &&
      <textarea className={`${CORE} text-input display-block full-width padding-h-10 padding-top-20`}
             disabled={isDisabled}
             onChange={e => this._onChange(e)}
             onBlur={e => this._onBlur(e)}
             onFocus={e => this._onFocus(e)} />}
    </label>;
  }

  constructor (props) {
    super(props);
    this.model = props.model instanceof EW_InputModel ? props.model : new EW_InputModel(props.value || EMPTY_STRING);
  }

}
