import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {StructureObservable} from "../../Core/Observables/StructureObservable";
import {computed} from "mobx";
import {AS_ARRAY, AS_FUNCTION, IS_FUNCTION, IS_NOT_NULL, IS_NULL} from "../../Core/Helpers/Helpers.misc";
import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";
import {StringObservable} from "../../Core/Observables/StringObservable";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {STRING_INPUT, TEXT_INPUT} from "../../Common/Constants/EW_InputTypes.cnst";
import {AbstractInput} from "../../Core/Abstract/AbstractInput";

export interface I_EW_InputSettings {
  placeholder?: string,
  type?: string,
  getFormattedLabel?: any,
  getIcon?: any,
  validators?: any | null
}

export class EW_InputModel extends AbstractInput {

  public readonly defaultValue: any;

  public readonly settings: I_EW_InputSettings;

  public readonly core: StringObservable;

  public readonly onSubmit: AbstractEventObserver = new AbstractEventObserver('submit');
  
  public setValue = (val: string = EMPTY_STRING): EW_InputModel => {
    this.core.setValue(val);
    return this;
  };

  public clearValue = (): EW_InputModel => {
    this.core.resetValue();
    return this;
  };

  public getDefaultSettings = (): I_EW_InputSettings => ({
    placeholder: EMPTY_STRING,
    type: STRING_INPUT,
    getFormattedLabel: () => this.currentValue,
    getIcon: () => EMPTY_STRING,
    validators: null
  });
  
  @computed
  public get hasDefaultValue (): boolean {
    return IS_NOT_NULL(this.defaultValue);
  }

  @computed
  public get currentValue (): string {
    return this.core.value;
  }

  @computed
  public get isValid (): boolean {
    const
      {core, settings} = this,
      {validators} = settings,
      val = core.value;

    return IS_NOT_NULL(validators) ? validators.every(d => AS_FUNCTION(d, val)) : true;
  }

  @computed
  public get hasChanges (): boolean {
    return `${this.core.value}`.trim() !== `${this.defaultValue}`.trim();
  }

  constructor (customSettings: I_EW_InputSettings = {}, defaultValue: string = EMPTY_STRING) {
    super();
    this.defaultValue = defaultValue;
    this.core = new StringObservable(this.defaultValue);
    this.settings = Object.assign(this.getDefaultSettings(), customSettings);

    if (IS_NOT_NULL(this.settings.validators)) this.settings.validators = AS_ARRAY(this.settings.validators).filter(IS_FUNCTION);
  }

}

