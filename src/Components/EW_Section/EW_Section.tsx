import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {EMPTY_STRING, IS_ACTIVE, IS_FINISHED} from "../../Core/Constants/ViewClasses.cnst";
import {EW_SectionModel} from "./EW_Section.model";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {AnimationFrameObserver} from "../../Core/Observers/AnimationFrameObserver";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {AS_FUNCTION, AWAIT, IS_NOT_NULL} from "../../Core/Helpers/Helpers.misc";

const
	ROOT = `ew-section-root`;

@observer
export class EW_Section extends AbstractComponent {
	
	private readonly model: EW_SectionModel;
	
	private readonly isFinished: BooleanObservable = new BooleanObservable();
	
	private readonly frameObserver: AnimationFrameObserver = new AnimationFrameObserver();
	
	private readonly root: any =  React.createRef();
	
	componentDidMount() {
		const
			{model, isFinished, frameObserver, root} = this,
			{isActive, onReady} = model,
			elem: HTMLElement = root.current;
		
		this.registerSubscriptions(
			
			isActive.getSubscription(
				state => state && frameObserver.run()
			),
			
			isFinished.getSubscription(
				state => {
					if (state) {
						frameObserver.stop();
						AWAIT(300).finally(() => onReady.next(null));
					}
				}
			),
			
			frameObserver.getSubscription(
				() => {
					if (!isActive.value) return;
					
					const
						rect = elem.getBoundingClientRect(),
						viewPort = [100, window.innerHeight - 100];
					
					if (
						(rect.bottom >= viewPort[0] && rect.bottom <= viewPort[1]) ||
						(rect.top >= viewPort[0] && rect.top <= viewPort[1]) ||
						(rect.top <= viewPort[0] && rect.bottom >= viewPort[1])
					) isFinished.setValue(true);
				}
			)
		);
	}
	
	componentWillUnmount() {
		super.componentWillUnmount();
		this.frameObserver.stop();
	}
	
	render () {
		const
			{props, root, model, isFinished} = this,
			{children, className} = props,
			{isActive} = model;
		
		return <section className={
			`${ROOT}
			${className || EMPTY_STRING}
			${isActive.value ? IS_ACTIVE : EMPTY_STRING}
			${isFinished.value ? IS_FINISHED : EMPTY_STRING}
			`} ref={root}>{children || EMPTY_STRING}</section>;
	}
	
	constructor(props) {
		super(props);
		this.model = props.model;
	}
	
}

