import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";

export class EW_SectionModel {

	public readonly isActive: BooleanObservable = new BooleanObservable();
	
	public readonly onReady: AbstractEventObserver = new AbstractEventObserver('ready');
	
}

