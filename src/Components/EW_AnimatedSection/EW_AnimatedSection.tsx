import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {EMPTY_STRING, IS_ACTIVE, IS_FINISHED} from "../../Core/Constants/ViewClasses.cnst";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {AnimationFrameObserver} from "../../Core/Observers/AnimationFrameObserver";
import {SharedService} from "../../Services/Shared.service";
import {AbstractPipe} from "../../Core/Abstract/AbstractPipe";
import {AS_FUNCTION, IS_NOT_NULL} from "../../Core/Helpers/Helpers.misc";

const
  ROOT = `ew-animated-section`;

@inject('sharedService')
@observer
export class EW_AnimatedSection extends AbstractComponent {

  private sharedService: SharedService;
  
  private isActive: BooleanObservable;
  
  private onAnimationEnd: any;

  private source: any = null;

  private isFinished: BooleanObservable = new BooleanObservable();

  private animationTimer: AnimationFrameObserver = new AnimationFrameObserver();

  private root: any = React.createRef();

  componentDidMount(): void {
    const
      {sharedService, source, isActive, isFinished, animationTimer} = this,
      root: HTMLElement = this.root.current;

    const
      sourcePipe: AbstractPipe = new AbstractPipe([
        () => sharedService.isWideScreen ? sharedService.searchImages(source || {}) : [],
        images => sharedService.loadImages(images)
      ], false);

    this.registerSubscriptions(

      sourcePipe.finally(() => isFinished.setValue(true)),

      isActive.getSubscription(
        state => state && animationTimer.run()
      ),

      isFinished.getSubscription(
        state => {
          if (state) {
            animationTimer.stop();
            AS_FUNCTION(this.onAnimationEnd, true);
          }
        }
      ),

      animationTimer.getSubscription(
        () => {
          if (sourcePipe.isRunning || !isActive.value) return;

          const
            rect = root.getBoundingClientRect(),
            viewPort = [100, window.innerHeight - 100];

          if (
            (rect.bottom >= viewPort[0] && rect.bottom <= viewPort[1]) ||
            (rect.top >= viewPort[0] && rect.top <= viewPort[1]) ||
            (rect.top <= viewPort[0] && rect.bottom >= viewPort[1])
          ) IS_NOT_NULL(source) ? sourcePipe.run() : isFinished.setValue(true);
        }
      )
    );
  }

  componentWillUnmount(): void {
    super.componentWillUnmount();
    this.animationTimer.stop();
  }

  render () {
    const
      {isActive, isFinished, props, root} = this,
      {className, children} = props;

    return <section className={`${ROOT}
      ${className || EMPTY_STRING} 
      ${isActive.value ? IS_ACTIVE : EMPTY_STRING}
      ${isFinished.value ? IS_FINISHED : EMPTY_STRING}
    `} ref={root}>{children}</section>
  }

  constructor (props) {
    super(props);
    this.sharedService = props.sharedService;
    this.isActive = props.isActive;
    this.source = props.source || null;
    this.onAnimationEnd = props.onAnimationEnd;
  }

}

