import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {EMPTY_STRING, IS_LOADING} from "../../Core/Constants/ViewClasses.cnst";
import {observer} from "mobx-react";

const
	ROOT = `ew-spinner`,
	ITEM = `${ROOT}-item`;

@observer
export default class EW_Spinner extends AbstractComponent {

	private readonly isLoading: BooleanObservable;
	
	render () {
		const
			{isLoading, props} = this,
			{className} = props;
		
		return <div className={`${ROOT} size-cover display-flex align-center ew-bg-white
				${className || EMPTY_STRING} ${isLoading.value ? IS_LOADING : EMPTY_STRING}`}>
			{[0, 1, 2].map((i) => <div className={`${ITEM}`} key={i}/>)}
		</div>;
	}
	
	constructor(props) {
		super(props);
		this.isLoading = props.isLoading instanceof BooleanObservable ? props.isLoading : new BooleanObservable(true);
	}
	
}
