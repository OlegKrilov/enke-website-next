import * as d3 from 'd3';
import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {StringObservable} from "../../Core/Observables/StringObservable";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {BACKGROUND_IMAGE} from "../../Core/Constants/PropertiesAndAttributes.cnst";

const
  ROOT = `ew-icon`;

@observer
export class EW_Icon extends AbstractComponent {

  private model: StringObservable;

  private root: any = React.createRef();

  componentDidMount(): void {
    const root = d3.select(this.root.current);
    this.registerSubscriptions(
      this.model.getSubscription(
        val => val && root.style(BACKGROUND_IMAGE, () => `url(/Icons/${val}.svg)`)
      )
    );
  }

  render () {
    const
      {props, model, root} = this,
      {className} = props;

    return <div className={`${ROOT} ${className || EMPTY_STRING}`} ref={root}/>
  }

  constructor (props) {
    super(props);
    this.model = props.model instanceof StringObservable ? props.model : new StringObservable(props.source);
  }

}

