import React from "react";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {MdChevronRight, MdKeyboardArrowRight} from "react-icons/all";

export class EW_ButtonModel {

  public static buildSquareButton = (label: any = EMPTY_STRING, className: string = EMPTY_STRING, icon: any = <MdChevronRight/>) =>
    <div className={`ew-button-interactive square-button display-inline-flex ew-color-white pos-rel ${className || EMPTY_STRING}`}>
      <div
        className={'square-button-label display-inline-flex text-uppercase font-bold font-size-15 padding full-height padding-h-20'}>
        <span>{label}</span>
      </div>
      <div
        className={`square-button-icon display-inline-flex base-half-width font-size-25 full-height align-center`}>{icon}</div>
      <div className={`ew-button-animation pos-abs top-0 left-0`}/>
    </div>;

  public static buildSquareButtonReverse = (label: any = EMPTY_STRING, className: string = EMPTY_STRING, icon: any = <MdChevronRight/>) =>
    <div className={`ew-button-interactive square-button display-inline-flex ew-color-blue pos-rel ${className || EMPTY_STRING}`}>
      <div
        className={'square-button-label display-inline-flex text-uppercase font-bold font-size-15 padding full-height padding-h-20'}>
        <span>{label}</span>
      </div>
      <div
        className={`square-button-icon display-inline-flex base-half-width font-size-25 full-height align-center`}>{icon}</div>
      <div className={`ew-button-animation pos-abs top-0 left-0`}/>
    </div>;

  public static buildLinkButton = (label: any = EMPTY_STRING, className: string = EMPTY_STRING, icon: any = <MdChevronRight/>) =>
    <div className={`ew-button-interactive as-text-button display-inline-flex ew-color-blue pos-rel ${className || EMPTY_STRING}`}>
      <div
        className={'square-button-label display-inline-flex text-uppercase font-bold font-size-15 padding full-height '}>
        <b>{label}</b>
      </div>
      <div
        className={`square-button-icon display-inline-flex base-half-width font-size-25 full-height align-center`}>{icon}</div>
      <div className={`ew-button-animation pos-abs top-0 left-0`}/>
    </div>;

  public static buildRoundButton = (label: any = EMPTY_STRING, className: string = EMPTY_STRING, icon: any = <MdChevronRight/>) =>
    <div className={`ew-button-interactive padding-h-20 round-button display-inline-flex ew-color-white pos-rel ${className || EMPTY_STRING}`}>
      <b className={'text-uppercase font-size-15 '}>{label}</b>
      {icon && <span className={`margin-left-10 font-size-20 line-height-20`}>{icon}</span>}
      <div className={`ew-button-animation pos-abs top-0 left-0`}/>
    </div>;

  public isDisabled: BooleanObservable = new BooleanObservable();

  public onClick: AbstractEventObserver = new AbstractEventObserver('click');

  public toggleDisabled = (state: boolean | null = null): EW_ButtonModel => {
    this.isDisabled.toggleValue(state);
    return this;
  };

}
