import * as d3 from 'd3';
import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {EW_ButtonModel} from "./EW_Button.model";
import {AbstractSubscription} from "../../Core/Abstract/AbstractSubscription";
import {EMPTY_STRING, IS_DISABLED} from "../../Core/Constants/ViewClasses.cnst";
import {AS_FUNCTION, CHECK_CLICK_TARGET, IS_FUNCTION} from "../../Core/Helpers/Helpers.misc";
import {observer} from "mobx-react";
import {
  BLOCK,
  DISPLAY, HEIGHT, LEFT,
  NONE,
  OPACITY,
  SCALE, TOP,
  TRANSFORM,
  TRANSLATE, WIDTH
} from "../../Core/Constants/PropertiesAndAttributes.cnst";

const
  ROOT = `ew-button`,
  CUSTOM = `${ROOT}-custom`,
  INTERACTIVE_ELEM = `${ROOT}-interactive`,
  ANIMATION_ELEM = `${ROOT}-animation`;

@observer
export class EW_Button extends AbstractComponent {

  private model: EW_ButtonModel;

  private root: any = React.createRef();

  private _onCLick = e => {
    const
      {model} = this,
      {isDisabled, onClick} = model;

    if (!isDisabled.value) {
      let
        root = d3.select(this.root.current),
        interactiveElem = root.select(`.${INTERACTIVE_ELEM}`),
        animationElem = root.select(`.${ANIMATION_ELEM}`),
        rect = root.node().getBoundingClientRect(),
        left = e.clientX - rect.left,
        top = e.clientY - rect.top;

      animationElem
        .style(DISPLAY, () => BLOCK)
        .style(TOP, () => `${top}px`)
        .style(LEFT, () => `${left}px`)
        .style(WIDTH, () => `${0}px`)
        .style(HEIGHT, () => `${0}px`)
        .style(OPACITY, () => 1)
        .transition().duration(500)
        .style(TOP, () => `${top - 50}px`)
        .style(LEFT, () => `${left - 50}px`)
        .style(WIDTH, () => `${100}px`)
        .style(HEIGHT, () => `${100}px`)
        .style(OPACITY, () => 0)
        .on('end', () => animationElem.style(DISPLAY, () => NONE));

      onClick.next(e);
    }
  };

  componentDidMount(): void {
    IS_FUNCTION(this.props.onClick) && this.registerSubscriptions(
      this.model.onClick.getSubscription(
        (e) => AS_FUNCTION(this.props.onClick, e)
      )
    );
  }

  render () {
    const
      {model, props, root} = this,
      {children, className} = props,
      {isDisabled, onClick} = model;

    return <button className={`
      display-inline-block
      ${ROOT} ${className ? className : EMPTY_STRING}
      ${isDisabled.value ? IS_DISABLED : EMPTY_STRING}`} onClick={(e) => this._onCLick(e)} ref={root}>
      {children && <div className={`${CUSTOM} full-width`}>{children}</div>}
    </button>;
  }

  constructor (props) {
    super(props);
    this.model = props.model instanceof EW_ButtonModel ? props.model : new EW_ButtonModel();
  }
}

