import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {EW_ImageInputModel} from "./EW_ImageInput.model";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {EW_FileInput} from "../EW_FileInput/EW_FileInput";
import {EW_BackgroundImage} from "../EW_BackgroundImage/EW_BackgroundImage";
import {StringObservable} from "../../Core/Observables/StringObservable";
import {DIG_OUT} from "../../Core/Helpers/Helpers.misc";
import {DATA, FILE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {IoMdClose} from "react-icons/all";

const
  ROOT = `ew-image-upload`,
  CORE = `${ROOT}-core`,
  PREVIEW = `${ROOT}-preview`,
  OVERLAY = `${ROOT}-overlay`,
  CLEAR_BUTTON = `${ROOT}-clear-button`;

@observer
export class EW_ImageInput extends AbstractComponent {

  private readonly model: EW_ImageInputModel;
  
  private readonly imageSource: StringObservable = new StringObservable();

  componentDidMount() {
    const
      {model, imageSource} = this;
    
    const
      sourceObserver: AbstractObserver = new AbstractObserver(() => model.currentValue);
    
    this.registerSubscriptions(
      sourceObserver.getSubscription(
        (val) => {
          const
            file = DIG_OUT(val, FILE);
  
          if (file) {
            const
              reader = new FileReader();

            reader.onload = () => imageSource.setValue(reader.result);
            reader.readAsDataURL(file);
          } else imageSource.resetValue();
        }
      )
    );
  }
  
  render () {
    const
      {props, model, imageSource} = this,
      {className, children, useSizeContain} = props;
    
    const
      showPreview = model.currentValue;

    return <div className={`${ROOT} ${className || EMPTY_STRING} pos-rel size-cover`}>
      {!showPreview && <EW_FileInput model={model} className={`${CORE} size-cover`}>{children}</EW_FileInput>}
      {showPreview && <EW_BackgroundImage className={`${PREVIEW} ${useSizeContain ? 'size-contain' : 'size-cover'}`} model={imageSource}>
        <div className={`pos-abs size-cover top-0 left-0`}>{children}</div>
        <div className={`${OVERLAY} size-cover ew-color-white text-right pos-rel`}>
          <div className={`${CLEAR_BUTTON} display-inline-flex margin-10 font-size-30`} onClick={() => model.clearValue()}>
            <IoMdClose />
          </div>
        </div>
      </EW_BackgroundImage>}
    </div>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
  }

}
