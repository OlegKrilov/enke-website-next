import {AbstractInput} from "../../Core/Abstract/AbstractInput";
import {StructureObservable} from "../../Core/Observables/StructureObservable";
import {EW_FileInputModel, I_EW_FileInputSettings} from "../EW_FileInput/EW_FileInput.model";
import {StringObservable} from "../../Core/Observables/StringObservable";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";

export class EW_ImageInputModel extends EW_FileInputModel {

  constructor (customSettings: I_EW_FileInputSettings = {}, defaultValue: any = null) {
    super(customSettings, defaultValue);
  }

}

