import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {inject, observer} from "mobx-react";
import {AWAIT, PIPE} from "../../Core/Helpers/Helpers.misc";

@inject('sharedService')
@observer
export class EW_UnderConstructionPage extends AbstractComponent {

  componentDidMount(): void {
    AWAIT(200).then(() => this.props.sharedService.isLoading.setValue(false));
  }

  render() {
    return <section className={`ew-section`}>
      <div className={`padding-100 display-flex align-center full-height`}>
        <h1>Page is under construction.</h1>
      </div>
    </section>
  }

  constructor (props) {
    super(props);
  }
}
