import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {EW_BackgroundImage} from "../EW_BackgroundImage/EW_BackgroundImage";
import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {CLASSNAME, MODEL, NAME, TYPE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {STRING_INPUT, TEXT_INPUT} from "../../Common/Constants/EW_InputTypes.cnst";
import {IS_REQUIRED, IS_VALID_EMAIL} from "../../Common/Helpers/EW_Validators";
import {EW_Input} from "../EW_Input/EW_Input";
import {EW_InputModel} from "../EW_Input/EW_Input.model";
import {EW_Button} from "../EW_Button/EW_Button";
import {EW_ButtonModel} from "../EW_Button/EW_Button.model";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {AS_FUNCTION, DIG_OUT} from "../../Core/Helpers/Helpers.misc";
import {I_EW_FormField} from "../../Common/Interfaces/FormField.interface";
import {EW_FormModel} from "./EW_Form.model";
import {EW_FileInput} from "../EW_FileInput/EW_FileInput";
import {IoMdCheckmarkCircleOutline, IoMdCloseCircleOutline, FaSpinner} from "react-icons/all";
import EW_Spinner from "../EW_Spinner/EW_Spinner";

const
  ROOT = `ew-form`,
  FORM_CONTENT = `${ROOT}-content`,
  FORM_HEADER = `${ROOT}-header`,
  FORM_BODY = `${ROOT}-body`,
  FORM_MESSAGE = `${ROOT}-message`,
  FORM_SUCCESS_ICON = `${FORM_MESSAGE}-success-icon`,
  FORM_ERROR_ICON = `${FORM_MESSAGE}-error-icon`,
  FORM_OVERLAY = `${ROOT}-overlay`,
  FORM_OVERLAY_SPINNER = `${FORM_OVERLAY}-spinner`;

@observer
export default class EW_Form extends AbstractComponent {

  private readonly model: EW_FormModel;

  private submitButton: EW_ButtonModel = new EW_ButtonModel();

  componentDidMount(): void {
    const
      {submitButton, props, model} = this;

    const
      formStateObserver = new AbstractObserver(
        () => model.isValid
      );

    this.registerSubscriptions(
      formStateObserver.getSubscription(
        state => submitButton.toggleDisabled(!state)
      ),

      submitButton.onClick.getSubscription(
        e => {
          if (!model.isValid) return;
          let {value} = model;

          AS_FUNCTION(DIG_OUT(props, 'onSubmit'), value);
          model.onSubmit.next(e, value);
        }
      )
    );
  }

  render () {
    const
      {props, model, submitButton} = this,
      {className, children, buttonLabel} = props,
      {successMessage, errorMessage, isSubmitted} = model;
    
    const
      showForm = !successMessage.value && !errorMessage.value;

    return <div className={`${ROOT} pos-rel`}>
      <div className={`${FORM_CONTENT} row`}>
        <div className={`col-12`}>
          <div className={`${FORM_HEADER}`}>{children}</div>
        </div>
        <div className={`col-3 hidden-md-down`} />
        <div className={`col-12 col-lg-6 col-xl-6`}>
          <div className={`${FORM_BODY} row`}>
            {showForm && model.fields.items.map((d: I_EW_FormField, i) =>
              <div className={`${d[CLASSNAME]} margin-bottom-20 ew-hidden-bottom transition-duration-10 transition-delay-${6 + (i * 2)}`} key={i}>
                <div className={`ew-form-input`}>
                  {d[TYPE] === 'fileInput' ?
                    <EW_FileInput className={`full-width`} model={d[MODEL]} children={
                      <div>
                        File should be .doc, .docx, .ppt or .pdf format. Max file size -
                        10Mb
                      </div>}/> :
                    <EW_Input className={`full-width`} model={d[MODEL]}/>
                  }
            
                </div>
              </div>
            )}
            {successMessage.value && <div className={`${FORM_MESSAGE}`}>
					    <div className={`${FORM_SUCCESS_ICON}`}>
						    <IoMdCheckmarkCircleOutline />
					    </div>
					    <span>{successMessage.value}</span>
				    </div>}
            {errorMessage.value && <div className={`${FORM_MESSAGE}`}>
					    <div className={`${FORM_ERROR_ICON}`}>
						    <IoMdCloseCircleOutline />
					    </div>
					    <span>{errorMessage.value}</span>
				    </div>}
            {showForm && <div className={`col-12 text-center ew-hidden-bottom transition-duration-10 transition-delay-15`}>
              <EW_Button model={submitButton} children={EW_ButtonModel.buildSquareButton(buttonLabel || 'Submit')}/>
            </div>}
          </div>
        </div>
      </div>
      {isSubmitted.value && <div className={`${FORM_OVERLAY} pos-abs top-0 left-0 size-cover`}>
        <EW_Spinner className={`${FORM_OVERLAY_SPINNER} size-cover`}/>
      </div>}
    </div>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
  }

}
