import {AbstractCollection} from "../../Core/Abstract/AbstractCollection";
import {CLASSNAME, DATA, FILE, MODEL, NAME, TYPE} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {EW_InputModel} from "../EW_Input/EW_Input.model";
import {
  IS_REQUIRED,
  IS_VALID_EMAIL,
  IS_VALID_FILE_SIZE,
  IS_VALID_FILE_TYPE,
  IS_VALID_PHONE
} from "../../Common/Helpers/EW_Validators";
import {FILE_INPUT, PASSWORD_INPUT, STRING_INPUT, TEXT_INPUT} from "../../Common/Constants/EW_InputTypes.cnst";
import {I_EW_FormField} from "../../Common/Interfaces/FormField.interface";
import {computed} from "mobx";
import {AbstractObserver} from "../../Core/Abstract/AbstractObserver";
import {AbstractEventObserver} from "../../Core/Observers/AbstractEventObserver";
import {EMPTY_STRING} from "../../Core/Constants/ViewClasses.cnst";
import {EW_FileInputModel} from "../EW_FileInput/EW_FileInput.model";
import {DIG_OUT} from "../../Core/Helpers/Helpers.misc";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {StringObservable} from "../../Core/Observables/StringObservable";

export const
  CONTACTS_FORM = 'contactsForm',
  CV_FORM = 'cvForm',
  CASE_STUDY_FORM = 'caseStudyForm',
  LOGIN_FORM = 'loginForm',
  ADD_COMMENT_FORM = 'addCommentForm';

export class EW_FormModel {

  private readonly allowedFileTypes: any[] = [
    /.doc$/i,
    /.docx$/i,
    /.ppt$/i,
    /.pdf$/i
  ];
  
  public fields: AbstractCollection = new AbstractCollection(NAME);

  public onSubmit: AbstractEventObserver = new AbstractEventObserver('submit');
  
  public isSubmitted: BooleanObservable = new BooleanObservable();
  
  public errorMessage: StringObservable = new StringObservable();
  
  public successMessage: StringObservable = new StringObservable();

  public resetForm = () => {
    this.fields.items.forEach((d: I_EW_FormField) => d[MODEL].clearValue());
    [this.isSubmitted, this.errorMessage, this.successMessage].forEach(f => f.resetValue());
    return this;
  };

  public getContactsFormFields = (): I_EW_FormField[] => ([
    {
      [NAME]: 'firstName',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'First Name'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'lastName',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'Last Name'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'email',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED(), IS_VALID_EMAIL()],
        type: STRING_INPUT,
        placeholder: 'Your email'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'phone',
      [MODEL]: new EW_InputModel({
        type: STRING_INPUT,
        placeholder: 'Phone number'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'text',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: TEXT_INPUT,
        placeholder: 'How can we help you?'
      }),
      [CLASSNAME]: 'col-12'
    }
  ]);
  
  public getCurriculumVitaeFormFields = (): I_EW_FormField[] => ([
    {
      [NAME]: 'firstName',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'First Name'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'lastName',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'Last Name'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'email',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED(), IS_VALID_EMAIL()],
        type: STRING_INPUT,
        placeholder: 'Your email'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'phone',
      [MODEL]: new EW_InputModel({
        type: STRING_INPUT,
        placeholder: 'Phone number'
      }),
      [CLASSNAME]: 'col-12 col-lg-6 col-xl-6'
    },
    {
      [NAME]: 'file',
      [TYPE]: 'fileInput',
      [MODEL]: new EW_FileInputModel({
        placeholder: 'Add your CV',
        validators: {
          isRequired: IS_REQUIRED(),
          isValidType: IS_VALID_FILE_TYPE(this.allowedFileTypes),
          isValidSize: IS_VALID_FILE_SIZE('15MB')
        },
      
      }),
      [CLASSNAME]: 'col-12'
    }
  ]);
  
  public getCaseStudyFormFields = (): I_EW_FormField[] => ([
    {
      [NAME]: 'firstName',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'First Name'
      }),
      [CLASSNAME]: 'col-12'
    },
    {
      [NAME]: 'lastName',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'Last Name'
      }),
      [CLASSNAME]: 'col-12'
    },
    {
      [NAME]: 'email',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED(), IS_VALID_EMAIL()],
        type: STRING_INPUT,
        placeholder: 'Email'
      }),
      [CLASSNAME]: 'col-12'
    }
  ]);
  
  public getAdminLoginFormFields = (): I_EW_FormField[] => ([
    {
      [NAME]: 'login',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: STRING_INPUT,
        placeholder: 'Login'
      }),
      [CLASSNAME]: 'col-12'
    },
    {
      [NAME]: 'password',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: PASSWORD_INPUT,
        placeholder: 'Password'
      }),
      [CLASSNAME]: 'col-12'
    }
  ]);
  
  public getAddCommentFormFields = (): I_EW_FormField[] => ([
    {
      [NAME]: 'comment',
      [MODEL]: new EW_InputModel({
        validators: [IS_REQUIRED()],
        type: TEXT_INPUT,
        placeholder: 'Your comment'
      }),
      [CLASSNAME]: 'col-12'
    }
  ]);

  @computed
  public get isValid () {
    return this.fields.items.reduce(
      (isValid: boolean, d: I_EW_FormField) => isValid ?  d[MODEL].isValid : isValid, true
    );
  }

  @computed
  public get value () {
    return this.isValid ?
      this.fields.items.reduce((agg, d: I_EW_FormField) => {
        const
          val = (d[TYPE] === 'fileInput' ?
            DIG_OUT(d[MODEL], 'currentValue', FILE) :
            DIG_OUT(d[MODEL], 'currentValue')
          );
        
        return Object.assign(agg, {[d[NAME]]: val});
      }, {}) : null;
  }
  
  constructor (formType: string = EMPTY_STRING) {
    let
      items;
    
    switch (formType) {
      case CONTACTS_FORM: items = this.getContactsFormFields(); break;
      case CV_FORM: items = this.getCurriculumVitaeFormFields(); break;
      case CASE_STUDY_FORM: items = this.getCaseStudyFormFields(); break;
      case LOGIN_FORM: items = this.getAdminLoginFormFields(); break;
      case ADD_COMMENT_FORM: items = this.getAddCommentFormFields(); break;
      default: items = []
    }
  
    this.fields.addItems(items);
  }
}
