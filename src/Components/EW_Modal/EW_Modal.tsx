import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {BooleanObservable} from "../../Core/Observables/BooleanObservable";
import {EMPTY_STRING, IS_CLOSED, IS_OPENED} from "../../Core/Constants/ViewClasses.cnst";
import {AWAIT} from "../../Core/Helpers/Helpers.misc";
import {observer} from "mobx-react";
import {MdClose} from "react-icons/all";

const
  ROOT = `ew-modal`,
  MODAL_CONTENT = `${ROOT}-content`,
  MODAL_OVERLAY = `${ROOT}-overlay`,
  MODAL_CLOSE = `${ROOT}-close-btn`;

@observer
export default class EW_Modal extends AbstractComponent {

  private readonly isOpened: BooleanObservable;

  private readonly isClosed: BooleanObservable = new BooleanObservable();

  componentDidMount(): void {
    const
      {isOpened, isClosed} = this;

    this.registerSubscriptions(
      isOpened.getSubscription(
        (state) => state ?
          isClosed.setValue(!isOpened.value) :
          AWAIT(300).then(() => isClosed.setValue(!isOpened.value))
      )
    );
  }

  render () {
    const
      {isOpened, isClosed, props} = this,
      {className, children, showCloseButton} = props;
    
    return <div className={`
      ${ROOT}
      ${isOpened.value ? IS_OPENED : EMPTY_STRING}
      ${isClosed.value ? IS_CLOSED : EMPTY_STRING} pos-fix top-0 left-0 size-cover`}>
      <div className={`${MODAL_OVERLAY} pos-abs top-0 left-0 size-cover`}
           onClick={() => showCloseButton && isOpened.toggleValue(false)} />
      <div className={`${MODAL_CONTENT} ${className || EMPTY_STRING}`}>
        {showCloseButton && <div className={`${MODAL_CLOSE} pos-abs ew-clickable top-10 right-10`}
                                 onClick={() => isOpened.toggleValue(false)}>
	        <MdClose />
        </div>}
        {children || EMPTY_STRING}</div>
    </div>
  }

  constructor (props) {
    super(props);
    this.isOpened = props.isOpened;
  }
}

