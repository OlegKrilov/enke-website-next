import * as d3 from 'd3';
import React from "react";
import {AbstractComponent} from "../../Core/Abstract/AbstractComponent";
import {observer} from "mobx-react";
import {EW_GrowlModel, I_EW_GrowlMessage} from "./EW_Growl.model";
import {DIG_OUT} from "../../Core/Helpers/Helpers.misc";
import {EMPTY_STRING, IS_OPENED} from "../../Core/Constants/ViewClasses.cnst";
import {CLASSNAME, CONTENT} from "../../Core/Constants/PropertiesAndAttributes.cnst";
import {IoMdClose} from "react-icons/all";
import {AbstractTimer} from "../../Core/Observers/AbstractTimer";

const
  ROOT = `ew-growl`,
  GROWL_CONTENT = `${ROOT}-content`,
  CLOSE_BTN = `${ROOT}-close-btn`;

@observer
export default class EW_Growl extends AbstractComponent {

  private readonly ref: any = React.createRef();

  private readonly model: EW_GrowlModel;

  private readonly timer: AbstractTimer = new AbstractTimer({delay: 5000});

  componentDidMount(): void {
    const
      {model, ref, timer} = this,
      {isOpened} = model,
      root = d3.select(ref.current);

    const
      runTimer = () => timer.run(() => isOpened.setValue(false));

    this.registerSubscriptions(
      isOpened.getSubscription(
        state => state ? runTimer() : timer.stop()
      )
    );

    root
      .on('mouseover', () => timer.stop())
      .on('mouseout', () => runTimer());
  }

  componentWillUnmount(): void {
    super.componentWillUnmount();
    this.timer.stop();
  }

  render () {
    const
      {model, props, ref} = this,
      {className} = props,
      {messages, isOpened} = model,
      message: I_EW_GrowlMessage = DIG_OUT(messages, 'selection') || {};

    return <div className={`${ROOT} pos-fix top-20 ew-bg-white
        ${className || EMPTY_STRING} 
        ${isOpened.value ? IS_OPENED : EMPTY_STRING}`} ref={ref}>
      <div className={`${CLOSE_BTN} pos-abs top-10 right-10`}>
        <IoMdClose className={`ew-clickable font-size-20`} onClick={() => isOpened.setValue(false)} />
      </div>
      <div className={`${GROWL_CONTENT} ${message[CLASSNAME] || EMPTY_STRING} padding-50`}>{message[CONTENT] || EMPTY_STRING}</div>
    </div>;
  }

  constructor (props) {
    super(props);
    this.model = props.model;
  }

}

