import {
  AS_ARRAY,
  AS_PROMISE,
  AS_FUNCTION,
  DO_NOTHING,
  IS_FUNCTION,
  IS_OBJECT,
  IS_PROMISE
} from "../Helpers/Helpers.misc";
import {AbstractCollection} from "./AbstractCollection";
import {AbstractSubscription} from "./AbstractSubscription";
import {StructureObservable} from "../Observables/StructureObservable";
import {DATUM, ID, TASK} from "../Constants/PropertiesAndAttributes.cnst";
import {BooleanObservable} from "../Observables/BooleanObservable";
import {computed} from "mobx";

const
  _NO_RESULT_ = '_no_result_';

interface I_AbstractPipe_Core {
  result: any,
  error: any,
  value: any
}

interface I_AbstractPipe_Task {
  [ID]: string,
  [TASK]: any,
  [DATUM]: any
}

export class AbstractPipe {
  
  private readonly _tasks: any[];

  private readonly _core: StructureObservable = new StructureObservable({
    result: false,
    error: false,
    value: undefined
  } as I_AbstractPipe_Core);

  private readonly _isRunning: BooleanObservable = new BooleanObservable();

  private readonly _stack: AbstractCollection = new AbstractCollection();

  private _refresh = () => this._stack.replaceItems(AS_ARRAY(this._tasks || []).map((task, i) => ({
    [ID]: `${i}`,
    [TASK]: task,
    [DATUM]: undefined
  } as I_AbstractPipe_Task)));
  
  public clearStack = (): AbstractPipe => {
    this._stack.clearItems();
    return this;
  };

  public run = (index: any = 0): AbstractPipe => {
    const
      {_core, _stack, _isRunning} = this;
    
    const
      gotError = (err) => {
        this.clearStack();
        _isRunning.setValue(false);
        _core.setValue({
          value: err,
          error: true
        });
      },

      gotResult = () => {
        _isRunning.setValue(false);
        _core.setValue({result: true});
      },

      checkError = (val) => IS_OBJECT(val) && val['error'],

      nextTask = () => {
        const taskId = `${index}`;
        if (_stack.hasItem(taskId)) {
          _stack.select(taskId);
          let
            {selection} = _stack,
            val = _core.value.value;
            try {
              if (checkError(val)) gotError(val);
              else {
                let step = selection[TASK];
                AS_PROMISE(IS_PROMISE(step) ? step : (IS_FUNCTION(step) ? AS_FUNCTION(step, val) : () => step))
                  .then(_val => {
                    _core.setValue({value: _val});
                    selection[DATUM] = _val;
                    index++;
                    nextTask();
                  })
                  .catch(err => gotError(err));
              }
            } catch (err) {
              gotError(err);
            }

        } else {
          _stack.clearSelection();
          gotResult();
        }
      };

    this._refresh().then(() => nextTask());

    return this;
  };

  public stop = (): AbstractPipe => {
    this
      .clearStack()
      ._core.resetValue();
    return this;
  };

  public getResults = () => this._stack.items.map((d: I_AbstractPipe_Task) => d[DATUM]);

  public then = (fn: any = DO_NOTHING): AbstractSubscription => this._core.getSubscription(
    (core: I_AbstractPipe_Core) => core.result && AS_FUNCTION(fn, core.value)
  );

  public catch = (fn: any = DO_NOTHING): AbstractSubscription => this._core.getSubscription(
    (core: I_AbstractPipe_Core) => core.error && AS_FUNCTION(fn, core.value)
  );

  public finally = (fn: any = DO_NOTHING): AbstractSubscription => this._core.getSubscription(
    (core: I_AbstractPipe_Core) => (core.result || core.error) && AS_FUNCTION(fn)
  );

  @computed
  public get isRunning () {
    return this._isRunning.value;
  }

  constructor (tasks: any, runImmediately: boolean = true) {
    this._tasks = AS_ARRAY(tasks);
    runImmediately && this.run();
  }

}

