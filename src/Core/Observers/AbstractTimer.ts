import {AS_FUNCTION, IS_OBJECT} from '../Helpers/Helpers.misc';
import {BooleanObservable} from '../Observables/BooleanObservable';

export interface IAbstractTimerSettings {
  delay?: number;
  loop?: boolean;
  runImmediately?: boolean;
}

export class AbstractTimer {

  private static _getDefaultSettings = (): IAbstractTimerSettings => ({
    delay: 25,
    loop: false,
    runImmediately: false
  });

  private _timer: any = null;

  private _settings: IAbstractTimerSettings = AbstractTimer._getDefaultSettings();

  private _isEnabled: BooleanObservable = new BooleanObservable();

  private _dropTimeout (): this {
    this._timer && clearTimeout(this._timer);
    this._timer = null;
    return this;
  }

  public run (callback) {
    let
      {delay, loop, runImmediately} = this._settings,
      _run = () => {
        this._dropTimeout();
        this._isEnabled.value && (this._timer = setTimeout(() => {
          AS_FUNCTION(callback);
          loop && _run();
        }, delay));
      };

    this._isEnabled.setValue(true);
    runImmediately && AS_FUNCTION(callback);
    _run();
  }

  public stop (): this {
    this._isEnabled.setValue(false);
    return this;
  }

  public get isEnabled (): boolean {
    return this._isEnabled.value;
  }

  constructor (settings: IAbstractTimerSettings = {}) {
    Object.assign(this._settings, IS_OBJECT(settings) ? settings : {});
  }

}

